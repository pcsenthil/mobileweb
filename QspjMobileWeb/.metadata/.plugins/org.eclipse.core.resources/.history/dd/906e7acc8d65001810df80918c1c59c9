package com.qspj.appstorefront;

import java.text.MessageFormat;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.appdata.StorefrontDataVerification;
import com.qspj.uimap.locator.storefront.LandingPage;
import com.qspj.uimap.locator.storefront.LoggedInPage;
import com.qspj.uimap.locator.storefront.SignInAndRegisterPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

@SuppressWarnings("javadoc")
public class SignUpApp extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final StorefrontDataVerification storeFrontDataVrf = new StorefrontDataVerification();
	private final LandingPageApp landingPageApp = new LandingPageApp();

	public void register(final String title, final String firstName, final String lastName, final String email,
			final String password) throws Exception {
		try {
			// Select Title 
			if (storeFrontDataVrf.vrfTitleData(title)) {
				if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.ddTitle)) {
					utils.setFalseResult("Title dropdown is not displayed");
					throw new Exception();
				} else {
					driver.elementEventControling.select(SignInAndRegisterPage.ddTitle, title);
				}
			} else {
				utils.setFalseResult("Invalid Title Data");
			}

			// Enter First Name

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtFirstName)) {
				utils.setFalseResult("First Name Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtFirstName, firstName);
			}

			// Enter Last Name
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtLastName)) {
				utils.setFalseResult("Last Name Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtLastName, lastName);
			}

			// Enter Email Address
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtEmailRegister)) {
				utils.setFalseResult("Email Address Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtEmailRegister, email);
			}

			// Enter Password
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtPasswordRegister)) {
				utils.setFalseResult("Password Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtPasswordRegister, password);
			}

			// Enter Confirm Password

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtConfirmPassword)) {
				utils.setFalseResult("Confirm Password Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtConfirmPassword, password);
				
			}

			// Select Register Button
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.btRegister)) {
				utils.setFalseResult("Register Button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.btRegister);
			}

			driver.waitControling.implicitlyWaitBySecond(5);
			this.vrfRegistration();
		}

		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
4			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void vrfRegistration() throws Exception {
		if (driver.elementChecking.isElementDisplay(LoggedInPage.alertSuccess) && (driver.elementEventControling.getText(LoggedInPage.alertSuccess).trim().contains(ReqStorefrontData.registrationSuccess))) {
			utils.setTrueResult("Registration is successful. Success Message displayed");

			landingPageApp.selectMenu();
			if (driver.elementEventControling.getText(LoggedInPage.lbUserWelcome).trim().contains("WELCOME")) {
				utils.setTrueResult("Registratiom Successful and Logged in");
				
		
			} else {
				utils.setFalseResult("Registratiom Successful but not Logged in");
				throw new Exception();
			}
		} else {
			utils.setFalseResult(MessageFormat.format("Registration is not successful. Message displayed : \"{0}\"",
					driver.elementEventControling.getText(LoggedInPage.alertSuccess)));
		}
	}

	
	public void forgotYourPassword() throws Exception {
		try {

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.lnkForgotPassword)) {
				utils.setFalseResult("Forgot password link is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.lnkForgotPassword);
				driver.waitControling.waitPageLoadTimeOutBySecond(45);

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}
	
	
	public void forgotPasswordPopup(final String userId) throws Exception {

		try {

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtForgotPasswordPopup)) {
				utils.setFalseResult("Forgot password popup is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtUserId, userId);

			}
			// Select Reset Password Button
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.btResetPassword)) {
				utils.setFalseResult("Password Rest Button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.btResetPassword);
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}

	}

}
