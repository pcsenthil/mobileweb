package com.qsp.appstorefront;

import java.text.MessageFormat;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.qsp.appcommon.CommonAppFunc;
import com.qsp.appdata.ReqStorefrontData;
import com.qsp.uimap.locator.storefront.CartPage;
import com.qsp.uimap.locator.storefront.CartPopup;
import com.qsp.uimap.locator.storefront.LandingPage;
import com.qsp.uimap.locator.storefront.ProductDetailPage;
import com.qsp.uimap.locator.storefront.WishListPage;
import com.qsp.uimap.locator.storefront.WishListPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

/******* SenthilKumar *************/

public class WishListApp extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final CartApp cartApp = new CartApp();

	public void addToWishlist() throws Exception {
		try {

			if (!driver.elementChecking.isElementDisplay(WishListPage.btAddToWishlist)) {
				utils.setFalseResult("Add To Cart button IS NOT displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.btAddToWishlist);
				utils.setTrueResult("Product is added to wishlist");
			}
			if (!driver.elementChecking.isElementDisplay(WishListPage.alertInfo)) {
				utils.setFalseResult("Alert message is NOT displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.alertInfo);
				utils.setTrueResult("Alert message is displayed");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	// Create Public Wish List

	public void createPublicWishlist(final String PublicWishlist) throws Exception {
		try {

			if (!driver.elementChecking.isElementDisplay(WishListPage.btAddToWishlistPlus)) {
				utils.setFalseResult("Add To wish list button + is not displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.btAddToWishlistPlus);
				utils.setTrueResult("Create wishlist accordian is displayed");
			}
			Thread.sleep(5000);
			if (!driver.elementChecking.isElementDisplay(WishListPage.createNewWishlist)) {
				utils.setFalseResult("Create New Wishlist button is not dsplayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.createNewWishlist);
				utils.setTrueResult("Open Popup for creating Public Wishlist");
			}
			if (!driver.elementChecking.waitForElementPresent(WishListPage.textCreateWishlistPopup, 10)) {
				utils.setFalseResult("Create Wishlist Popup is not displayed");
				throw new Exception();
			} else {

				utils.setTrueResult("Create Wishlist Popup is displayed");
			}
			if (!driver.elementChecking.isElementDisplay(WishListPage.optPublicWishlist)) {
				utils.setFalseResult("Option is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(WishListPage.optPublicWishlist);
			}
			if (!driver.elementChecking.isElementDisplay(WishListPage.textPublicWishlist)) {
				utils.setFalseResult("Create new public wishlist text box is  not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(WishListPage.textPublicWishlist, PublicWishlist);
			}

			if (!driver.elementChecking.isElementDisplay(WishListPage.btCreateNewWishlist)) {
				utils.setFalseResult("Create new wishlist button is  not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(WishListPage.btCreateNewWishlist);
			}
			
			if(!driver.elementChecking.isElementDisplay(ProductDetailPage.alertSuccess))
			{
				utils.setFalseResult("Success Alert is not displayed");
			}else{
				if(driver.elementEventControling.getText(ProductDetailPage.alertSuccess).contains("Created Successfully")){
					utils.setTrueResult(MessageFormat.format(
							"\"{0}\" - \"{1}\"",
							PublicWishlist, driver.elementEventControling.getText(ProductDetailPage.alertSuccess).trim()));
				}
			}
			
			this.addProductToWishlist(PublicWishlist);
			

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void createPrivateWishlist(final String PrivateWishlist) throws Exception {
		try {

			if (!driver.elementChecking.isElementDisplay(WishListPage.btAddToWishlistPlus)) {
				utils.setFalseResult("Add product To wish list button + is not displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.btAddToWishlistPlus);
				utils.setTrueResult("Create wish List accordian is displayed");
			}
			Thread.sleep(5000);
			if (!driver.elementChecking.isElementDisplay(WishListPage.createNewWishlist)) {
				utils.setFalseResult("Create New Wish list button is not displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.createNewWishlist);
				utils.setTrueResult("Open Popup for creating Public Wish list");
			}
			if (!driver.elementChecking.waitForElementPresent(WishListPage.textCreateWishlistPopup, 5)) {
				utils.setFalseResult("Popup is not displayed");
				throw new Exception();
			} else {

				utils.setTrueResult("Popup is displayed");
			}
			if (!driver.elementChecking.isElementDisplay(WishListPage.textPrivateWishlist)) {
				utils.setFalseResult("Create new Private wish list text box is  not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(WishListPage.textPrivateWishlist, PrivateWishlist);
			}

			if (!driver.elementChecking.isElementDisplay(WishListPage.btCreateNewWishlist)) {
				utils.setFalseResult("Create new wish list button is  not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(WishListPage.btCreateNewWishlist);
			}
			
			if(!driver.elementChecking.isElementDisplay(ProductDetailPage.alertSuccess))
			{
				utils.setFalseResult("Success Alert is not displayed");
			}else{
				if(driver.elementEventControling.getText(ProductDetailPage.alertSuccess).equalsIgnoreCase("Created Successfully")){
					utils.setTrueResult(MessageFormat.format(
							"\"{0}\" - \"{1}\"",
							PrivateWishlist, driver.elementEventControling.getText(ProductDetailPage.alertSuccess).trim()));
				}
			}
			
			this.addProductToWishlist(PrivateWishlist);
			

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void removePublicPrivateWishlist() throws Exception {
		try {
			// click my Account Wish List

			if (!driver.elementChecking.isElementDisplay(WishListPage.texPublicwishlist)) {
				utils.setTrueResult("Wishlist is not available");

			} else {
				// get product count on Wish list
				final int countProd = this.countPublicPrivateWishlist();
				if (countProd == 0) {
					utils.setTrueResult("Wish List is empty");
				} else {
					for (int i = 2; i <= countProd; i++) {
						// click remove icon on each product to remove
						this.deleteWishlist();
					}

				}

				if (this.countPublicPrivateWishlist() == 1) {
					utils.setTrueResult("Only Default Wishlist is Available");
				} else if (this.countPublicPrivateWishlist() == 0) {
					utils.setTrueResult("WishList is empty.");
				} else {
					utils.setFalseResult("Wishlist is not empty");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public int countPublicPrivateWishlist() throws Exception {

		int countProd = 0;
		try {
			// get List of menu item which have tag "li"
			final List<WebElement> wishListTagLI = commonApp.getSubItemListByTag(WishListPage.listingItemsInWishlist,
					"li");
			countProd = wishListTagLI.size();
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return countProd;

	}

	private void deleteWishlist() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(WishListPage.btnDeleteWishlist)) {
				utils.setFalseResult("Remove Wishlist Icon of  IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(WishListPage.btnDeleteWishlist);
				utils.setTrueResult("Remove Wishlist icon is clicked correct");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

	}

	public void myAccountWishlist() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkDefaultWishlist)) {
				utils.setFalseResult("Default Wishlist link is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkDefaultWishlist);
				utils.setTrueResult("Defaultwishlist products are Displayed");

			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	// public Wish list
	public void publicWishlistName() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.texPublicwishlist)) {
				utils.setFalseResult("Public Wishlist is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.texPublicwishlist);
				utils.setTrueResult("Public Wishlist product are Displayed");

			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void removeWishlist() throws Exception {
		try {
			// click my Account Wish List

			if (driver.elementChecking.isElementDisplay(LandingPage.defmsg)) {
				utils.setTrueResult("Wishlist is not available");

			} else {
				// get product count on Wish list
				final int countProd = this.countWishlist();
				if (countProd == 0) {
					utils.setTrueResult("Wish List is empty");
				} else {
					for (int i = 1; i <= countProd; i++) {
						// click remove icon on each product to remove
						this.deleteItemInWishlist();
					}

				}

				if (driver.elementChecking.isElementDisplay(LandingPage.defmsg)) {
					utils.setTrueResult("Wishlist is empty. All products are removed successfully");
				} else {
					utils.setFalseResult("WishList is not empty. Still products are available in the Wishlist");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public int countWishlist() throws Exception {

		int countProd = 0;
		try {
			// get List of menu item which have tag "li"
			final List<WebElement> cartListTagLI = commonApp.getSubItemListByClass(WishListPage.listItemsInWishlist,
					WishListPage.itemEntryClassName);
			countProd = cartListTagLI.size();
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return countProd;

	}
	
	public int countWishlistPDP() throws Exception {

		int countProd = 0;
		try {
			// get List of menu item which have tag "li"
			final List<WebElement> cartListTagLI = commonApp.getSubItemListByTag(WishListPage.listWishlist,
					"li");
			countProd = cartListTagLI.size();
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return countProd;

	}


	public void deleteItemInWishlist() throws Exception {
		try {
			// verify Remove product icon
			if (!driver.elementChecking.isElementDisplay(LandingPage.btnDeleteitem)) {
				utils.setFalseResult("Remove Product Icon of  IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(LandingPage.btnDeleteitem);
				utils.setTrueResult("Remove Product icon is clicked correct");

				/*
				 * if(driver.elementEventControling.getText(CartPage.alertInfo).
				 * contains("Product has been removed from your cart")) { utils.
				 * setTrueResult("'Product has been removed from your cart' alert is displayed"
				 * ); } else { utils.
				 * setFalseResult("'Product has been removed from your cart' alert is not displayed"
				 * ); }
				 */
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

	}

	public void myPublicWishlist() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkPublictWishlist)) {
				utils.setFalseResult("Public Wishlist link is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkPublictWishlist);
				utils.setTrueResult("Public Wishlist products are Displayed");
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void wishlistClickAddToCart() throws Exception {
		try {

			if (!driver.elementChecking.isElementDisplay(LandingPage.btnWishlistAddToCart)) {
				utils.setFalseResult("Add To wish list button is not displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(LandingPage.btnWishlistAddToCart);
				utils.setTrueResult("Wishlist product moved to Cart");
			}

			if (!driver.elementChecking.isElementDisplay(LandingPage.defmsg)) {
				utils.setFalseResult("Wish list product is not moved to Cart");
				throw new Exception();
			} else {

				utils.setTrueResult("Wishlist product is moved to Cart");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void findWishlistByEmail(final String emailId) throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.texWishlistEmail)) {
				utils.setFalseResult("User Name/Email Id text field is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.texWishlistEmail, emailId);
				utils.setTrueResult("Email Id entered successfully");
			}
			if (!driver.elementChecking.isElementDisplay(LandingPage.btnFindNow)) {
				utils.setFalseResult("Find Now button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(LandingPage.btnFindNow);
				utils.setTrueResult("User's wishlist is displayed");
			}
			
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkSeeAllWishlists)) {
				utils.setFalseResult("All the User's Wishlist link is not displayed");
				throw new Exception();
			} else {
				/*// get Public wish list count on find wish list page
				
				final int countProd = this.countWishlist();
				if (countProd == 0) {
					utils.setTrueResult("Wish List is empty");
				} else {
					for (int i = 1; i <= countProd; i++) {
						// click remove icon on each product to remove
						this.deleteItemInWishlist();
					}*/
				driver.elementEventControling.click(LandingPage.lnkSeeAllWishlists);
				utils.setTrueResult("Wishlist Products are displayed");
			}
			if (!driver.elementChecking.isElementDisplay(LandingPage.btnOthersWishlistAddToCart)) {
				utils.setFalseResult("Add to Cart button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(LandingPage.btnOthersWishlistAddToCart);
				utils.setTrueResult("Product is moved to cart");
			}
			
			cartApp.openCartPopup();
			cartApp.clickCheckOutOnCartPopup();

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void addProductToWishlist(final String wishListName) throws Exception {
		try {
			boolean searchresult = false;
			if (!driver.elementChecking.isElementDisplay(WishListPage.btAddToWishlistPlus)) {
				utils.setFalseResult("Add To wish list button + is not displayed");
				throw new Exception();
			} else {

				driver.elementEventControling.click(WishListPage.btAddToWishlistPlus);
				utils.setTrueResult("Create wishlist accordian is Clicked");
			}
			Thread.sleep(5000);
			
			final int countProd = this.countWishlistPDP();
			if (countProd == 0) {
				utils.setFalseResult("Wishlist - List is empty");
			}else{
				for (int i = 1; i <= countProd; i++) {
					String wishListNameXpath = WishListPage.listWishlist +"/li["+i+"]/span[1]";
					if(driver.elementEventControling.getText(wishListNameXpath).trim().equalsIgnoreCase(wishListName)){
						searchresult = true;
						utils.setTrueResult("Wishlist found");
						driver.elementEventControling.click(wishListNameXpath);
						driver.waitControling.waitPageLoadTimeOutBySecond(10);

						if(!driver.elementChecking.isElementDisplay(ProductDetailPage.alertSuccess))
						{
							utils.setFalseResult("Success Alert is not displayed");
						}else{
							if(driver.elementEventControling.getText(ProductDetailPage.alertSuccess).contains("Created Successfully")){
								utils.setTrueResult(MessageFormat.format(
										"\"{0}\" - \"{1}\"",
										wishListName, driver.elementEventControling.getText(ProductDetailPage.alertSuccess).trim()));
							}
						}
						break;
					}
			}
				if(!searchresult)
				{
					utils.setFalseResult("WishList NOT found");
				}			
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickCreatedPublicWishlistName() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkPublictWishlist)) {
				utils.setFalseResult("Public Wishlist product is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkPublictWishlist);
				utils.setTrueResult("Public wishlist Product is Displayed");

			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	public void vrfWishList_MyAccount(final String wishListName, final String productName)throws Exception {
		try {
			
			boolean searchresult = false;
			final int countProd = this.countPublicPrivateWishlist();
			if (countProd == 0) {
				utils.setFalseResult("Wishlist - List is empty");
			}else{
				for (int i = 2; i <= countProd; i++) {
					String wishListNameXpath = WishListPage.listingItemsInWishlist +"/li["+i+"]/span[1]/a";
					if(driver.elementEventControling.getText(wishListNameXpath).trim().equalsIgnoreCase(wishListName)){
						searchresult = true;
						utils.setTrueResult("Wishlist found");
						driver.elementEventControling.click(wishListNameXpath);
						driver.waitControling.waitPageLoadTimeOutBySecond(5);
						this.searchProdutInWishList(productName);
					}
				}
				if(!searchresult)
				{
					utils.setFalseResult("WishList NOT found in Wishlist Page");
				}	
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	private void searchProdutInWishList(final String productName)throws Exception {
		try {		
			boolean searchresult = false;
			final int countProd = this.countWishlist();
			if (countProd == 0) {
				utils.setFalseResult("No Products available in the Wishlist");
			}
			else{
				for (int i = 1; i <= countProd; i++) {
					String productNameXpath = WishListPage.listItemsInWishlist +"/div["+i+"]/div/div[2]/div[1]/a/span";
					if(driver.elementEventControling.getText(productNameXpath).trim().equalsIgnoreCase(productName)){
						searchresult = true;
						utils.setTrueResult("Product found in the Wishlist");			
					}
				}
				if(!searchresult)
				{
					utils.setFalseResult("Product NOT found in the WishList");
				}
			}
		}
		catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		
		
	}
}
