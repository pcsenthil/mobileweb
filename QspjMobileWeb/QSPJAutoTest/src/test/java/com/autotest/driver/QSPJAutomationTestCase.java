package com.autotest.driver;

import org.junit.BeforeClass;
import org.junit.Test;

import com.qspj.utils.StoreFrontExcelHandler;
import com.qspj.utils.StoreFrontImageHandler;
import com.sai.driver.AutomationFWTestCase;


/**
 *
 */
public class QSPJAutomationTestCase extends AutomationFWTestCase
{

	/**
	 *
	 */
	public QSPJAutomationTestCase()
	{
	}

	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void beforeClass() throws Exception
	{
		excelhandle = new StoreFrontExcelHandler();
		sikuliFunc = new StoreFrontImageHandler();
		beforeRunner();
	}
//
	@Override
	@Test
	public void driverScript() throws Exception
	{
		super.driverScript();
	}
}
