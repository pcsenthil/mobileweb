package com.oneliberty.appcommon;

import java.text.MessageFormat;

import com.oneliberty.uimap.locator.common.PaymentProcessPage;
import com.oneliberty.uimap.locator.pa.PA_NewProposalStep4;
import com.oneliberty.uimap.locator.pa.PaymentPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class PaymentProcess extends Environment{

	public CommonAppFunc commonApp = new CommonAppFunc();

	/**
	 * @Function: 		selectPaymentType
	 * @Description:	this function is used to select Payment type
	 * @param 			paymentType: "Cash", "Check", "Credit Card", "Credit Card Instalment"
	 * @throws 			Exception
	 */
	public void selectPaymentType(String paymentType) throws Exception {
		try {
			// select Cash
			if(paymentType.equals(AppInit.paymentTypeNew[0])) {
				jqueryFunc.onclick(PaymentProcessPage.rbCashID);
			}
			// select Check
			else if(paymentType.equals(AppInit.paymentTypeNew[1])) {
				jqueryFunc.onclick(PaymentProcessPage.rbCheckID);
			}
			// select Credit Card
			else if(paymentType.equals(AppInit.paymentTypeNew[2])) {
				jqueryFunc.onclick(PaymentProcessPage.rbCreditCardID);
			}
			// select Credit Card Instalment
			else if(paymentType.equals(AppInit.paymentTypeNew[3])) {
				jqueryFunc.onclick(PaymentProcessPage.rbCreditCardInstalment);
			}
			else {
				utils.setFalseResult("Invalid data of payment Type.");
				throw new Exception();
			}
			driver.waitControling.sleep(2000);
			utils.setTrueResult(MessageFormat.format("Payment Type: \"{0}\" is selected correct", paymentType));
		}
		catch (Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	/**
	 * @Function: 		gotoPaymentPage
	 * @Description:	this function is used to go to Payment page	
	 * @param 			none
	 * @throws 			Exception
	 */
	public void gotoPaymentPage() throws Exception {
		try {
			// click on Payment button
			jqueryFunc.onclick(PaymentProcessPage.btPaymentID);
			// click OK button on Declaration form
			if(!driver.elementChecking.waitForElementPresent(PaymentProcessPage.btOK, 10)) {
				utils.setFalseResult("Timeout...Button OK IS NOT presented");
				throw new Exception();
			}
			driver.elementEventControling.click(PaymentProcessPage.btOK);

			// please wait process
			commonApp.pleaseWaitProcess("Loading Payment page");

			// verify system navigate to payment site
			String currentWindowTitle= driver.browserControling.getBrowserTitle();
			if(currentWindowTitle.equals(AppInit.paymentWindowTitle)) {
				utils.setTrueResult("Payment Page is loaded successful");
			}
			else {
				utils.setFalseResult("Payment Page IS NOT loaded successful");
			}
		}
		catch (Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	/**
	 * @Function: 		selectPaymentMethod
	 * @Description:	this function is used to select payment method on Payment process	
	 * @param 			paymentMethod: Visa -- MasterCard
	 * @throws 			Exception
	 */
	public void selectPaymentMethod(String paymentMethod) throws Exception {
		try {
			if(paymentMethod.equals(AppInit.paymentMethod[0]))
				driver.elementEventControling.click(PaymentPage.visaMethod);
			else if(paymentMethod.equals(AppInit.paymentMethod[1])) {
				driver.elementEventControling.click(PaymentPage.masterCardMethod);
			}
			else {
				utils.setFalseResult("Invalid data of Payment Method. It should be Visa or MasterCard value");
				throw new Exception();
			}
			utils.setTrueResult(MessageFormat.format("Payment Method: \"{0}\" is selected", paymentMethod));
		}
		catch (Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	/**
	 * @Function: 		inputPaymentDetail
	 * @Description:	this function is used to input card detail on Payment process	
	 * @param 			cardNo
	 * @param 			expiryMonth
	 * @param 			expiryYear
	 * @param 			securityCode
	 * @throws 			Exception
	 */
	public void inputPaymentDetail(String cardNo, String expiryMonth, String expiryYear, String securityCode) throws Exception{
		try {
			driver.elementEventControling.type(PaymentPage.txtCardNo, cardNo);
			driver.elementEventControling.type(PaymentPage.txtExpiryMonth, expiryMonth);
			driver.elementEventControling.type(PaymentPage.txtExpiryYear, expiryYear);
			driver.elementEventControling.type(PaymentPage.txtSecurityCode, securityCode);

			utils.setTrueResult("Payment Detail is entered");
		}
		catch (Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	/**
	 * @Function: 		payNewProposal
	 * @Description:	this function is used to click Pay button to proceed payment process on Payment process	
	 * @param 			none
	 * @throws 			Exception
	 */
	public void payNewProposal() throws Exception {
		try {
			// click on Pay button
			driver.elementEventControling.click(PaymentPage.btPay);
			driver.waitControling.waitPageLoadTimeOutBySecond(30);
			// verify transaction: timeout is 30 seconds
			if(this.vrfPaymentTransaction()) {
				utils.setFalseResult("Payment transaction IS NOT successful");
				throw new Exception();
			}
			// verify direct back OneLiberty site
			driver.waitControling.waitPageLoadTimeOutByMinute(30);
			if(!driver.elementChecking.waitForElementPresent(PA_NewProposalStep4.txtCustomerEmail, 30))
				utils.setFalseResult("Could not direct back OneLiberty site after proceeded payment");
			else
				utils.setTrueResult("Payment New Proposal is executed successful");					
		}
		catch (Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	/**
	 * @Function: 		vrfPaymentTransaction
	 * @Description:	this function is used to verify payment process on Payment process	
	 * @param 			none
	 * @throws 			Exception
	 */
	public boolean vrfPaymentTransaction() throws Exception {
		try {
			for (int second = 0;; second++) {
				if (second >= 30) { 
					utils.setFalseResult("Time out 30 seconds. Could not load Address Information after input Postal Code");
					return false;
				}
				else {
					String transResult = driver.elementEventControling.getValue(PaymentPage.paymentResult);
					if (!transResult.contains(AppInit.paymentTransResultText))
						return false;
				}
				Thread.sleep(1000);
			}
		}
		catch (Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return true;
	}

}
