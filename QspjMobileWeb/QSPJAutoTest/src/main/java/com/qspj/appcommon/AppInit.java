package com.qspj.appcommon;

import java.util.ArrayList;

import com.sai.configure.Environment;


@SuppressWarnings("javadoc")
public class AppInit extends Environment
{
	public enum LocatorType
	{
		ID, CLASS, CSS, XPATH
	}

	public static String imgSikuli = "/src/main/java/com/qspj/uimap/img/";

	public static String appTestScriptPackage = "com.qspj.testscript";
	public static ArrayList<ArrayList> allApplicationList = new ArrayList();

	public static String commonTestscriptClass = "com.qspj.testscript.CommonTestscript";
	public static String storefrontTestscriptClass = "com.qspj.testscript.StorefrontTestscript";
	

	public static ArrayList<String> availCommonScript = new ArrayList<String>();
	public static ArrayList<String> availStorefrontScript = new ArrayList<String>();
	public static ArrayList<String> availHMCScript = new ArrayList<String>();
	public static ArrayList<String> availProductCockpitScript = new ArrayList<String>();
	public static ArrayList<String> availCMSCockpitScript = new ArrayList<String>();
	public static ArrayList<String> availCSCockpitScript = new ArrayList<String>();


	// Default Window Title
	public static String storefrontWindowTitle = "Taiwan Site";
	public static String storefrontOrderConfirmationWindowTitle = "Order Confirmation | Taiwan Site";
	public static String hmcWindowTitle = "hybris Management Console (hMC)";
	public static String cmsCockpitWindowTitle = "Login";
	public static String csCockpitWindowTitle = "Cscockpit";
	public static String productCockpitWindowTitle = "";
	public static String ncccNotifyWindowTitle = "Notify MERCHANT";
	public static String ncccAdvertisingWindowTitle = "聯合信用卡處理中心";
	public static String ncccWindowTitle = "NCCC ePayment Hosted Pay Page";
	public static String cyberSourceWindowTitle = "";

	// For registered account
	public static String registeredUsername = "";
	public static String registeredEmail = "";
	public static String registeredPassword = "";

	// OTP Token
	public static String otpToken = "";

	public static String AdminUsername = "admin";
	public static String AdminPassword = "nimda";



}
