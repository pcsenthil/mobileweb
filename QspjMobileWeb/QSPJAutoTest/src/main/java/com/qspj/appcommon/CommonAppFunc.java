package com.qspj.appcommon;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.sai.configure.Environment;
import com.sai.configure.GlobalVariables;
import com.sai.utils.StackTraceInfo;


@SuppressWarnings("javadoc")
public class CommonAppFunc extends Environment
{
	public void switchToStorefrontWindow() throws Exception
	{
		try
		{
			this.switchToWindow(AppInit.storefrontWindowTitle);
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void switchToHMCWindow() throws Exception
	{
		try
		{
			this.switchToWindow(AppInit.hmcWindowTitle);
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void switchToWindow(final String windowTitle) throws Exception
	{
		try
		{
			boolean isWindowSelected = false;
			String currentWindowTitle = "";
			final Object[] allBrowserID = driver.browserControling.getBrowserIDs();
			for (int i = 0; i < allBrowserID.length; i++)
			{
				driver.browserControling.selectBrowser(allBrowserID[i].toString());
				currentWindowTitle = driver.browserControling.getBrowserTitle();
				if (currentWindowTitle.contains(windowTitle))
				{
					isWindowSelected = true;
					break;
				}
			}
			if (!isWindowSelected)
			{
				utils.setFalseResult(MessageFormat.format("\"{0}\" window IS NOT loaded", windowTitle));
			}
			else
			{
				utils.setTrueResult(MessageFormat.format("\"{0}\" window is selected and ready for work", windowTitle));
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void closeWindow(final String closeWindowTitle, final String switchNextWindowTitle) throws Exception
	{
		try
		{
			boolean isWindowClosed = false;
			String currentWindowTitle = "";
			final Object[] allBrowserID = driver.browserControling.getBrowserIDs();
			if (allBrowserID.length == 1)
			{
				currentWindowTitle = driver.browserControling.getBrowserTitle();
				if (currentWindowTitle.contains(closeWindowTitle))
				{
					driver.browserControling.closeBrowser(allBrowserID[1].toString());
				}
			}
			else
			{
				for (int i = 0; i < allBrowserID.length; i++)
				{
					driver.browserControling.selectBrowser(allBrowserID[i].toString());
					currentWindowTitle = driver.browserControling.getBrowserTitle();
					if (currentWindowTitle.contains(closeWindowTitle))
					{
						driver.browserControling.closeBrowser(allBrowserID[i].toString());
						isWindowClosed = true;
						break;
					}
				}
				if (!isWindowClosed)
				{
					utils.setFalseResult(MessageFormat.format("\"{0}\" window IS NOT closed or not existed", closeWindowTitle));
				}
				else
				{
					// switch to next window title
					this.switchToWindow(switchNextWindowTitle);
					utils.setTrueResult(
							MessageFormat.format("\"{0}\" window is closed and new Window Title \"{1}\" and ready for work",
									closeWindowTitle, switchNextWindowTitle));
				}
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void throwExceptionMess(final String functionName, final String message) throws Exception
	{
		if (GlobalVariables.result[0] == "" || GlobalVariables.result[0] == "true")
		{
			utils.setFalseResult(MessageFormat.format("Function: \"{0}\" throws exception: =: \"{1}\"", functionName, message));
		}
	}

	public ArrayList<String> getSubItemNameList(final String parentLocator) throws Exception
	{
		final ArrayList<String> subItemList = new ArrayList<String>();
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			// get List of menu item which have tag "li"
			final List<WebElement> menuListTagLI = eleMenu.findElements(By.tagName("li"));
			String menuText = "";
			for (int i = 0; i < menuListTagLI.size(); i++)
			{
				// get list of menu item which have tag "a"
				final WebElement menuListTagA = menuListTagLI.get(i).findElement(By.tagName("a"));
				menuText = menuListTagA.getText().toString().trim();
				if (!"".equals(menuText))
				{
					subItemList.add(menuText);
				}
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}

	public ArrayList<String> getTextSubItemNameListWithLI(final String parentLocator, final String childTag) throws Exception
	{
		final ArrayList<String> subItemList = new ArrayList<String>();
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			// get List of menu item which have tag "li"
			final List<WebElement> menuListTagLI = eleMenu.findElements(By.tagName("li"));
			String menuText = "";
			for (int i = 0; i < menuListTagLI.size(); i++)
			{
				// get list of menu item which have tag childTag
				final WebElement menuListTagA = menuListTagLI.get(i).findElement(By.tagName(childTag));
				menuText = menuListTagA.getText().toString().trim();
				if (!"".equals(menuText))
				{
					subItemList.add(menuText);
				}
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}

	public ArrayList<String> getTextSubItemNameList(final String parentLocator, final String childTag) throws Exception
	{
		final ArrayList<String> subItemList = new ArrayList<String>();
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			final List<WebElement> menuListTag = eleMenu.findElements(By.tagName(childTag));
			String menuText = "";
			for (int i = 0; i < menuListTag.size(); i++)
			{
				menuText = menuListTag.get(i).getText().toString().trim();
				if (!"".equals(menuText))
				{
					subItemList.add(menuText);
				}
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}
	
	
	public ArrayList<String> getTextSubItemNameList(final WebElement parentLocator, final String childTag) throws Exception
	{
		final ArrayList<String> subItemList = new ArrayList<String>();
		try
		{
			final List<WebElement> menuListTag = parentLocator.findElements(By.tagName(childTag));
			String menuText = "";
			for (int i = 0; i < menuListTag.size(); i++)
			{
				menuText = menuListTag.get(i).getText().toString().trim();
				if (!"".equals(menuText))
				{
					subItemList.add(menuText);
				}
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}

	public ArrayList<WebElement> getSubItemList(final String parentLocator) throws Exception
	{
		final ArrayList<WebElement> subItemList = new ArrayList<WebElement>();
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			// get List of menu item which have tag "li"
			final List<WebElement> menuListTagLI = eleMenu.findElements(By.tagName("li"));
			for (int i = 0; i < menuListTagLI.size(); i++)
			{
				// get list of menu item which have tag "a"
				final WebElement menuListTagA = menuListTagLI.get(i).findElement(By.tagName("a"));
				if (menuListTagA != null)
				{
					subItemList.add(menuListTagA);
				}
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}

	public ArrayList<WebElement> getSubItemListByTag(final String parentLocator, final String tagName) throws Exception
	{
		final ArrayList<WebElement> subItemList = new ArrayList<WebElement>();
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			final List<WebElement> tagList = eleMenu.findElements(By.tagName(tagName));
			for (int i = 0; i < tagList.size(); i++)
			{
				subItemList.add(tagList.get(i));
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}
	
	public ArrayList<WebElement> getSubItemListByClass(final String parentLocator, final String className) throws Exception
	{
		final ArrayList<WebElement> subItemList = new ArrayList<WebElement>();
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			final List<WebElement> eleList = eleMenu.findElements(By.className(className));
			for (int i = 0; i < eleList.size(); i++)
			{
				subItemList.add(eleList.get(i));
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}

	public ArrayList<WebElement> getSubItemListByTag(final WebElement parentElement, final String tagName) throws Exception
	{
		final ArrayList<WebElement> subItemList = new ArrayList<WebElement>();
		try
		{
			final List<WebElement> tagList = parentElement.findElements(By.tagName(tagName));
			for (int i = 0; i < tagList.size(); i++)
			{
				subItemList.add(tagList.get(i));
			}
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return subItemList;
	}

	public WebElement getFirstSubItemListByTag(final String parentLocator, final String tagName) throws Exception
	{
		WebElement firstSubItem = null;
		try
		{
			final WebElement eleMenu = driver.elementEventControling.getWebElementDisplay(parentLocator);
			firstSubItem = eleMenu.findElement(By.tagName(tagName));
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return firstSubItem;
	}

	public WebElement getFirstSubItemListByTag(final WebElement parentElement, final String tagName) throws Exception
	{
		WebElement firstSubItem = null;
		try
		{
			firstSubItem = parentElement.findElement(By.tagName(tagName));
		}
		catch (final Exception e)
		{
			this.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return firstSubItem;
	}
}
