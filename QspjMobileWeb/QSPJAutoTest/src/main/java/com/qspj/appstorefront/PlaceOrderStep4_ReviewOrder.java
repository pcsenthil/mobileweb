package com.qspj.appstorefront;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.uimap.locator.storefront.Step4FinalReviewPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class PlaceOrderStep4_ReviewOrder extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final PlaceOrder_ConfirmationPage confirmationPage = new PlaceOrder_ConfirmationPage();

	public void vrfOrderReviewPage() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.orderSummary)) {
				utils.setTrueResult("Order Summary Details are Displayed");
			} else {
				utils.setFalseResult("Order Summary Details are NOT Displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.chbAcceptTerms)) {
				utils.setTrueResult("Accept Terms and Conditions Checkbox is Displayed");
			} else {
				utils.setFalseResult("Accept Terms and Conditions Checkbox is NOT Displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.btPlaceOrder)) {
				utils.setTrueResult("Place Order Button is Displayed");
			} else {
				utils.setFalseResult("Place Order Button is NOT Displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.orderSummarySection)) {
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.orderListItems)) {
					utils.setTrueResult("List of Items for Order are displayed in Order Summary Section");
				}
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.OrderSummaryAddress)) {
					utils.setTrueResult("Shipping Address is Selected");
				}
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.OrderSummaryPaymentAddress)) {
					utils.setTrueResult("Billing Address & Payment Method  is Selected");
				}
			} else {
				utils.setFalseResult("Order Summary Section is not displayed");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void chkAcceptTermsConditions() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.chbAcceptTerms)) {
				driver.elementEventControling.click(Step4FinalReviewPage.chbAcceptTerms);
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void clickPlaceOrder(final String paymentMethod) throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.btPlaceOrder)) {
				driver.elementEventControling.click(Step4FinalReviewPage.btPlaceOrder);

				utils.setTrueResult("Place Order button is clicked");

				driver.waitControling.waitPageLoadTimeOutBySecond(10);
				driver.waitControling.sleep(3000);

				// Cash On Delivery
				if (ReqStorefrontData.paymentMethodList[2].equals(paymentMethod)) {
					final String actualURL = driver.browserControling.getBrowserURL();
					if (actualURL.contains(ReqStorefrontData.urlOrderConfirmationPage)) {
						utils.setTrueResult("Order Confirmation Page is displayed");
//						confirmationPage.vrfOrderConfirmationPage();
					} else {
						utils.setFalseResult("Order Confirmation Page is NOT displayed");
						throw new Exception();
					}
				}

//				// LinePay Payment
//				if (ReqStorefrontData.paymentMethodList[5].equals(paymentMethod)) {
//					final String actualURL = driver.browserControling.getBrowserURL();
//					if (actualURL.contains(ReqStorefrontData.urlLinePay)) {
//						utils.setTrueResult("LinePay payment page is displayed");
//					} else {
//						utils.setFalseResult("LinePay payment page IS NOT displayed");
//					}
//				}
//
//				// NCCC Payment
//				if (ReqStorefrontData.paymentMethodList[7].equals(paymentMethod)) {
//					final String actualURL = driver.browserControling.getBrowserURL();
//					if (actualURL.contains(ReqStorefrontData.urlNCCC)) {
//						utils.setTrueResult("NCCC payment page is displayed");
//					} else {
//						utils.setFalseResult("NCCC payment page IS NOT displayed");
//					}
//				}

				// CyberSource payment
				if (ReqStorefrontData.paymentMethodList[1].equals(paymentMethod)) {
					final String actualURL = driver.browserControling.getBrowserURL();
					if (actualURL.contains(ReqStorefrontData.urlCyberSource)) {
						utils.setTrueResult("CyberSource payment page is displayed");
					} else {
						utils.setFalseResult("CyberSource payment page IS NOT displayed");
					}
				}

//				// PayPal payment
//				if (ReqStorefrontData.paymentMethodList[6].equals(paymentMethod)) {
//					final String actualURL = driver.browserControling.getBrowserURL();
//					if (actualURL.contains(ReqStorefrontData.urlPayPal)) {
//						utils.setTrueResult("PayPal payment page is displayed");
//					} else {
//						utils.setFalseResult("PayPal payment page IS NOT displayed");
//					}
//				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

}
