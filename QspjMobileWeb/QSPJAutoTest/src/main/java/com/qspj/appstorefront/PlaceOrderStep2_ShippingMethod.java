package com.qspj.appstorefront;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.appdata.StorefrontDataVerification;
import com.qspj.uimap.locator.storefront.Step2ShipmentMethod;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class PlaceOrderStep2_ShippingMethod extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final StorefrontDataVerification datavrf = new StorefrontDataVerification();
	private final PlaceOrderStep3_PaymentMethod paymentMethod = new PlaceOrderStep3_PaymentMethod();

	private String setShipmentMethodValue(final String shipmentMethod) throws Exception {
		String value = null;
		if (shipmentMethod.equalsIgnoreCase("Standard Delivery")) {
			value = ReqStorefrontData.shipmentMethodValue[0];
		} else if (shipmentMethod.equalsIgnoreCase("Premium Delivery")) {
			value = ReqStorefrontData.shipmentMethodValue[1];
		}
		return value;
	}

	public void vrfShipmentMethodPage() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step2ShipmentMethod.itemList)) {
				utils.setTrueResult("Shipping Address & List of items are displayed");
			} else {
				utils.setFalseResult("Shipping Address & List of items are NOT displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step2ShipmentMethod.drpdwnShipmentMethod)) {
				utils.setTrueResult("Shipment Method Combo Box is displayed");
			} else {
				utils.setFalseResult("Shipment Method Combo Box is NOT displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step2ShipmentMethod.orderSummarySection)) {
				if (driver.elementChecking.isElementDisplay(Step2ShipmentMethod.orderListItems)) {
					utils.setTrueResult("List of Items for Order are displayed in Order Summary Section");
				}
				if (driver.elementChecking.isElementDisplay(Step2ShipmentMethod.OrderSummaryAddress)) {
					utils.setTrueResult("Shipping Address is Selected");
				}
			} else {
				utils.setFalseResult("Order Summary Section is not displayed");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void selectShipmentMethod(final String shipmentMethod) throws Exception {
		try {
			if (!datavrf.vrfShipmentMethodData(shipmentMethod)) {
				utils.setFalseResult("Invalid Shipment Method Type Data");
			} else {

				String value = this.setShipmentMethodValue(shipmentMethod);
				driver.elementEventControling.selectValue(Step2ShipmentMethod.drpdwnShipmentMethod, value);
				utils.setTrueResult("Shipment Method selected");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickNext() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step2ShipmentMethod.btnNext)) {
				driver.elementEventControling.click(Step2ShipmentMethod.btnNext);
				utils.setTrueResult("Next button is clicked");

				final String actualURL = driver.browserControling.getBrowserURL();
				if (actualURL.contains(ReqStorefrontData.urlPaymentMethodPage)) {
					utils.setTrueResult("Payment Method Page is displayed");
//					paymentMethod.vrfPaymentMethodPage();
				} else {
					utils.setFalseResult("Payment Method Page is not displayed");
					throw new Exception();
				}
			} else {
				utils.setFalseResult("Next Button is NOT displayed");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

}
