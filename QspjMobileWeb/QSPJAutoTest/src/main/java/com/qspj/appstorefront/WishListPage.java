package com.qspj.appstorefront;

public class WishListPage {

	// Wishlist
		public static String lnkDefaultWishlist = "//a[@href='/qspjstorefront/qspj/en/my-account/wishlist/defaultWishlist?wishlistName=Default Wishlist']";
		public static String lnkPrivateWishlist = "//a[@href='/qspjstorefront/qspj/en/my-account/wishlist/defaultWishlist?wishlistName=Senthil Private']";
		public static String lnkPublictWishlist = "//a[@href='/qspjstorefront/qspj/en/my-account/wishlist/defaultWishlist?wishlistName=Senthil Public']";

		public static String defmsg = "//div[@class='wishlist-noproduct-msg']";


		// public static String btnDeleteitem="//a[@class='glyphicon
		// glyphicon-remove']";
		public static String btnDeleteitem = "//div[@class='removebtn']/a";

		public static String listingItemsInWishlist = "//div[@class='listing-wishlist']";
		public static String itemRemoveClassName = "wishlist-remove";
		public static String btnDeletePublicWishlist = "//div[@class='listing-wishlist']/li[3]/span[2]/a/i";

		// public static String
		// txtExistPublicWishlist="//span[@class='wishlist-name']/a";
		public static String texPublicwishlist = "//div[@class='listing-wishlist']/li[3]/span[2]/a/i";
		public static String btnWishlistAddToCart = "//button[@id='saveCartButton']";
		// Find A Wishlist
		public static String texWishlistEmail = "//form[@id='addToWishListDropDown']/div[1]/div[1]/div/div/input";
		public static String btnFindNow = "//form[@id='addToWishListDropDown']/div[1]/div[2]/input";
		public static String defmsgFindWishlist = "//div[@wishlist-top-nav']/div";
		public static String lnkSeeAllWishlists = "//a[@class='js-see-all-lists']";
		public static String btnOthersWishlistAddToCart = "//button[@id='saveCartButton']";
}
