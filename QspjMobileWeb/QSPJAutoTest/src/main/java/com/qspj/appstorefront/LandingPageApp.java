package com.qspj.appstorefront;

import java.text.MessageFormat;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.StorefrontDataVerification;
import com.qspj.uimap.locator.storefront.LandingPage;
import com.qspj.uimap.locator.storefront.LoggedInPage;
import com.qspj.uimap.locator.storefront.SignInAndRegisterPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class LandingPageApp extends Environment {
	private static final String phone = null;
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final StorefrontDataVerification storeFrontDataVrf = new StorefrontDataVerification();

	public void gotoSignInRegisterPage() throws Exception {
		try {
			this.selectMenu();
			// click on SignIn/Register link on header
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkSignInRegister)) {
				this.signOut();
			} else {
				driver.elementEventControling.click(LandingPage.lnkSignInRegister);
				driver.waitControling.waitPageLoadTimeOutBySecond(15);
				// verify SignIn/Register page is displayed
				if (driver.elementChecking.waitForElementPresent(SignInAndRegisterPage.btLogin, 15)) {
					utils.setTrueResult("SignIn/Register page is displayed correct");
				} else {
					utils.setFalseResult("SignIn/Register page IS NOT displayed correct");
				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	public void selectMenu()throws Exception {
		try {
			
			//Click on Menu 
			if(!driver.elementChecking.isElementDisplay(LandingPage.btnMenu)) {
				utils.setFalseResult("Menu button is NOT Displayed");
			}else {
				driver.elementEventControling.click(LandingPage.btnMenu);
				driver.waitControling.webDriverWaitElementLocated(LandingPage.menuList, 5);
				utils.setTrueResult("Menu List is Displayed");
			}
		}
		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		
	}
	
	public void closeMenu()throws Exception {
		try {
			
			if(!driver.elementChecking.isElementDisplay(LandingPage.menuList)) {
				utils.setTrueResult("Menu List is NOT Displayed");
			}else {
				//Click on Menu 
				if(!driver.elementChecking.isElementDisplay(LandingPage.btnMenuClose)) {
					utils.setFalseResult("Close button is NOT Displayed");
				}else {
					driver.elementEventControling.click(LandingPage.btnMenuClose);
					driver.waitControling.webDriverWaitElementLocated(LandingPage.btnMenu, 5);
					utils.setTrueResult("Menu is Closed");
				}
			}
		}
		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		
	}
	
	
	public void selectSearchIcon()throws Exception {
		try {
			
			//Click on Menu 
			if(!driver.elementChecking.isElementDisplay(LandingPage.btnSearch)) {
				utils.setFalseResult("Search button is NOT Displayed");
			}else {
				driver.elementEventControling.click(LandingPage.btnSearch);
				driver.waitControling.webDriverWaitElementLocated(LandingPage.txtSearch, 5);
				utils.setTrueResult("Search Text Box is Displayed");
			}
		}
		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		
	}
	

	public void signOut() throws Exception {
		try {
			// click on Sign Out link
			if (!driver.elementChecking.isElementDisplay(LoggedInPage.lnkSignOut)) {
				utils.setFalseResult("Sign out link IS NOT displayed");
				throw new Exception();
			}

			else {
				driver.elementEventControling.click(LoggedInPage.lnkMyAccount);
				driver.elementEventControling.click(LoggedInPage.lnkSignOut);
			}

			// verify login success or not
			driver.waitControling.waitPageLoadTimeOutBySecond(10);
			if (driver.elementChecking.waitForElementPresent(LandingPage.lnkSignInRegister, 15)) {
				utils.setTrueResult("Logged out successful");
			} else {
				utils.setFalseResult("Logged out unsuccessful");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void gotoNavItemMyAccount(final String MyAccountnavItemName) throws Exception {
		try {
			this.selectMenu();
			boolean isItemDisplay = false;
			if (driver.elementChecking.isElementDisplay(LandingPage.lnkMyAccount)) {
				driver.elementEventControling.click(LandingPage.lnkMyAccount);

				final List<WebElement> leftNavList = commonApp.getSubItemListByTag(LandingPage.lnkListMyAccount, "li");

				for (int i = 0; i < leftNavList.size(); i++) {
					if (!isItemDisplay) {
						int temp = i + 1;
						String linkXpath = LandingPage.lnkListMyAccount + "/li[" + temp + "]/a";

						String linkTitle = driver.elementEventControling.getAttribute(linkXpath, "title").trim();

						if (MyAccountnavItemName.equalsIgnoreCase(linkTitle)) {
							isItemDisplay = true;
							driver.elementEventControling.click(linkXpath);
						}
					}
				}
				if (!isItemDisplay) {
					utils.setFalseResult(MessageFormat.format("Could not find \"{0}\" link under My Account Menu",
							MyAccountnavItemName));
				}
			} else {
				utils.setFalseResult("Could not find My Account Menu");
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	// Update Personal Details

	public void updatePersonalDetails(final String title, final String firstName, final String lastName)
			throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkUpdatePersonalDetails)) {
				utils.setFalseResult("Update Personal Details link is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkUpdatePersonalDetails);
				utils.setTrueResult("Personal Details are Displayed");
			}

			if (storeFrontDataVrf.vrfTitleData(title)) {
				if (!driver.elementChecking.isElementDisplay(LandingPage.ddTitle)) {
					utils.setFalseResult("Title dropdown is not displayed");
					throw new Exception();
				} else {
					driver.elementEventControling.select(LandingPage.ddTitle, title);
				}
			} else {
				utils.setFalseResult("Invalid Title Data");
			}

			// Enter First Name

			if (!driver.elementChecking.isElementDisplay(LandingPage.txtFirstName)) {
				utils.setFalseResult("First Name Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtFirstName, firstName);
			}

			// Enter Last Name
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtLastName)) {
				utils.setFalseResult("Last Name Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtLastName, lastName);
			}
			// Select Update Button
			if (!driver.elementChecking.isElementDisplay(LandingPage.btSubmit)) {
				utils.setFalseResult("Register Button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(LandingPage.btSubmit);
			}

			driver.waitControling.implicitlyWaitBySecond(5);
			this.vrfProfileUpdate();

		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfProfileUpdate() throws Exception {
		if (driver.elementChecking.isElementDisplay(LandingPage.alertSuccess)) {
			utils.setTrueResult("profile has been updated. Success Message displayed");

		} else {
			utils.setFalseResult("profile has not been updated");
			throw new Exception();
		}

	}

	public void selectShopAllDeptMenu() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkShopAllDepartment)) {
				utils.setFalseResult("Shop All Departments Menu is NOT Displayed");
			}

			driver.elementEventControling.click(LandingPage.lnkShopAllDepartment);
			if (driver.elementChecking.isElementDisplay(LandingPage.lnkListShopAllDept)) {
				utils.setTrueResult("Shop All Departments Menu is Clicked and Category list is Displayed");
			}

		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void gotoNavItemShopAllDept(final String categoryMenu) throws Exception {
		try {
			this.selectShopAllDeptMenu();
			List<WebElement> categoryList = commonApp.getSubItemListByTag(LandingPage.lnkListShopAllDept, "li");

			for (int i = 0; i < categoryList.size(); i++) {
				int temp = i + 1;
				String xpath = LandingPage.lnkListShopAllDept + "/li[" + temp + "]/span[1]/a";
				String title = driver.elementEventControling.getText(xpath);
				if (title.equalsIgnoreCase(categoryMenu)) {
					driver.elementEventControling.click(xpath);
					utils.setTrueResult(MessageFormat
							.format("Selected the Menu :  \"{0}\" under 'Shop All Departments' Menu", categoryMenu));
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void gotoSubNavItemShopAllDept(final String categoryMenu, final String subCategoryMenu) throws Exception {
		try {
			boolean vrf = false;
			this.selectShopAllDeptMenu();
			List<WebElement> categoryList = commonApp.getSubItemListByTag(LandingPage.lnkListShopAllDept, "li");

			for (int i = 0; i <= categoryList.size(); i++) {
				int temp = i + 1;
				String xpath = LandingPage.lnkListShopAllDept + "/li[" + temp + "]/span[1]/a";
				String title = driver.elementEventControling.getText(xpath).trim();
				if (title.equalsIgnoreCase(categoryMenu)) {
					vrf = true;
					driver.elementEventControling.mouseOver(xpath);
					driver.waitControling.sleep(2000);
					utils.setTrueResult(MessageFormat
							.format("Selected the Menu :  \"{0}\" under 'Shop All Departments' Menu", categoryMenu));

					String subXpath = LandingPage.lnkListShopAllDept + "/li[" + temp + "]/div/div/div/ul";

					List<WebElement> subCategoryList = commonApp.getSubItemListByTag(subXpath, "li");
					for (int j = 0; j <= subCategoryList.size(); j++) {
						int temp1 = j + 1;
						String xpath1 = subXpath + "/li[" + temp1 + "]/a";
						String title1 = driver.elementEventControling.getText(xpath1).trim();
						if (title1.equalsIgnoreCase(subCategoryMenu)) {
							driver.elementEventControling.click(xpath1);
							utils.setTrueResult(
									MessageFormat.format("Selected the Sub Category Menu :  \"{0}\" under \"{1}\" Menu",
											subCategoryMenu, categoryMenu));
							break;
						}
					}
				}
				if (vrf) {
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void forgotYourPasswordPopup() throws Exception {

		try {

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtForgotPasswordPopup)) {
				utils.setFalseResult("Forgot password popup is not displayed");
				throw new Exception();
			} else {
				driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtForgotPasswordPopup);
				driver.waitControling.waitPageLoadTimeOutBySecond(15);

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}

	}

	
	public void changeYourPassword(final String currentPassword, final String newPassword, final String confNewPassword)
			throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkChangeYourPassword)) {
				utils.setFalseResult("Change Password link is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkChangeYourPassword);
				utils.setTrueResult("Change Password link is Displayed");
			}

			// Enter Current password

			if (!driver.elementChecking.isElementDisplay(LandingPage.txtCurrentPassword)) {
				utils.setFalseResult("Current password Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtCurrentPassword, currentPassword);
			}

			// Enter New Password
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtNewPassword)) {
				utils.setFalseResult(" New Password Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtNewPassword, newPassword);
			}
			// Enter Confirm New Password
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtConfirmNewPassword)) {
				utils.setFalseResult("  Confirm New Password Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtConfirmNewPassword, confNewPassword);
			}
			// Select Update Button
			if (!driver.elementChecking.isElementDisplay(LandingPage.btPwdSubmit)) {
				utils.setFalseResult("Update Button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(LandingPage.btPwdSubmit);
			}

			driver.waitControling.implicitlyWaitBySecond(5);
			// this.vrfProfileUpdate();

		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void updateYourEmail(final String newEmailAddress, final String confirmNewEmail, final String password)
			throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkChangeYourEmail)) {
				utils.setFalseResult("Change Email link is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkChangeYourEmail);

				utils.setTrueResult("Change Email link is Displayed");
			}

			// Enter New Email Address

			if (!driver.elementChecking.isElementDisplay(LandingPage.txtNewEmail)) {
				utils.setFalseResult("New Email Address Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtNewEmail, newEmailAddress);
			}

			// Enter Confirm New Email Address
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtConfEmail)) {
				utils.setFalseResult(" Confirm Email Address Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtConfEmail, confirmNewEmail);
			}
			// Enter Profile Password
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtPassword)) {
				utils.setFalseResult("Password Text box is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(LandingPage.txtPassword, password);
			}
			// Select Update Button
			if (!driver.elementChecking.isElementDisplay(LandingPage.btChangeEmailSubmit)) {
				utils.setFalseResult("Update Button is not displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(LandingPage.btChangeEmailSubmit);
			}

			driver.waitControling.implicitlyWaitBySecond(5);
			// this.vrfProfileUpdate();

		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		
	}
	

	public void gotoMyAccount()
			throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(LandingPage.lnkMyAccount)) 
			{
				utils.setFalseResult("My Account link is NOT Displayed");
			} else {
				driver.elementEventControling.click(LandingPage.lnkMyAccount);
				driver.waitControling.waitPageLoadTimeOutBySecond(15);
				utils.setTrueResult("My Account link is NOT Displayed and clicked");
			}
		}
			catch (final Exception e) {
				log.error(StackTraceInfo.getCurrentClassName());
				log.error(StackTraceInfo.getCurrentFileName());
				log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
	}
}
}
