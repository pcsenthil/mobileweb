package com.qspj.appstorefront;

import java.text.MessageFormat;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.uimap.locator.storefront.OrderConfirmationPage;
import com.qspj.uimap.locator.storefront.Step4FinalReviewPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class PlaceOrder_ConfirmationPage extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final LandingPageApp landingPage = new LandingPageApp();
	private final OrderHistoryApp orderHistoryApp = new OrderHistoryApp();
	public String orderNo;

	public void vrfOrderConfirmationPage() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.orderSummarySection)) {
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.orderListItems)) {
					utils.setTrueResult("List of Items Ordered are displayed in Order Summary Section");
				}
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.OrderSummaryAddress)) {
					utils.setTrueResult("Shipping Address is Displayed");
				}
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.OrderSummaryPaymentAddress)) {
					utils.setTrueResult("Billing Address is Displayed");
				}
				if (driver.elementChecking.isElementDisplay(Step4FinalReviewPage.OrderSummaryTotal)) {
					utils.setTrueResult("Order Details(Total) are displayed");
				}
			} else {
				utils.setFalseResult("Order Summary Section is not displayed");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfOrder() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(OrderConfirmationPage.messOrderSuccess)) {
				utils.setTrueResult(driver.elementEventControling.getText(OrderConfirmationPage.messOrderSuccess));
				orderNo = this.getOrderNo();
				utils.setTrueResult(MessageFormat.format("Order \"{0}\" is placed successful.", orderNo));
			}

			this.vrfOrderInOrderHistory(orderNo);			
			
			
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
//	public void vrfOrderConfEmail(final String orderID) throws Exception{
//		try{
//			//Wait for Email
//			Thread.sleep(10000);
//			emailHelper.verifyEmail(emailHelper.getMailSubject("Order Confirmation", orderID));
//		}
//		 catch (final Exception e) {
//			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
//			log.error(StackTraceInfo.getCurrentClassName());
//			log.error(StackTraceInfo.getCurrentFileName());
//			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
//		}
//	}

	public void vrfOrderForGuestUser() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(OrderConfirmationPage.messOrderSuccess)) {
				utils.setTrueResult(driver.elementEventControling.getText(OrderConfirmationPage.messOrderSuccess));
				orderNo = this.getOrderNo();
				utils.setTrueResult(MessageFormat.format("Order \"{0}\" is placed successful.", orderNo));
			}

			if (driver.elementChecking.isElementDisplay(OrderConfirmationPage.guestNewAccount)) {
				if (driver.elementChecking.isElementDisplay(OrderConfirmationPage.guestNewAccountForm)) {
					utils.setTrueResult("Create An Account Block is displayed in Confirmation page for Guest Users");
				}
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public String getOrderNo() throws Exception {
		orderNo = "";

		try {
			if (!driver.elementChecking.isElementDisplay(OrderConfirmationPage.messOrderNo, false)) {
				utils.setFalseResult("Order Number IS NOT displayed in Confirmation Screen");
				throw new Exception();
			} else {
				// get Order No
				orderNo = driver.elementEventControling.getText(OrderConfirmationPage.messOrderNo).trim();
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return orderNo;

	}

	public void vrfOrderInOrderHistory(final String orderID) throws Exception {
		try {
			driver.elementEventControling.click(OrderConfirmationPage.btContinueShopping);
			driver.waitControling.waitPageLoadTimeOutBySecond(10);
			landingPage.gotoNavItemMyAccount("Order History");
			orderHistoryApp.searchOrderID(orderID);
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

	}

}
