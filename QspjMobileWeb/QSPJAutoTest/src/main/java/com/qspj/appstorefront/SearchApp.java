package com.qspj.appstorefront;

import java.text.MessageFormat;

import java.util.ArrayList;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.uimap.locator.storefront.LandingPage;
import com.qspj.uimap.locator.storefront.SearchResultPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

@SuppressWarnings("javadoc")

public class SearchApp extends Environment {
	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final ProductDetailPageApp pdpApp = new ProductDetailPageApp();
	private final LandingPageApp landingPageApp = new LandingPageApp();

	public void searchProductByName(final String productName) throws Exception {
		try {
			landingPageApp.selectSearchIcon();
			boolean searchResult = false;
			String productTitle = "";
			ArrayList<WebElement> productList = new ArrayList<WebElement>();
			// input product name into Search text field
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtSearch)) {
				utils.setFalseResult("Search text field IS NOT displayed");
				throw new Exception();
			}
			driver.elementEventControling.type(LandingPage.txtSearch, productName);
			driver.elementEventControling.click(LandingPage.iconSearch);
			driver.waitControling.waitPageLoadTimeOutBySecond(15);
			driver.waitControling.sleep(5000);
			// verify search result is not found
			if (driver.elementChecking.isElementDisplay(SearchResultPage.lbSearchEmpty, false)) {
				utils.setFalseResult(MessageFormat.format("Search Empty with search text:\"{0}\"", productName));
			}
			// click Show all link if have
			if (driver.elementChecking.isElementDisplay(SearchResultPage.lbSearchResults, false)) {
				utils.setTrueResult(
						MessageFormat.format("Search Results displayed for search text:\"{0}\"", productName));
			}
			// get product list item
			productList = commonApp.getSubItemListByTag(SearchResultPage.lstProductList, "div");

			for (int i = 0; i < productList.size(); i++) {
				int temp = 0;
				temp = i + 1;
				String titlexpath = SearchResultPage.lstProductList + "/div[" + temp + "]/div/div[1]/a";

				// productTitle =
				// productList.get(i).getAttribute("title").toString().trim();
				productTitle = driver.elementEventControling.getText(titlexpath).toString().trim();
				if (productTitle.contains(productName)) {
					searchResult = true;
					break;
				}
			}
			if (searchResult) {
				utils.setTrueResult(MessageFormat.format(
						"Products matched with search text:\"{0}\" are displayed in Search Results", productName));
			} else {
				utils.setFalseResult(
						MessageFormat.format("Could not found product with search text:\"{0}\"", productName));
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	
	public void searchProductByNameAutoComplete(final String productName) throws Exception
	 {
	  try
	  {
		  landingPageApp.selectSearchIcon();
			boolean searchResult = false;
			String productTitle = "";
			ArrayList<WebElement> productList = new ArrayList<WebElement>();
			// input product name into Search text field
			if (!driver.elementChecking.isElementDisplay(LandingPage.txtSearch)) {
				utils.setFalseResult("Search text field IS NOT displayed");
				throw new Exception();
			}
			driver.elementEventControling.type(LandingPage.txtSearch, productName);
			driver.waitControling.webDriverWaitElementLocated(LandingPage.searchAutoCompleteList, 5);
			// get product list item
			productList = commonApp.getSubItemListByTag(LandingPage.searchAutoCompleteList, "li");

			for (int i = 0; i < productList.size(); i++) {
				int temp = 0;
				temp = i + 1;
				 String titlexpath = LandingPage.searchAutoCompleteList + "/li[" + temp + "]/a/div[2]";

				productTitle = driver.elementEventControling.getText(titlexpath).toString().trim();
				if (productTitle.contains(productName)) {
					searchResult = true;
					String linkxpath = LandingPage.searchAutoCompleteList + "/li[" + temp + "]/a";
					driver.elementEventControling.click(linkxpath);
					
					driver.waitControling.waitPageLoadTimeOutBySecond(20);
					utils.setTrueResult(MessageFormat.format("PDP of Product name:\"{0}\" is loaded correct", productName));
					break;
				}
			}
			if (searchResult) {
				utils.setTrueResult(MessageFormat.format(
						"Products matched with search text:\"{0}\" are displayed in Search Results", productName));
			} else {
				utils.setFalseResult(
						MessageFormat.format("Could not found product with search text:\"{0}\"", productName));
			}
	  }
	   
	   catch (final Exception e)
	   {
	    commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
	    log.error(StackTraceInfo.getCurrentClassName());
	    log.error(StackTraceInfo.getCurrentFileName());
	    log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
	   }
	  }
	
	public boolean gotoPDPOnSearchResult(final String product) throws Exception {
		try {
			boolean searchResult = false;
			ArrayList<WebElement> productList = new ArrayList<WebElement>();

			// get product list item
			productList = commonApp.getSubItemListByTag(SearchResultPage.lstProductList, "div");
			// searching with productID or product Name
			for (int i = 0; i < productList.size(); i++) {
				int temp = 0;
				temp = i + 1;
				String xpath = SearchResultPage.lstProductList + "/div[" + temp + "]/div/div[1]/a";

				final String hrefAtt = driver.elementEventControling.getAttribute(xpath, "href");

				final String productTitle = driver.elementEventControling.getText(xpath).toString().trim();
				if (hrefAtt.contains("/p/" + product) || product.equals(productTitle)) {
					searchResult = true;
					// click on href link
					driver.elementEventControling.click(xpath);
					break;
				}
			}
			if (searchResult) {
				// verify PDP display
				driver.waitControling.waitPageLoadTimeOutBySecond(20);
				driver.waitControling.sleep(3000);
				if (pdpApp.vrfProductName(product)) {
					utils.setTrueResult(MessageFormat.format("PDP of Product name:\"{0}\" is loaded correct", product));
					return true;
				} else {
					utils.setFalseResult(
							MessageFormat.format("Product name:\"{0}\" IS NOT displayed correct on PDP", product));
				}
			} else {
				utils.setFalseResult(MessageFormat
						.format("Could not find product on Search Result page with ProductName :\"{0}\"", product));
			}
		} catch (TimeoutException te) {
			driver.waitControling.sleep(5000);
			driver.browserControling.refreshPage();
			driver.waitControling.waitPageLoadTimeOutBySecond(20);
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

		return false;
	}

	public void addProductToCartByName(final String productName, final String quantity) throws Exception {
		try {
			this.searchProductByName(productName);		
			this.gotoPDPOnSearchResult(productName);
			pdpApp.addProductToCart(quantity);
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	public void addProductToCartByNameAC(final String productName, final String quantity) throws Exception {
		try {
			this.searchProductByNameAutoComplete(productName);		
			pdpApp.addProductToCart(quantity);
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

}
