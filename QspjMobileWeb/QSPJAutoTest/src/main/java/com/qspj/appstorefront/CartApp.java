package com.qspj.appstorefront;

import java.text.MessageFormat;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.uimap.locator.storefront.CartPage;
import com.qspj.uimap.locator.storefront.CartPopup;
import com.qspj.uimap.locator.storefront.LandingPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

@SuppressWarnings("javadoc")
public class CartApp extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final PlaceOrderStep1_DeliveryAddress deliveryApp = new PlaceOrderStep1_DeliveryAddress();

	public void openCartPopup() throws Exception {
		try {
			
			if(!driver.browserControling.getBrowserURL().contains(ReqStorefrontData.urlCartPage)){
			
				// click on cart icon on heade
				if (!driver.elementChecking.isElementDisplay(LandingPage.iconCart)) {
					utils.setFalseResult("Cart Icon IS NOT displayed on page header");
					throw new Exception();
				} else {
					driver.elementEventControling.click(LandingPage.iconCart);
					driver.waitControling.sleep(2000);
				}
				// verify the cart popup displays
				if (driver.elementChecking.waitForElementPresent(CartPopup.btContinueShopping, 15)) {
					utils.setTrueResult("Cart popup is opened");
				} else {
					utils.setFalseResult("Could not open Cart popup");
				}
			}
			else
			{
				log.info("Already in Cart Page");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void closeCartPopup() throws Exception {
		try {

			// click on Close button On Cart popup
			if (!driver.elementChecking.isElementDisplay(CartPopup.btClose)) {
				utils.setFalseResult("Close button IS NOT displayed on Cart popup");
				throw new Exception();
			} else {
				driver.elementEventControling.click(CartPopup.btClose);
				driver.waitControling.sleep(3000);
			}
			// verify the Cart popup is closed
			if (driver.elementChecking.isElementDisplay(CartPopup.btClose)) {
				utils.setFalseResult("Could not close Cart popup");
			} else {
				utils.setTrueResult("Cart popup is closed");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public int countCartList() throws Exception {

		int countProd = 0;
		try {
			// get List of menu item which have tag "li"
			final List<WebElement> cartListTagLI = commonApp.getSubItemListByClass(CartPage.listItemsInCart,
					CartPage.itemEntryClassName);
			countProd = cartListTagLI.size();
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return countProd;
	}

	public void clickCheckOutOnCartPopup() throws Exception {
		try {
			if(!driver.browserControling.getBrowserURL().contains(ReqStorefrontData.urlCartPage)){
				
				// click on ShoppingCart button On Cart pop-up
				if (!driver.elementChecking.isElementDisplay(CartPopup.btCheckoutMiniCart, false)) {
					utils.setFalseResult("Checkout button IS NOT displayed on Cart popup");
					throw new Exception();
				} else {
					driver.elementEventControling.click(CartPopup.btCheckoutMiniCart);
					driver.waitControling.waitPageLoadTimeOutBySecond(10);
					if (driver.browserControling.getBrowserURL().contains(ReqStorefrontData.urlCartPage)) {
						utils.setTrueResult("Cart Page is Displayed");
					}
				}
			}
			else
			{
				log.info("Already in Cart Page");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfCartPage() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(CartPage.btnCheckout1)
					&& driver.elementChecking.isElementDisplay(CartPage.btnCheckout2)) {
				utils.setTrueResult("Checkout button IS displayed at two places");
			} else {
				utils.setFalseResult("Checkout button IS NOT displayed in atleast one place");
				throw new Exception();
			}

			if (driver.elementChecking.isElementDisplay(CartPage.btnConShopping)) {
				utils.setTrueResult("Continue Shopping link IS displayed");
			} else {
				utils.setFalseResult("Continue Shopping link IS NOT displayed");
				throw new Exception();
			}

			if (driver.elementChecking.isElementDisplay(CartPage.listItemsInCart)) {
				utils.setTrueResult("Item list is displayed");
			} else {
				utils.setFalseResult("Item list is NOT displayed");
				throw new Exception();
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

	}

	public void deleteItemInCart() throws Exception {
		try {
			// verify Remove product icon
			if (!driver.elementChecking.isElementDisplay(CartPage.btnDeleteitem)) {
				utils.setFalseResult("Remove Product Icon of  IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(CartPage.btnDeleteitem);
				utils.setTrueResult("Remove Product icon is clicked correct");

				if (driver.elementEventControling.getText(CartPage.alertInfo)
						.contains("Product has been removed from your cart")) {
					utils.setTrueResult("'Product has been removed from your cart' alert is displayed");
				} else {
					utils.setFalseResult("'Product has been removed from your cart' alert is not displayed");
				}
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

	}

	public void removeAllProducts() throws Exception {
		try {
			// click Cart icon on header
			this.openCartPopup();
			if (driver.elementChecking.isElementDisplay(CartPage.emptyCartBanner)) {
				utils.setTrueResult("Cart is Empty");
				driver.elementEventControling.click(CartPage.btnClosePopup);
			} else {
				this.clickCheckOutOnCartPopup();

				// get product count on cart list
				final int countProd = this.countCartList();
				if (countProd == 0) {
					utils.setTrueResult("Cart List is empty");
				} else {
					for (int i = 1; i <= countProd; i++) {
						// click remove icon on each product to remove
						this.deleteItemInCart();
					}

				}

				if (driver.elementChecking.isElementDisplay(CartPage.messEmptyCart)) {
					utils.setTrueResult("Cart is empty. All products are removed successfully");
				} else {
					utils.setFalseResult("Cart is not empty. Still products are available in the Cart");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfProductTotalPrice(final String productName, final String totalprice) throws Exception {
		try {
			
			final int countProd = this.countCartList();
			if (countProd == 0) {
				utils.setTrueResult("Cart List is empty");
			} else {
				for (int i = 1; i <= countProd; i++) {

					String productNameXpath = CartPage.listItemsInCart + "/li[@class='" + CartPage.itemEntryClassName
							+ "'][" + i + "]/div[2]/div/a/span";
					if (driver.elementEventControling.getText(productNameXpath).trim().equalsIgnoreCase(productName)) {

						utils.setTrueResult("Product is Available in the Cart");
						String productTotalPriceXpath = CartPage.listItemsInCart + "/li[@class='"
								+ CartPage.itemEntryClassName + "'][" + i + "]/div[2]/div[3]/div[3]";
						String price = driver.elementEventControling.getText(productTotalPriceXpath).trim().substring(1).replace(",", "");
						if (price.equals(totalprice)) {
							utils.setTrueResult(MessageFormat.format(
									"Total Price for the Product : \"{0}\" is correct (\"{1}\")", productName,
									driver.elementEventControling.getText(productTotalPriceXpath).trim()));
						} else {
							utils.setFalseResult(MessageFormat.format(
									"Total Price for the Product : \"{0}\" is NOT correct (\"{1}\")", productName,
									driver.elementEventControling.getText(productTotalPriceXpath).trim()));
						}
					}

				}

			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfOrderTotal() throws Exception {
		int subtotal = 0;
		int discount = 0;
		int total = 0;
		try {
			if (!driver.elementChecking.isElementDisplay(CartPage.lblSubtotal)) {
				utils.setFalseResult("Order Subtotal IS NOT Displayed");
			} else {
				utils.setTrueResult(MessageFormat.format("Subtotal for the order is \"{0}\"",
						driver.elementEventControling.getText(CartPage.lblSubtotal).trim()));

				subtotal = Integer.parseInt(driver.elementEventControling.getText(CartPage.lblSubtotal).trim()
						.substring(1).replaceAll(",", ""));
			}

			if (!driver.elementChecking.isElementDisplay(CartPage.lblDiscountValue)) {
				if (driver.elementEventControling.getText(CartPage.lblSubtotal).trim()
						.equalsIgnoreCase(driver.elementEventControling.getText(CartPage.lblOrderTotal))) {
					utils.setTrueResult(MessageFormat.format("Order Total for the order is Correct : \"{0}\"",
							driver.elementEventControling.getText(CartPage.lblOrderTotal).trim()));
				} else {
					utils.setFalseResult(MessageFormat.format("Order Total for the order is NOT Correct : \"{0}\"",
							driver.elementEventControling.getText(CartPage.lblOrderTotal).trim()));
				}
			} else {
				utils.setTrueResult(MessageFormat.format("Discount Value for the order is \"{0}\"",
						driver.elementEventControling.getText(CartPage.lblDiscountValue).trim()));
				discount = Integer.parseInt(driver.elementEventControling.getText(CartPage.lblDiscountValue).trim()
						.substring(1).replaceAll(",", ""));
				total = Integer.parseInt(driver.elementEventControling.getText(CartPage.lblOrderTotal).trim()
						.substring(1).replaceAll(",", ""));

				if ((subtotal - discount) == total) {
					utils.setTrueResult(MessageFormat.format(
							"Order Total for the order is Correct : \"{0}\" with Discount of \"{1}\"",
							driver.elementEventControling.getText(CartPage.lblOrderTotal).trim(), discount));
				} else {
					utils.setFalseResult(MessageFormat.format("Order Total for the order is NOT Correct : \"{0}\"",
							driver.elementEventControling.getText(CartPage.lblOrderTotal).trim()));
				}
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickCheckOutOnCartPageNotLogin() throws Exception {
		try {
			// click on Checkout button on Cart page
			if (!driver.elementChecking.isElementDisplay(CartPage.btnCheckout1)) {
				utils.setFalseResult("Checkout button IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(CartPage.btnCheckout1);
				driver.waitControling.waitPageLoadTimeOutBySecond(10);
				driver.waitControling.sleep(3000);
				final String actualURL = driver.browserControling.getBrowserURL();
				if (actualURL.contains(ReqStorefrontData.urlCheckoutLogin)) {
					utils.setTrueResult("Login Page is displayed");
				} else {
					utils.setFalseResult("Login Page is not displayed");
					throw new Exception();
				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickCheckOutOnCartPage() throws Exception {
		try {
			// Verify the Order Total Calculation
//			this.vrfOrderTotal();

			// click on Checkout button on Cart page
			if (!driver.elementChecking.isElementDisplay(CartPage.btnCheckout2)) {
				utils.setFalseResult("Checkout button IS NOT displayed");
				throw new Exception();
			} else {
				
				driver.elementEventControling.click(CartPage.btnCheckout2);
				driver.waitControling.waitPageLoadTimeOutBySecond(10);
				driver.waitControling.sleep(3000);
				final String actualURL = driver.browserControling.getBrowserURL();
				if (actualURL.contains(ReqStorefrontData.urlDeliveryAddressPage)) {
					utils.setTrueResult("Delivery Address Page is displayed");
					deliveryApp.vrfDeliveryAddressPage();
				} else {
					utils.setFalseResult("Delivery Address Page is not displayed");
					throw new Exception();
				}
			}
		}

		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	private int getProductPrice(final String xpath) throws Exception {
		int priceValue = 0;
		String price = "";
		try {
			if (!driver.elementChecking.isElementDisplay(xpath)) {
				utils.setFalseResult("Product Price is not Displayed");
			} else {
				price = driver.elementEventControling.getText(xpath);
				if (price.contains("¥")) {
					String pricevalue[] = price.split("¥");
					price = pricevalue[1];
				}
				price = price.replaceAll(",", "");
				priceValue = Integer.parseInt(price);
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return priceValue;
	}

	public void changeQuantity(final String productName, final String newQuantity) throws Exception {
		try {
			final int countProd = this.countCartList();
			if (countProd == 0) {
				utils.setTrueResult("Cart List is empty");
			} else {
				for (int i = 1; i <= countProd; i++) {
					String productNameXpath = CartPage.listItemsInCart + "/li[@class='" + CartPage.itemEntryClassName
							+ "'][" + i + "]/div[2]/div/a/span";
					if (driver.elementEventControling.getText(productNameXpath).trim().equalsIgnoreCase(productName)) {
						utils.setTrueResult("Product is Available in the Cart");
						String productQtyXpath = CartPage.listItemsInCart + "/li[@class='" + CartPage.itemEntryClassName
								+ "'][" + i + "]/div[2]/div[3]/div[2]/form/input[4]";
						String btnIncrease = CartPage.listItemsInCart + "/li[@class='" + CartPage.itemEntryClassName
								+ "'][" + i + "]/div[2]/div[3]/div[2]/form/span[@class='"
								+ CartPage.btnIncreaseClassName + "']";
						String btnDecrease = CartPage.listItemsInCart + "/li[@class='" + CartPage.itemEntryClassName
								+ "'][" + i + "]/div[2]/div[3]/div[2]/form/span[@class='"
								+ CartPage.btnDecreaseClassName + "']";
						String oldQuantity = driver.elementEventControling.getValue(productQtyXpath).trim();
						if (oldQuantity.equals(newQuantity)) {
							log.info("New Quantity value and Old Quantity values are Same. No Change required!!");
						} else {
							int oldValue = Integer.parseInt(oldQuantity);
							int newValue = Integer.parseInt(newQuantity);
							this.changeQty(newValue, oldValue, btnDecrease, btnIncrease);

							if (driver.elementEventControling.getValue(productQtyXpath).trim().equals(newQuantity)) {
								utils.setTrueResult("New Quantity is Set Successfully");

								int productPrice = this.getProductPrice(CartPage.listItemsInCart + "/li[@class='"
										+ CartPage.itemEntryClassName + "'][" + i + "]/div[2]/div[3]/div[1]");
								int productQuantity = Integer.parseInt(newQuantity);
								int totalprice = productPrice * productQuantity;
								String productTotalPrice = Integer.toString(totalprice);
								this.vrfProductTotalPrice(productName, productTotalPrice);
							} else {
								utils.setFalseResult("New Quantity is NOT Set Successfully");
							}
						}

					}
				}

			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	private void changeQty(final int newValue, final int oldValue, final String xpathMinus, final String xpathPlus)
			throws Exception {
		if (newValue < oldValue) {
			for (int i = oldValue; i > newValue; i--) {
				driver.elementEventControling.click(xpathMinus);
				driver.waitControling.waitPageLoadTimeOutBySecond(5);
			}
		}
		if (newValue > oldValue) {
			for (int i = oldValue; i < newValue; i++) {			
				driver.elementEventControling.click(xpathPlus);
				driver.waitControling.waitPageLoadTimeOutBySecond(5);
			}
		}
	}

	/********* Save For Later ****** By BABIN ********/

	public void SveFrLtrremoveAllProducts() throws Exception {
		try {

			if (driver.elementChecking.isElementDisplay(CartPage.alertContent)) {
				utils.setTrueResult("Save for later is Empty");

			} else {

				final int countProd = this.countWishlistList();
				if (countProd == 0) {
					utils.setTrueResult("save for later is empty");
				} else {
					for (int i = 1; i <= countProd; i++) {
						// click remove icon on each product to remove
						this.deleteSveFrLtrItemInCart();
					}

				}

				if (driver.elementChecking.isElementDisplay(CartPage.alertContent)) {
					utils.setTrueResult("Save for later is empty. All products are removed successfully");
				} else {
					utils.setFalseResult("save for later is not empty. Still products are available in the Cart");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickSaveForLater() throws Exception {
		try {
			// click on ShoppingCart button On Cart pop-up
			if (!driver.elementChecking.isElementDisplay(CartPage.btMveToCrt, false)) {
				utils.setFalseResult("Save for later link IS NOT displayed on Cart page");
				throw new Exception();
			} else {
				driver.elementEventControling.click(CartPage.btMveToCrt);
				utils.setTrueResult("Save for later is clicked correct");

				if (driver.elementChecking.isElementDisplay(CartPage.SveForLtrAlertInfo)) {
					utils.setTrueResult("'Save for later' alert is displayed");
				} else {
					utils.setFalseResult("'Save for later' alert is not displayed");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickMoveToCart() throws Exception {
		try {
			// click on ShoppingCart button On Cart pop-up
			if (!driver.elementChecking.isElementDisplay(CartPage.btMveToCrt, false)) {
				utils.setFalseResult("Move to cart link IS NOT displayed on Cart page");
				throw new Exception();
			} else {
				driver.elementEventControling.click(CartPage.btMveToCrt);
				utils.setTrueResult("Moved to cart link is clicked correct");

				if (driver.elementChecking.isElementDisplay(CartPage.SveForLtrAlertInfo)) {
					utils.setTrueResult("'Moved to cart' alert is displayed");
				} else {
					utils.setFalseResult("'Moved to cart' alert is not displayed");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public int countWishlistList() throws Exception {

		int countProd = 0;
		try {
			// get List of menu item which have tag "li"
			final List<WebElement> cartListTagLI = commonApp.getSubItemListByClass(CartPage.listItemsInWishlist,
					CartPage.listItemsInWish);
			countProd = cartListTagLI.size();

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return countProd;
	}

	public void deleteSveFrLtrItemInCart() throws Exception {
		try {
			// verify Remove product icon
			if (!driver.elementChecking.isElementDisplay(CartPage.btnDeleteSVitem)) {
				utils.setFalseResult("Remove Product Icon of  IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(CartPage.btnDeleteSVitem);
				utils.setTrueResult("Remove Product icon is clicked correct");

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}

	}
}
