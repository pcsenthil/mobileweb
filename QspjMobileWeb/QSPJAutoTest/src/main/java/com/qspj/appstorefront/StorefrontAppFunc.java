package com.qspj.appstorefront;

import java.text.MessageFormat;
import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.appdata.StorefrontDataAction;
import com.qspj.appdata.StorefrontDataVerification;
import com.qspj.uimap.locator.storefront.LandingPage;
import com.qspj.uimap.locator.storefront.SignInAndRegisterPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

@SuppressWarnings("javadoc")
public class StorefrontAppFunc extends Environment {
	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final StorefrontDataAction dataAction = new StorefrontDataAction();
	private final StorefrontDataVerification dataVrf = new StorefrontDataVerification();
	private final CommonAppFunc commonApp = new CommonAppFunc();

	private enum Site {
		SG, HK, TL, TN
	}

	private enum URLSitePart {
		csin, chk, ctl, ctn
	}

	private void clearTextLoginForm() throws Exception {
		try {
			driver.elementEventControling.clearText(SignInAndRegisterPage.txtEmail);
			driver.elementEventControling.clearText(SignInAndRegisterPage.txtPassword);
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void openApp() throws Exception {
		try {

			// get url from excel file
			final String url = dataAction.getSiteURLData();
			// use openURL from browserControling class to open URL
			driver.browserControling.deleteCookies();
			driver.browserControling.openURL(url);
				if (driver.browserControling.getBrowserTitle().contains(ReqStorefrontData.siteWindowTitle)) {
					// Passed: set passed to this function
					utils.setTrueResult(
							MessageFormat.format("QSPJ Storefront Site \"{0}\" is loaded and ready for work", url));
//					this.selectCurrency();
				} else {
					// Failed: set failed to this function
					utils.setFalseResult(
							MessageFormat.format("QSPJ Storefront Site \"{0}\" is not loaded successful", url));
				}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	private void selectCurrency() throws Exception
	{
		try {
			if(driver.elementChecking.isElementDisplay(LandingPage.ddCurrency)) {
				driver.elementEventControling.selectValue(LandingPage.ddCurrency, "USD");
				utils.setTrueResult("USD Currency is Selected");
			}else {
				utils.setFalseResult("Currency Selector Dropdown is NOT Displayed");
			}
		}
		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void openAppWithoutCookiesDeleted() throws Exception {
		try {

			// get url from excel file
			final String url = dataAction.getSiteURLData();
			driver.browserControling.openURL(url);
			if (driver.browserControling.getBrowserTitle().contains(ReqStorefrontData.siteWindowTitle)) {
				// Passed: set passed to this function
				utils.setTrueResult(
						MessageFormat.format("QSPJ Storefront Site \"{0}\" is loaded and ready for work", url));
			} else {
				// Failed: set failed to this function
				utils.setFalseResult(
						MessageFormat.format("QSPJ Storefront Site \"{0}\" is not loaded successful", url));
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

}
