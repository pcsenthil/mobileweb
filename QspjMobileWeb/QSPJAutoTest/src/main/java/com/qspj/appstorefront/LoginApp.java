package com.qspj.appstorefront;

import java.text.MessageFormat;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.uimap.locator.storefront.LandingPage;
import com.qspj.uimap.locator.storefront.LoggedInPage;
import com.qspj.uimap.locator.storefront.SignInAndRegisterPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

@SuppressWarnings("javadoc")

public class LoginApp extends Environment {

	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final SignUpApp signUpApp = new SignUpApp();
	private final LandingPageApp landingPageApp = new LandingPageApp();

	public boolean vrfLoginPageLaunch() throws Exception {
		boolean isVrf = false;
		try {
			final String urlWindow = driver.browserControling.getBrowserURL();
			if (urlWindow.contains("en/login")) {
				utils.setTrueResult("Login page is launched");
				isVrf = true;
			} else {
				utils.setFalseResult("Login page IS NOT launched");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrf;
	}

	private void inputEmailAddress(final String username) throws Exception {
		try {
			// input email address
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtEmail)) {
				utils.setFalseResult("Email textbox IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtEmail, username);
			}

			// validate
			final String inputedText = driver.elementEventControling.getValue(SignInAndRegisterPage.txtEmail);
			if (inputedText.equals(username)) {
				utils.setTrueResult(MessageFormat.format("Email Address:\"{0}\" is entered correct", username));
			} else {
				utils.setFalseResult(MessageFormat.format("Email Address:\"{0}\" IS NOT entered correct", username));
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	private void inputPassword(final String password) throws Exception {
		try {
			// input password
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtPassword)) {
				utils.setFalseResult("Password textbox IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtPassword, password);
			}

			// verify Password
			final String passwordText = driver.elementEventControling.getText(SignInAndRegisterPage.txtPassword);
			final String passwordValue = driver.elementEventControling.getAttribute(SignInAndRegisterPage.txtPassword,
					"value");
			if ("".equals(passwordText) && password.equals(passwordValue)) {
				utils.setTrueResult(MessageFormat.format("Password:\"{0}\" is entered correct", password));
			} else {
				utils.setFalseResult(MessageFormat.format("Password:\"{0}\" IS NOT entered correct", password));
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	private void clickLoginButton() throws Exception {
		try {
			// click on Login button
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.btLogin)) {
				utils.setFalseResult("Login button IS NOT displayed");
				throw new Exception();
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.btLogin);
				driver.waitControling.waitPageLoadTimeOutBySecond(15);
				driver.waitControling.sleep(2000);
			}
			utils.setTrueResult("Login button is clicked successful");
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	/**
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	public void login(final String username, final String password) throws Exception {
		try {
			this.inputEmailAddress(username);
			this.inputPassword(password);
			this.clickLoginButton();
			landingPageApp.selectMenu();
			// verify login success or not
			if (driver.elementChecking.isElementDisplay(LoggedInPage.lbUserWelcome)) {
				utils.setTrueResult("Login is Successful");
			} else {
				utils.setFalseResult("Login is not Successful");
				throw new Exception();
			}
			landingPageApp.closeMenu();
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void gotoSignOut(final String MyAccountnavItemName) throws Exception {
		try {
			boolean isItemDisplay = false;
			if (driver.elementChecking.isElementDisplay(LandingPage.lnkMyAccount)) {
				driver.elementEventControling.click(LandingPage.lnkMyAccount);

				final List<WebElement> leftNavList = commonApp.getSubItemListByTag(LandingPage.lnkListMyAccount, "li");

				for (int i = 0; i < leftNavList.size(); i++) {
					if (!isItemDisplay) {
						int temp = i + 1;
						String linkXpath = LandingPage.lnkListMyAccount + "/li[" + temp + "]/a";

						String linkTitle = driver.elementEventControling.getAttribute(linkXpath, "title").trim();

						if (MyAccountnavItemName.equalsIgnoreCase(linkTitle)) {
							isItemDisplay = true;
							driver.elementEventControling.click(linkXpath);
						}
					}
				}
				if (!isItemDisplay) {
					utils.setFalseResult(MessageFormat.format("Could not find \"{0}\" link under My Account Menu",
							MyAccountnavItemName));
				}
			} else {
				utils.setFalseResult("Could not find My Account Menu");
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void guestUserLogin(final String email) throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtGuestEmail)) {
				utils.setFalseResult("Email Address Textbox is NOT Displayed for Guest User");
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtGuestEmail, email);
			}

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.txtGuestConEmail)) {
				utils.setFalseResult("Confirm Email Address Textbox is NOT Displayed for Guest User");
			} else {
				driver.elementEventControling.type(SignInAndRegisterPage.txtGuestConEmail, email);
			}

			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.btGuestCheckout)) {
				utils.setFalseResult("Checkout As a Guest Button is NOT Displayed");
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.btGuestCheckout);
				driver.waitControling.waitPageLoadTimeOutBySecond(10);
				String currentURL = driver.browserControling.getBrowserURL();
				if (currentURL.contains(ReqStorefrontData.urlDeliveryAddressPage)) {
					utils.setTrueResult("Checkout - Delivery Address Page is Displayed ");
//					deliveryApp.vrfDeliveryAddressPage();
				} else {
					utils.setFalseResult("Checkout - Delivery Address Page is NOT Displayed ");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void selectGoogleSignIn() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.btLoginWithGoogle)) {
				utils.setFalseResult("Google Sign In Button is NOT Displayed");
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.btLoginWithGoogle);

				driver.waitControling.sleep(2000);
				if (driver.browserControling.selectNewWindow()) {
					final String currentWindowTitle = driver.browserControling.getBrowserTitle();

					if (currentWindowTitle.contains(SignInAndRegisterPage.googleSignInWindowTitle)) {
						utils.setTrueResult("Google Sign In Window is displayed");
					}
				} else {
					utils.setTrueResult("Google Signed In successfully");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void selectFaceBookSignIn() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(SignInAndRegisterPage.btLoginWithFB)) {
				utils.setFalseResult("Facebook Sign In Button is NOT Displayed");
			} else {
				driver.elementEventControling.click(SignInAndRegisterPage.btLoginWithFB);

				driver.waitControling.sleep(2000);
				if (driver.browserControling.selectNewWindow()) {
					final String currentWindowTitle = driver.browserControling.getBrowserTitle();

					if (currentWindowTitle.contains(SignInAndRegisterPage.facebookSignInWindowTitle)) {
						utils.setTrueResult("Facebook Sign In Window is displayed");
					}
				} else {
					utils.setTrueResult("Facebook Signed In successfully");
				}

			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void popupForgotYourPassword(String string) {
		// TODO Auto-generated method stub

	}

}
