package com.qspj.appstorefront;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.uimap.locator.storefront.CartPopup;
import com.qspj.uimap.locator.storefront.ProductDetailPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class ProductDetailPageApp extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final CartApp cartApp = new CartApp();

	private void inputQuantity(final String quantity) throws Exception {
		try {
			// input quantity
			if (!driver.elementChecking.isElementDisplay(ProductDetailPage.txtQuantity)) {
				utils.setFalseResult("Quantity textbox IS NOT displayed");
				throw new Exception();
			} else {
				// clear existed quantity
				driver.elementEventControling.clearText(ProductDetailPage.txtQuantity);			
				driver.elementEventControling.type(ProductDetailPage.txtQuantity, quantity);
			}

			// validate
			final String inputedText = driver.elementEventControling.getValue(ProductDetailPage.txtQuantity);
			if (inputedText.equals(quantity)) {
				utils.setTrueResult(MessageFormat.format("Product Quantity:\"{0}\" is entered correct", quantity));
			} else {
				utils.setFalseResult(MessageFormat.format("Product Quantity:\"{0}\" IS NOT entered correct", quantity));
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void addProductToCart(final String quantity) throws Exception {
		try {
			// verify test data
			if (utils.parseStringToInt(quantity) <= 0) {
				utils.setFalseResult("INVALID test data. Quantity should be greater 0");
				throw new Exception();
			}

			if (!driver.elementChecking.isElementDisplay(ProductDetailPage.btAddToCart)) {
				utils.setFalseResult("Add To Cart button IS NOT displayed");
				throw new Exception();
			} else {
				// put quantity
				this.inputQuantity(quantity);

			}
			String productName = driver.elementEventControling.getText(ProductDetailPage.lbProductName).trim();
			
			// click Add To Cart button
			driver.elementEventControling.click(ProductDetailPage.btAddToCart);
			driver.waitControling.sleep(3000);
			
			// 'ADDED TO YOUR SHOPPING CART' pop up
			if (driver.elementChecking.waitForElementPresent(CartPopup.popUpAddedToCart, 15)) {
				utils.setTrueResult("'ADDED TO YOUR SHOPPING CART' pop up is displayed");
				if(driver.elementEventControling.getText(CartPopup.pdtDetail).trim().contains(productName)){
					utils.setTrueResult(MessageFormat.format("Product :\"{0}\" is Added to Cart Successfully", productName));
					if(driver.elementEventControling.getText(CartPopup.pdtQty).trim().endsWith(quantity)){
						utils.setTrueResult(MessageFormat.format("Product Quantity :\"{0}\" is Correct", driver.elementEventControling.getText(CartPopup.pdtQty).trim()));
					}else{
						utils.setFalseResult(MessageFormat.format("Product Quantity :\"{0}\" is NOT Correct", driver.elementEventControling.getText(CartPopup.pdtQty).trim()));
					}
				}else{
					utils.setFalseResult(MessageFormat.format("Product :\"{0}\" is NOT Added to Cart Successfully", productName));
				}
				driver.elementEventControling.click(CartPopup.btClose);
			} else {
				utils.setFalseResult("'ADDED TO YOUR SHOPPING CART' pop up is NOT displayed");
				throw new Exception();
			}
			this.vrfProductInCart(productName, quantity);

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	
	public int countMiniCartList() throws Exception {

		int countProd = 0;
		try {
			// get List of menu item which have tag "li"
			final List<WebElement> cartListTagLI = commonApp.getSubItemListByClass(CartPopup.listProductItems,
					CartPopup.ListProductClassName);
			countProd = cartListTagLI.size();
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return countProd;
	}

	// Check the Shopping Cart for the Product Added with Quantity & Price.

	private void vrfProductInCart(final String productName, final String quantity) throws Exception {
		try {
			BigDecimal productPrice = this.getProductPrice();
			BigDecimal productQuantity = new BigDecimal(quantity);
			BigDecimal totalprice = productPrice.multiply(productQuantity);
			String productTotalPrice = totalprice.toString();
			boolean searchresult = false;
			cartApp.openCartPopup();
			
			//Verify Product in Mini Cart
			
			final int countProd = this.countMiniCartList();
			if (countProd == 0) {
				utils.setTrueResult("MiniCart List is empty");
			} else {
				for (int i = 1; i <= countProd; i++) {

					String productNameXpath = CartPopup.listProductItems + "/li[@class='" + CartPopup.ListProductClassName
							+ "'][" + i + "]/div[2]/a";
					if (driver.elementEventControling.getText(productNameXpath).trim().startsWith(productName)) {
						utils.setTrueResult("Product is Available in the Cart");
						searchresult = true;
						String productQtyXpath = CartPopup.listProductItems + "/li[@class='" + CartPopup.ListProductClassName
								+ "'][" + i + "]/div[2]/div";
						if (driver.elementEventControling.getText(productQtyXpath).trim().contains(quantity)) {
							utils.setTrueResult(MessageFormat.format(
									"Product Quantity for the Product:\"{0}\" is Correct : \"{1}\"", productName,
									driver.elementEventControling.getText(productQtyXpath).trim()));
						} else {
							utils.setFalseResult(MessageFormat.format(
									"Product Quantity for the Product:\"{0}\" is NOT Correct : \"{1}\"", productName,
									driver.elementEventControling.getText(productQtyXpath).trim()));
						}
					}

				}
				if(!searchresult){
					utils.setFalseResult(MessageFormat.format(
							"Product : \"{0}\" is Not Available in the MiniCart", productName));
				}
					
			}			
			// Go to Cart page
			cartApp.clickCheckOutOnCartPopup();
			
			//Verify the product in Cart 
			cartApp.vrfProductTotalPrice(productName, productTotalPrice);

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	// Get the product Price
	private BigDecimal getProductPrice() throws Exception {
		BigDecimal priceValue = null ;
		String price = "";
		try {
			if (!driver.elementChecking.isElementDisplay(ProductDetailPage.lbProductPrice)) {
				utils.setFalseResult("Product Price is not Displayed");
			} else {
				price = driver.elementEventControling.getText(ProductDetailPage.lbProductPrice).trim().substring(1).replace(",", "");
				log.info("Product Price is " +price);		
				priceValue = new BigDecimal(price);
				price = priceValue.toString();
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return priceValue;
	}

	public boolean vrfProductName(final String productName) throws Exception {
		try {
			// verify bundle label
			if (!driver.elementChecking.isElementDisplay(ProductDetailPage.lbProductName)) {
				utils.setFalseResult("Product Name IS NOT displayed");
				return false;
			}
			// get product name
			final String actProdName = driver.elementEventControling.getText(ProductDetailPage.lbProductName).trim();
			if (!actProdName.equals(productName)) {
				utils.setFalseResult(MessageFormat
						.format("Product Name IS NOT displayed correct on PDP, Actual result: \"{0}\"", actProdName));
				return false;
			} else {
				utils.setTrueResult(
						MessageFormat.format("Product Name: \"{0}\" is displayed correct on PDP", actProdName));
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return true;
	}

	// Verify whether the product Details & Specification

	/*public void vrfProductDetailsSpec() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(ProductDetailPage.conProductDetails)) {
				utils.setTrueResult("Products Details is displayed");

				if (driver.elementEventControling.getText(ProductDetailPage.conDescrip).contains("Description")) {
					utils.setTrueResult("Product's DESCRIPTION is displayed");
				} else {
					utils.setFalseResult("Product's DESCRIPTION is NOT displayed");
				}

				if (driver.elementEventControling.getText(ProductDetailPage.conSpec).contains("Specification")) {
					utils.setTrueResult("Product's SPECIFICATION is displayed");
				} else {
					utils.setFalseResult("Product's SPECIFICATION is NOT displayed");
				}

				if (driver.elementEventControling.getText(ProductDetailPage.conReview).contains("Reviews")) {
					utils.setTrueResult("Product's REVIEWS is displayed");
				} else {
					utils.setFalseResult("Product's REVIEWS is NOT displayed");
				}

				if (driver.elementEventControling.getText(ProductDetailPage.conDelivery).contains("Delivery")) {
					utils.setTrueResult("Product's DELIVERY is displayed");
				} else {
					utils.setFalseResult("Product's DELIVERY is NOT displayed");
				}

			} else {
				utils.setFalseResult("Products Details is NOT displayed");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
	*/
	public void addProductToCartNew(final String quantity) throws Exception {
	  try {
	   // verify test data
	   if (utils.parseStringToInt(quantity) <= 0) {
	    utils.setFalseResult("INVALID test data. Quantity should be greater 0");
	    throw new Exception();
	   }

	   if (!driver.elementChecking.isElementDisplay(ProductDetailPage.btAddToCart)) {
	    utils.setFalseResult("Add To Cart button IS NOT displayed");
	    throw new Exception();
	   } else {       this.inputQuantity(quantity);

	   }

	   // click Add To Cart button
	   driver.elementEventControling.click(ProductDetailPage.btAddToCart);
	   driver.waitControling.sleep(3000);
	   // wait for Cart popup display
	   if (driver.elementChecking.waitForElementPresent(CartPopup.btCheckout, 25)) {
	    cartApp.closeCartPopup();
	   } else {
	    utils.setFalseResult("Cart popup is not displayed after clicked Add To Cart button");
	    throw new Exception();
	   }
	   utils.setTrueResult("Product is added to cart");
	  } catch (final Exception e) {
	   commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
	   log.error(StackTraceInfo.getCurrentClassName());
	   log.error(StackTraceInfo.getCurrentFileName());
	   log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
	  }
	 }
	
	
	public void shareFeature(final String windowTitle) throws Exception {
	  try {
	   
	   if (!driver.elementChecking.isElementDisplay(ProductDetailPage.sharelabel)) 
	   {
	    utils.setFalseResult("Add To Cart button IS NOT displayed");
	    throw new Exception();
	   } 
	   else 
	   {
	    
	    driver.elementEventControling.click(ProductDetailPage.shareFacebook);
	    driver.waitControling.sleep(500);
	    commonApp.switchToWindow(windowTitle);
	   }
	  }
	   
	   catch (final Exception e) {
	    commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
	    log.error(StackTraceInfo.getCurrentClassName());
	    log.error(StackTraceInfo.getCurrentFileName());
	    log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
	   }
	  }
	
	
	public void facebookLogin(final String login, final String password,String closeWindowTitle,String switchNextWindowTitle)throws Exception
	   {
	   
	   try
	   
	   {
	   
	    driver.elementEventControling.type(ProductDetailPage.facebookLogin, login);
	    driver.elementEventControling.type(ProductDetailPage.facebookpsw, password);
	    driver.elementEventControling.click(ProductDetailPage.facebookSubmit);
	    driver.waitControling.sleep(500);
	    
	    
	    
	    
	    if (driver.elementChecking.waitForElementPresent(ProductDetailPage.facebookTitle, 25))
	    {
	     driver.elementEventControling.click(ProductDetailPage.ButtonPostFacebook);
	     //commonApp.closeWindow(closeWindowTitle,switchNextWindowTitle);
	     
	    } else {
	     utils.setFalseResult("FaceBook popup is not displayed after clicked Facebook button");
	     throw new Exception();
	    }
	    utils.setTrueResult("FaceBook logged in");
	    
	    
	   }
	 catch (final Exception e) {
	   commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
	   log.error(StackTraceInfo.getCurrentClassName());
	   log.error(StackTraceInfo.getCurrentFileName());
	   log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
	  }
	 }
	
	public void vrfProductDetailsSpec() throws Exception
	 {
	   try
	   {
	    if(driver.elementChecking.isElementDisplay(ProductDetailPage.conProductDetails))
	    {
	     utils.setTrueResult("Product Details is displayed");
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productImageGallery))
	     {
	      utils.setTrueResult("Product's image gallery is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product's image gallery is NOT displayed");
	     }
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productImage))
	     {
	      utils.setTrueResult("Product's image is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product's image is NOT displayed");
	     }
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productName))
	     {
	      utils.setTrueResult("Product Name is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product Name is NOT displayed");
	     }
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productRating))
	     {
	      utils.setTrueResult("Product's Rating is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product's Rating is NOT displayed");
	     }
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productReviews))
	     {
	      utils.setTrueResult("Product's Review is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product's Review is NOT displayed");
	     }
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productDescription))
	     {
	      utils.setTrueResult("Product's DESCRIPTION is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product's DESCRIPTION is NOT displayed");
	     }
	     
	     if(driver.elementChecking.isElementDisplay(ProductDetailPage.productPrice))
	     {
	      utils.setTrueResult("Product Price is displayed");
	     }
	     else 
	     {
	      utils.setFalseResult("Product Price is NOT displayed");
	     }
	     if(driver.elementEventControling.getText(ProductDetailPage.conDescrip).contains("Description"))
	     {
	      utils.setTrueResult("DESCRIPTION  section is displayed");
	     }
	     else
	     {
	      utils.setFalseResult("DESCRIPTION  section is not displayed");
	     }
	     
	     if(driver.elementEventControling.getText(ProductDetailPage.conSpec).contains("Specification"))
	     {
	      utils.setTrueResult(" SPECIFICATION section is displayed");
	     }
	     else
	     {
	      utils.setFalseResult("SPECIFICATION section is NOT displayed");
	     }
	     
	     
	     if(driver.elementEventControling.getText(ProductDetailPage.conReview).contains("Reviews"))
	     {
	      utils.setTrueResult("REVIEWS  section is displayed");
	     }
	     else
	     {
	      utils.setFalseResult("REVIEWS  section is not displayed");
	     }
	          
	    }
	    else
	    {
	     utils.setFalseResult("Products Details is NOT displayed");
	    }
	   }
	   catch (final Exception e)
	  {
	   commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
	   log.error(StackTraceInfo.getCurrentClassName());
	   log.error(StackTraceInfo.getCurrentFileName());
	   log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
	  }
	 }
	
	

}
