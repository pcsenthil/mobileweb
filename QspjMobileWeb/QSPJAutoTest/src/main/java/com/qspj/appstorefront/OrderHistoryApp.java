package com.qspj.appstorefront;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebElement;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.appdata.StorefrontDataVerification;
import com.qspj.uimap.locator.storefront.OrderHistoryPage;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class OrderHistoryApp extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final StorefrontDataVerification dataVrf = new StorefrontDataVerification();

	public boolean checkrecentOrder(final String orderNo) throws Exception {
		boolean vrf = false;
		try {
			if (driver.elementEventControling.getText(OrderHistoryPage.firstOrder).contains(orderNo)) {
				utils.setTrueResult(MessageFormat.format("Order \"{0}\" found in Order Histor Page", orderNo));
				vrf = true;
			} else {
				utils.setFalseResult(MessageFormat.format("Order \"{0}\" is not found in Order Histor Page", orderNo));
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
		return vrf;
	}

	public void vrfOrderList() throws Exception {
		boolean isVrf = true;
		try {

			// get order list
			final List<WebElement> orderList = commonApp.getSubItemListByTag(OrderHistoryPage.orderList, "tr");
			for (int i = 2; i <= orderList.size(); i++) {
				String orderIdXpath = OrderHistoryPage.orderList + "/tr[" + i + "]/td[2]/a";
				String orderStatusXpath = OrderHistoryPage.orderList + "/tr[" + i + "]/td[4]";
				String orderDateXpath = OrderHistoryPage.orderList + "/tr[" + i + "]/td[6]";
				String orderTotalXpath = OrderHistoryPage.orderList + "/tr[" + i + "]/td[8]";

				String orderID = driver.elementEventControling.getText(orderIdXpath).trim();
				// verify order id text
				if (orderID == null) {
					utils.setFalseResult("Order ID IS NULL");
					isVrf = false;
					break;
				}

				// verify the Order Detail link
				if (!driver.elementEventControling.getAttribute(orderIdXpath, "href").contains("/order/" + orderID)) {
					utils.setFalseResult("Order Detail link IS NOT displayed");
					isVrf = false;
					break;
				}
				// verify order Status
				if (driver.elementEventControling.getText(orderStatusXpath) == null) {
					utils.setFalseResult("Order Status IS NULL");
					isVrf = false;
					break;
				}
				// verify order Date Status
				if (driver.elementEventControling.getText(orderDateXpath) == null) {
					utils.setFalseResult("Order Date IS NULL");
					isVrf = false;
					break;
				}

				// verify order Date Status
				if (driver.elementEventControling.getText(orderTotalXpath) == null) {
					utils.setFalseResult("Order Total IS NULL");
					isVrf = false;
					break;
				}
			}
			if (isVrf) {
				utils.setTrueResult("Order Content is displayed correct for available Orders");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfPagination() throws Exception {
		try {
			boolean isVrf = true;
			final String partURLOfPaging = "my-account/orders?sort=byDate&page=";
			if (!driver.elementChecking.isElementDisplay(OrderHistoryPage.tabPagination)) {
				utils.setFalseResult("Pagination Tab is not displayed in the Order History Page");
			}
			List<WebElement> pageList = commonApp.getSubItemListByTag(OrderHistoryPage.tabPagination, "li");

			if (pageList != null) {
				for (int i = 2; i < pageList.size(); i++) {
					pageList.get(i).click();
					driver.waitControling.waitPageLoadTimeOutBySecond(15);
					int temp = i - 1;
					String url = driver.browserControling.getBrowserURL().trim();
					String linkPath = OrderHistoryPage.tabPagination + "/li[" + temp + "]/span";
					if (!url.contains(partURLOfPaging + temp)) {
						isVrf = false;
						break;
					}
					this.vrfOrderList();
					pageList = commonApp.getSubItemListByTag(OrderHistoryPage.tabPagination, "li");
				}
			}

			if (isVrf) {
				utils.setTrueResult("Pagination Tab is worked correct");
			} else {
				utils.setFalseResult("Pagination Tab IS NOT worked correct");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void vrfSorting() throws Exception {
		try {
			final String sortByDateUrl = "my-account/orders?sort=byDate";
			final String sortByOrderNumberUrl = "my-account/orders?sort=byOrderNumber";
			boolean vrf = false;
			if (!driver.elementChecking.isElementDisplay(OrderHistoryPage.drpdwnSortby)) {
				utils.setTrueResult("Sort By is not displayed in the Order History page");
			} else {
				// Order Number
				driver.elementEventControling.selectValue(OrderHistoryPage.drpdwnSortby, "byOrderNumber");
				final String sortByDateUrl1 = driver.browserControling.getBrowserURL();
				if (driver.browserControling.getBrowserURL().contains(sortByOrderNumberUrl)) {
					vrf = true;
					utils.setTrueResult("Order List Sorted by Order Number");
				}

				// Date
				driver.elementEventControling.selectValue(OrderHistoryPage.drpdwnSortby, "byDate");
				if (driver.browserControling.getBrowserURL().contains(sortByDateUrl)) {
					vrf = true;
					utils.setTrueResult("Order List Sorted by Date");
				}

				if (!vrf) {
					utils.setTrueResult("Order Sort By IS NOT WORKING properly");
				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void clickrecentOrderID() throws Exception {
		try {
			if (!driver.elementChecking.isElementDisplay(OrderHistoryPage.firstOrder)) {
				utils.setTrueResult(" Order details are not available");

			} else {
				String orderID = driver.elementEventControling.getText(OrderHistoryPage.firstOrder).trim();
				driver.elementEventControling.click(OrderHistoryPage.firstOrder);
				String url = driver.browserControling.getBrowserURL().trim();

				if (url.contains("my-account/order/" + orderID)) {
					utils.setTrueResult(
							MessageFormat.format("Order Details Page is displayed for the Order \"{0}\"", orderID));
				} else {
					utils.setFalseResult(
							MessageFormat.format("Order Details Page is NOT displayed for the Order \"{0}\"", orderID));
				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public boolean searchOrderID(final String orderID) throws Exception {
		boolean isVrf = false;
		try {
			final List<WebElement> orderList = commonApp.getSubItemListByTag(OrderHistoryPage.orderList, "tr");
			for (int i = 2; i <= orderList.size(); i++) {
				if (!isVrf) {
					String orderIdXpath = OrderHistoryPage.orderList + "/tr[" + i + "]/td[2]/a";
					// verify order id
					if (driver.elementEventControling.getText(orderIdXpath).trim().equals(orderID)) {
						isVrf = true;
						utils.setTrueResult(MessageFormat.format("Order \"{0}\" found in Order History Page", orderID));
					}
				}
			}
			if (!isVrf) {
				utils.setFalseResult(MessageFormat.format("Order \"{0}\" is NOT found in Order History Page - \"{1}\" ",
						orderID, driver.browserControling.getBrowserURL().trim()));
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrf;
	}

	public void vrfPaginationTab() throws Exception {
		boolean isVrf = true;
		final String partURLOfPaging = "https://qspj:qspj%40123@qspj.sai-digital.com/qspjstorefront/qspj/en/my-account/orders?sort=byDate&page=";
		int pageNo = 1;
		try {
			// verify Pagination tab
			driver.waitControling.sleep(2000);

			if (!driver.elementChecking.isElementDisplay(OrderHistoryPage.btNextPage)) {
				utils.setTrueResult(" Pagination IS NOT AVAILABLE IN THIS PAGE");

			} else {
				// get all pagination item
				ArrayList<WebElement> pagItemList = commonApp.getSubItemListByTag(OrderHistoryPage.pagination, "li");
				pagItemList = commonApp.getSubItemListByTag(OrderHistoryPage.pagination, "li");
				// verify paging from third page

				for (int i = 2; i < pagItemList.size() - 1; i++) {
					driver.elementEventControling.click(OrderHistoryPage.btNextPage);
					driver.waitControling.waitPageLoadTimeOutBySecond(15);
					// verify
					pageNo = i - 1;
					final String currURL = driver.browserControling.getBrowserURL();
					if (!currURL.contains(partURLOfPaging + pageNo)) {
						isVrf = false;
					}

				}

				if (isVrf) {
					utils.setTrueResult("Pagination Tab is worked correct");
				} else {
					utils.setFalseResult("Pagination Tab IS NOT worked correct");
				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
}
