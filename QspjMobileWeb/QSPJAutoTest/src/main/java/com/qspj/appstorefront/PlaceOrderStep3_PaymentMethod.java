package com.qspj.appstorefront;

import com.qspj.appcommon.CommonAppFunc;
import com.qspj.appdata.ReqStorefrontData;
import com.qspj.appdata.StorefrontDataVerification;
import com.qspj.uimap.locator.storefront.Step3PaymentMethod;
import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

public class PlaceOrderStep3_PaymentMethod extends Environment {

	private final ReqStorefrontData reqData = new ReqStorefrontData();
	private final CommonAppFunc commonApp = new CommonAppFunc();
	private final StorefrontDataVerification datavrf = new StorefrontDataVerification();

	public void vrfPaymentMethodPage() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.orderSummarySection)) {
				if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.orderListItems)) {
					utils.setTrueResult("List of Items for Order are displayed in Order Summary Section");
				}
				if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.OrderSummaryAddress)) {
					utils.setTrueResult("Shipping Address is Selected");
				}
			} else {
				utils.setFalseResult("Order Summary Section is not displayed");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}

	public void selectPaymentMethod(final String paymentMethod) throws Exception {
		try {
			if (datavrf.vrfPaymentMethodData(paymentMethod)) {

//				// Credit Card
//				if (ReqStorefrontData.paymentMethodList[0].equalsIgnoreCase(paymentMethod)) {
//					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbCreditDebitCard)) {
//						utils.setTrueResult("Credit Card Radio Button is displayed");
//						driver.elementEventControling.click(Step3PaymentMethod.rbCreditDebitCard);
//					} else {
//						utils.setFalseResult("Credit Card Radio Button is NOT displayed");
//					}
//
//				}

				// Cybersource

				if (ReqStorefrontData.paymentMethodList[1].equalsIgnoreCase(paymentMethod)) {
					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbCybersource)) {
						utils.setTrueResult("CyberSource Radio Button is displayed");
						driver.elementEventControling.click(Step3PaymentMethod.rbCybersource);
					} else {
						utils.setFalseResult("CyberSource Radio Button is NOT displayed");
					}

				}

				// Cash On Delivery
				if (ReqStorefrontData.paymentMethodList[2].equalsIgnoreCase(paymentMethod)) {
					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbCashOnDelivery)) {
						utils.setTrueResult("Cash On Delivery Radio Button is displayed");
						driver.elementEventControling.click(Step3PaymentMethod.rbCashOnDelivery);
					} else {
						utils.setFalseResult("Cash On Delivery Radio Button is NOT displayed");
					}
				}

//				// Bank Transfer
//				if (ReqStorefrontData.paymentMethodList[3].equalsIgnoreCase(paymentMethod)) {
//					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbBankTransfer)) {
//						utils.setTrueResult("Bank Transfer Radio Button is displayed");
//						driver.elementEventControling.click(Step3PaymentMethod.rbBankTransfer);
//					} else {
//						utils.setFalseResult("Bank Transfer Radio Button is NOT displayed");
//					}
//
//				}
//
//				// Konbini (Convenience Store)
//				if (ReqStorefrontData.paymentMethodList[4].equalsIgnoreCase(paymentMethod)) {
//					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbKonbiniConvenientStore)) {
//						utils.setTrueResult("Konbini (Convenience Store) Radio Button is displayed");
//						driver.elementEventControling.click(Step3PaymentMethod.rbKonbiniConvenientStore);
//					} else {
//						utils.setFalseResult("Konbini (Convenience Store) Radio Button is NOT displayed");
//					}
//
//				}
//
//				// LinePay
//
//				if (ReqStorefrontData.paymentMethodList[5].equalsIgnoreCase(paymentMethod)) {
//					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbLinePay)) {
//						utils.setTrueResult("LinePay Radio Button is displayed");
//						driver.elementEventControling.click(Step3PaymentMethod.rbLinePay);
//					} else {
//						utils.setFalseResult("LinePay Radio Button is NOT displayed");
//					}
//
//				}
//
//				// PayPal
//
//				if (ReqStorefrontData.paymentMethodList[6].equalsIgnoreCase(paymentMethod)) {
//					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbPaypal)) {
//						utils.setTrueResult("PayPal Radio Button is displayed");
//						driver.elementEventControling.click(Step3PaymentMethod.rbPaypal);
//					} else {
//						utils.setFalseResult("PayPal Radio Button is NOT displayed");
//					}
//
//				}
//
//				// NCCC
//
//				if (ReqStorefrontData.paymentMethodList[7].equalsIgnoreCase(paymentMethod)) {
//					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbNCCC)) {
//						utils.setTrueResult("NCCC Radio Button is displayed");
//						driver.elementEventControling.click(Step3PaymentMethod.rbNCCC);
//					} else {
//						utils.setFalseResult("NCCC Radio Button is NOT displayed");
//					}
//
//				}
			} else {
				utils.setFalseResult("Invalid Payment Method DATA");
			}
		}

		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void inputCardDetails(final String cardType, final String nameOnCard, final String cardNumber,
			final String expiryMonth, final String expiryYear, final String cvv) throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.ddCardType)) {
				driver.elementEventControling.select(Step3PaymentMethod.ddCardType, "Visa");
				utils.setTrueResult("Card Type is Selected");
			} else {
				utils.setFalseResult("Card Type combo box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtNameOnCard)) {
				driver.elementEventControling.type(Step3PaymentMethod.txtNameOnCard, nameOnCard);
				utils.setTrueResult("Name On Card is entered Correctly");
			} else {
				utils.setFalseResult("Name On Card Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtCardNumber)) {
				driver.elementEventControling.type(Step3PaymentMethod.txtCardNumber, cardNumber);
				utils.setTrueResult("Card Number is entered Correctly");
			} else {
				utils.setFalseResult("Card Number Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.ddExpiryMonth)) {
				driver.elementEventControling.select(Step3PaymentMethod.ddExpiryMonth, expiryMonth);
				utils.setTrueResult("Card Number is entered Correctly");
			} else {
				utils.setFalseResult("Expiry Month combo box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.ddExpiryYear)) {
				driver.elementEventControling.select(Step3PaymentMethod.ddExpiryYear, expiryYear);
				utils.setTrueResult("Card Number is entered Correctly");
			} else {
				utils.setFalseResult("Expiry Year combo box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtCVV)) {
				driver.elementEventControling.type(Step3PaymentMethod.txtCVV, cvv);
				utils.setTrueResult("CVV is entered Correctly");
			} else {
				utils.setFalseResult("CVV Text box is not displayed");
			}
		}

		catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void selectBillingAddress(final String billingAdd) throws Exception {
		try {
			// Same As Shipping Address
			if (ReqStorefrontData.billingAddressList[0].equalsIgnoreCase(billingAdd)) {
				if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbSameAsShippingAddress)) {
					utils.setTrueResult("Same As Shipping Address Radio Button is displayed");
					driver.elementEventControling.click(Step3PaymentMethod.rbSameAsShippingAddress);
				} else {
					utils.setFalseResult("Same As Shipping Address Radio Button is NOT displayed");
				}

			}

			// New Billing Address
			if (ReqStorefrontData.billingAddressList[1].equalsIgnoreCase(billingAdd)) {
				if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.rbNewBillingAddress)) {
					utils.setTrueResult("New Billing Address Radio Button is displayed");
					driver.elementEventControling.click(Step3PaymentMethod.rbNewBillingAddress);
					if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.formBillingAddress)) {
						utils.setTrueResult("New Billing Address Form IS displayed");
					}
				} else {
					utils.setFalseResult("New Billing Address Radio Button is NOT displayed");
				}
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void enterNewBillingAddress(final String country, final String title, final String lastName,
			final String firstName, final String address1, final String address2, final String city,
			final String prefectureName, final String postCode) throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.cbCountry)) {
				utils.setTrueResult("Country Combo box is displayed");
				driver.waitControling.sleep(3000);
				driver.elementEventControling.select(Step3PaymentMethod.cbCountry, country);
				
			} else {
				utils.setFalseResult("Country Combo box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.cbTitle)) {
				utils.setTrueResult("Title Combo box is displayed");
				driver.elementEventControling.select(Step3PaymentMethod.cbTitle, title);
			} else {
				utils.setFalseResult("Title Combo box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtLastName)) {
				driver.elementEventControling.type(Step3PaymentMethod.txtLastName, lastName);
				utils.setTrueResult("Last Name entered Correctly");
			} else {
				utils.setFalseResult("Last Name Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtFirstName)) {
				driver.elementEventControling.type(Step3PaymentMethod.txtFirstName, firstName);
				utils.setTrueResult("First Name entered Correctly");
			} else {
				utils.setFalseResult("First Name Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtFurtSubArea)) {
				utils.setTrueResult("Address Line1 Text box is displayed");
				driver.elementEventControling.type(Step3PaymentMethod.txtFurtSubArea, address1);
			} else {
				utils.setFalseResult("Address Line1 Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtSubArea)) {
				utils.setTrueResult("Address Line2 Text box is displayed");
				driver.elementEventControling.type(Step3PaymentMethod.txtSubArea, address2);
			} else {
				utils.setFalseResult("Address Line2 Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtCity)) {
				utils.setTrueResult("City Text box is displayed");
				driver.elementEventControling.type(Step3PaymentMethod.txtCity, city);
			} else {
				utils.setFalseResult("City Text box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.cbPrefecName)) {
				utils.setTrueResult("Prefecture Name Combo box is displayed");
				driver.elementEventControling.select(Step3PaymentMethod.cbPrefecName, prefectureName);
			} else {
				utils.setFalseResult("Prefecture Name Combo box is not displayed");
			}

			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.txtPostCode)) {
				utils.setTrueResult("Postal Code Text Box is displayed");

				driver.elementEventControling.type(Step3PaymentMethod.txtPostCode, postCode);
			} else {
				utils.setFalseResult("Postal Code Text Box is not displayed");
			}

		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());

		}
	}

	public void clickNext() throws Exception {
		try {
			if (driver.elementChecking.isElementDisplay(Step3PaymentMethod.btnNext)) {
				driver.elementEventControling.click(Step3PaymentMethod.btnNext);
				utils.setTrueResult("Next button is clicked");

				final String actualURL = driver.browserControling.getBrowserURL();
				if (actualURL.contains(ReqStorefrontData.urlOrderReviewPage)) {
					utils.setTrueResult("Order Review Page is displayed");
				} else {
					utils.setFalseResult("Order Review Page is not displayed");
					throw new Exception();
				}
			} else {
				utils.setFalseResult("Next Button is NOT displayed");
			}
		} catch (final Exception e) {
			commonApp.throwExceptionMess(StackTraceInfo.getCurrentMethodName(), e.getMessage());
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
	}
}
