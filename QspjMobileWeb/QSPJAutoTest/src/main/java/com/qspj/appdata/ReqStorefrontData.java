package com.qspj.appdata;

import com.sai.configure.Environment;
import com.sai.utils.Log;


@SuppressWarnings("javadoc")
public class ReqStorefrontData extends Environment
{
	public static Log log = new Log(Environment.class);

	// E-num for SITE
	public enum Site
	{
		SG, HK, TL, TN
	}

	// Property files path
	public final static String propBreadcrumbMenuPath = "src/main/java/com/qspj/properties/breadcrumbMenulist.properties";
	public final static String propProdSpec = "src/main/java/com/qspj/properties/prodID2278102Spec.properties";
	public final static String propCategoryTreePath = "src/main/java/com/qspj/properties/CategoryTree.properties";
	public final static String propLandingPagePath = "src/main/java/com/qspj/properties/LandingPage.properties";
	public final static String prop_CTNDistrictListPath = "src/main/java/com/qspj/properties/districtList.properties";
	public final static String prop_CTNSubDistrictListPath = "src/main/java/com/qspj/properties/subdistrictList.properties";

	public final static String propDistrictListValue = "districtNameList";
	public static String[] siteList =
	{ "SAI Tex" };
	public static String siteWindowTitle = "QSPJ Site | Homepage";

	// URL PATHS
	public static String urlCartPage = "/cart";
	public static String urlMyProfile = "/my-account/profile";
	public static String urlAddressBookPage = "/my-account/address-book";
	public static String urlDeliveryAddressPage = "/checkout/multi/delivery-address";
	public static String urlDeliveryMethodPage = "/checkout/multi/delivery-method";
	public static String urlPaymentMethodPage = "/checkout/multi/payment-mode";
	public static String urlOrderReviewPage = "/checkout/multi/summary/view";
	public static String urlOrderConfirmationPage = "/checkout/orderConfirmation/";
	public static String urlNCCC = "https://nccnet-ectest.nccc.com.tw/merchant/HPPRequest";
	public static String urlCyberSource = "https://testsecureacceptance.cybersource.com/billing";
	public static String urlCheckoutLogin = "/login/checkout";
	public static String urlLinePay = "https://sandbox-web-pay.line.me/web/payment/";
	public static String urlPayPal = "https://securepayments.sandbox.paypal.com/";
	
	//Page Title 
	public static String pageTitleEditAccInfo = "Edit Account Information";

	// MESSAGE
	public static String registrationSuccess = "Thank you for registering."; 
	public static String notSubscribedNewsLetter = "You aren't subscribed to our newsletter"; 
	public static String SubscribedNewsLetter = "You are subscribed to"; 
	public static String expRegisterSuccess = "Thank you for registering with ";
	public static String forgotPasswordSuccess= "you will receive an email with a link to reset your password";
	public static String saveAccountInfoSuccess= "You saved the account information.";
	public static String EmptyCartMessage = "You have no items in your shopping cart.";
	public static String orderSuccessMessage = "Thank you for your purchase!";
	
	public static String expError = "Please correct the errors below";
	public static String msgUpdatedProfile = "Your profile has been updated.";
	public static String msgUpdatedProfilePassword = "Your profile & password have been updated";


	public static String[] titleNameList =
	{ "Mr.", "Mrs.", "Miss", "Ms.", "Dr.", "Rev." };
	public static String[] titleValueList =
	{ "mr", "mrs", "miss", "ms", "dr", "rev" };
	public static String[] prefectureNameList =
		{"AITI"	};


	
	public static String[] shipmentMethodList = { "Standard Delivery", "Premium Delivery" };

	public static String[] shipmentMethodValue = { "standard-gross", "premium-gross" };

	public static String[] paymentMethodList = { "Credit Card", "CyberSource", "COD", "Bank Transfer",
			"Konbini (Convenience Store)", "LinePay", "PayPal", "NCCC" };

	public static String[] billingAddressList = { "Same As Shipping Address", "New Billing Address" };

	
	public static String[] productTabList =
	{ "PRODUCT DETAILS", "SPECS", "REVIEWS", "DELIVERY" };
	public static String[] ratingStarList =
	{ "1", "2", "3", "4", "5" };

	//public static String[] myAccountItemList =	{ "Update Personal Details", "Update Email", "Payment Details", "Update Password", "Order History", "Address Book", "Loyalty Corner" };

	// updated 07/18/2016
	public static String[] myAccountItemList =
	{ "Personal Details", "Email Address", "Password", "Order History", "Address Book"};

	public static String[] myAccountLeftNavItemList =
	{ "Personal Details", "Contact Information", "Update Password", "Edit Addresses", "Add a new Address", "View Loyalty Points",
			"View Coupon page", "View Order History", "Active Contracts", "Terminated Contracts" };

	public static String[] EventInfoList =
	{ "Hope in a bottle", "Win an OTO E-Twirl", "Game on with BRANDS today" };
	public static String[] AnnouncementsInfoList =
	{ "September 2015 gift box with healthy free replacement notice", "Name change - Iron + Vitamin B complex tablets chicken",
			"Health Mall Mobile Certification Bulletin" };
	// PLP
	public static String[] sortByItemListPLP = { "Position", "Product Name", "Price"};
	
	public static String[] sortByOptionValueList =
	{ "position", "name", "price" };

	// CER-317
	public static String windowTitleFacebook = "Facebook";
	public static String windowTitleTwitter = "Share a link on Twitter";
	// CER-276
	public static String errEN_ExistedAccOtherSiteLogin = "Account registered in others cerebos site";
	

	

}
