package com.qspj.appdata;

import java.util.ArrayList;
import java.util.Arrays;

import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;


@SuppressWarnings("javadoc")
public class StorefrontDataAction extends Environment
{
	private final ReqStorefrontData reqData = new ReqStorefrontData();

	public String getSiteURLData() throws Exception
	{
		try
		{
			
			return excelhandle.excelReadableGetGlobalVars().get(0);
			
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return "";
	}
	
	

	public ArrayList<String> getCategoriesListData(final String categoryItem) throws Exception
	{
		ArrayList<String> arrResult = null;
		String[] categoryList;
		String returnKey = "";
		try
		{
			returnKey = utils.getPropertyValue(ReqStorefrontData.propCategoryTreePath, categoryItem);
			categoryList = returnKey.split(",");
			arrResult = new ArrayList<String>(Arrays.asList(categoryList));
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return arrResult;
	}

	public String getBreadcrumMenuPathData(final String breadcrumbMenu) throws Exception
	{
		final String returnKey = "";
		try
		{
			return utils.getPropertyValue(ReqStorefrontData.propBreadcrumbMenuPath, breadcrumbMenu);
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return returnKey;
	}

	public String getProductSpecData(final String itemSpec) throws Exception
	{
		final String returnKey = "";
		try
		{
			return utils.getPropertyValue(ReqStorefrontData.propProdSpec, itemSpec);
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return returnKey;
	}

	public String parseToTitleValue(final String title) throws Exception
	{
		String titleValue = "";
		try
		{
			// verify data
			for (int i = 0; i < ReqStorefrontData.titleNameList.length; i++)
			{
				if (ReqStorefrontData.titleNameList[i].equals(title))
				{
					titleValue = ReqStorefrontData.titleValueList[i];
					break;
				}
			}
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return titleValue;
	}

	public ArrayList<String> getLandingPageDataList(final String landingPageItem) throws Exception
	{
		ArrayList<String> arrResult = null;
		String[] itemList;
		String returnKey = "";
		try
		{
			returnKey = utils.getPropertyValue(ReqStorefrontData.propLandingPagePath, landingPageItem);
			itemList = returnKey.split(",");
			arrResult = new ArrayList<String>(Arrays.asList(itemList));
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return arrResult;
	}

	public String getLandingPageDataValue(final String itemName) throws Exception
	{
		final String returnKey = "";
		try
		{
			return utils.getPropertyValue(ReqStorefrontData.propLandingPagePath, itemName);
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return returnKey;
	}

	// For District and Sub-District
	public ArrayList<String> getDistrictNameListData() throws Exception
	{
		ArrayList<String> arrResult = null;
		final String[] districtNameList;
		String returnKey = "";
		try
		{
			returnKey = utils.getPropertyValue(ReqStorefrontData.prop_CTNDistrictListPath, ReqStorefrontData.propDistrictListValue);
			districtNameList = returnKey.split(",");
			arrResult = new ArrayList<String>(Arrays.asList(districtNameList));
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return arrResult;
	}

	public ArrayList<String> getSubDistrictNameListData(final String districtName) throws Exception
	{
		ArrayList<String> arrResult = null;
		final String[] subDistrictNameList;
		String returnKey = "";
		try
		{
			returnKey = utils.getPropertyValue(ReqStorefrontData.prop_CTNSubDistrictListPath, districtName);
			subDistrictNameList = returnKey.split(",");
			arrResult = new ArrayList<String>(Arrays.asList(subDistrictNameList));
		}
		catch (final Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return arrResult;
	}
	
}
