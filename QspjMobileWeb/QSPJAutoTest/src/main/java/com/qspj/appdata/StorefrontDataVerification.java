package com.qspj.appdata;

import com.sai.configure.Environment;
import com.sai.utils.StackTraceInfo;

@SuppressWarnings("javadoc")
public class StorefrontDataVerification extends Environment {
	private final ReqStorefrontData sfData = new ReqStorefrontData();
	private final StorefrontDataAction sfDataAction = new StorefrontDataAction();

	public boolean vrfProductTabData(final String productTab) throws Exception {
		boolean isVrfData = false;
		try {
			// verify data
			for (int i = 0; i < ReqStorefrontData.productTabList.length; i++) {
				if (ReqStorefrontData.productTabList[i].equals(productTab)) {
					isVrfData = true;
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrfData;
	}

	public boolean vrfPLPSortByData(final String sortBy) throws Exception {
		boolean isVrfData = false;
		try {
			// verify data
			for (int i = 0; i < ReqStorefrontData.sortByItemListPLP.length; i++) {
				if (ReqStorefrontData.sortByItemListPLP[i].equals(sortBy)) {
					isVrfData = true;
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrfData;
	}

	/******** QSP *********************/

	public boolean vrfTitleData(final String title) throws Exception {
		boolean isVrfData = false;
		try {
			// verify data
			for (int i = 0; i < ReqStorefrontData.titleNameList.length; i++) {
				if (ReqStorefrontData.titleNameList[i].equals(title)) {
					isVrfData = true;
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrfData;
	}
//Prefecture name
	public boolean vrfPrefectureNameData(final String prefectureName) throws Exception {
		boolean isVrfData = false;
		try {
			// verify data
			for (int i = 0; i < ReqStorefrontData.prefectureNameList.length; i++) {
				if (ReqStorefrontData.prefectureNameList[i].equals(prefectureName)) {
					isVrfData = true;
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrfData;
	}
	
	public boolean vrfShipmentMethodData(final String shipmentMethod) throws Exception {
		boolean isVrfData = false;
		try {
			// verify data
			for (int i = 0; i < ReqStorefrontData.shipmentMethodList.length; i++) {
				if (ReqStorefrontData.shipmentMethodList[i].equals(shipmentMethod)) {
					isVrfData = true;
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrfData;
	}

	public boolean vrfPaymentMethodData(final String paymentMethod) throws Exception {
		boolean isVrfData = false;
		try {
			// verify data
			for (int i = 0; i < ReqStorefrontData.paymentMethodList.length; i++) {
				if (ReqStorefrontData.paymentMethodList[i].trim().equals(paymentMethod)) {
					isVrfData = true;
					break;
				}
			}
		} catch (final Exception e) {
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
		}
		return isVrfData;
	}
}
