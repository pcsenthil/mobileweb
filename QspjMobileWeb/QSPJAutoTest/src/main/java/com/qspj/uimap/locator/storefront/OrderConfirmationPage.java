package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class OrderConfirmationPage {

	public static String messOrderSuccess = "//div[@class='checkout-success__body']/div";
	public static String messOrderNo = "//div[@class='checkout-success__body']/p[1]/b";
	public static String messCustomerEmail = "//div[@class='checkout-success__body']/p[2]/b";

	public static String btContinueShopping = "//button[@class='btn btn-primary btn-block btn--continue-shopping js-continue-shopping-button']";
	public static String btBackToCart = "//button[@class='btn btn-default btn-block btn-checkout-back js-continue-shopping-button']";

	// PHASE2

	public static String txtOrderTotal = "//div[@class='order-summary-detail']/p[3]/span";
	public static String txtItem = "//div[@class='order-summary-detail']/p[4]/span";

	// GuestUSer
	public static String guestNewAccount = "//div[@class='checkout__new-account']";
	public static String guestNewAccountForm = "//form[@id='guestRegisterForm']";

	// Applied Coupons

	public static String couponCode = "//span[@class='coupon-code']";

}
