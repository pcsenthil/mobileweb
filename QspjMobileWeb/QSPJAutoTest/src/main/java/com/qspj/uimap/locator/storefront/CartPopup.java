package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class CartPopup {

	public static String cartCount = "//div[@class='mini-cart-count js-mini-cart-count']";
	public static String btCheckout = "//a[@class='btn btn-primary btn-block add-to-cart-button']";
	public static String btCheckoutMiniCart = "//a[@class='btn btn-primary btn-block mini-cart-checkout-button']";
	public static String btContinueShopping = "//a[@class='btn btn-default btn-block js-mini-cart-close-button']";
	public static String btClose = "//button[@id='cboxClose']/span[@class='glyphicon glyphicon-remove']";
	
	//Mini Cart
	public static String listProductItems = "//div[@id='cboxLoadedContent']/div/div/ol[@class='mini-cart-list']";
	public static String ListProductClassName = "mini-cart-item";
	
	
	//Pop Up displayed after Add to Cart
	
	public static String popUpAddedToCart = "//div[@id='addToCartLayer']/div[2]/h4";
	public static String pdtDetail = "//div[@id='addToCartLayer']/div[2]/div[2]/a";
	public static String pdtQty = "//div[@id='addToCartLayer']/div[2]/div[2]/div";
	
	
}
