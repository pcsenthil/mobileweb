 package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class LandingPage {
	// QSP
	public static String storefrontWindowTitle = "QSPJ Site | Homepage";
	public static String btnMenu = "//div[@class='mobile-menu']/button";
	public static String btnMenuClose = "//div[@class='close-nav']/button";
	public static String btnSearch = "//li[@class='visible-xs visible-sm mob-search-nav']/button";
	public static String lnkSignInRegister = "//ul[@class='sticky-nav-top hidden-lg hidden-md js-sticky-user-group hidden-md hidden-lg']/li[1]/a";
	public static String menuList = "//ul[@class='nav__links nav__links--products js-offcanvas-links']";
	public static String txtSearch = "//input[@id='js-site-search-input']";
	public static String iconSearch = "//button[@class='btn btn-link js_search_button']";
	public static String searchAutoCompleteList = "//ul[@class='ui-autocomplete ui-front ui-menu ui-widget ui-widget-content']";
	public static String searchAutoCompItemClassName = "ui-menu-item";
	public static String iconCart = "//a[@class='mini-cart-link js-mini-cart-link']/div";
	public static String ddCurrency = "//select[@id='currency_selector_LangCurrencyComponent']";

	// Header Link
	public static String lnkMyAccount = "//*[@id='signedInUserAccountToggle']/span";
	//public static String lnkMyAccount = "//span[@class='glyphicon myAcctExp glyphicon-chevron-down']";
	
	public static String lnkListMyAccount = "//div[@class='myAccountLinksContainer js-myAccountLinksContainer']/ul[1]";
	public static String lnkSignOut = "//a[@href='/qspjstorefront/qspj/en/logout']";

	public static String lnkShopAllDepartment = "//a/i[@class='glyphicon glyphicon-menu-hamburger']";
	public static String lnkListShopAllDept = "//ul[@class='nav__links nav__links--products js-offcanvas-links']";
	public static String ListShopAllDeptClassName = "//ul[@class='nav__links nav__links--products js-offcanvas-links']";
	public static String lnkListSubCategory = "//div[@class='sub-navigation-section col-md-12']/ul[@class='sub-navigation-list']";
	/**************************************/

	public static String topheaderlnk = "//section[@class='top-header hidden-xs']";
	public static String topmenuCurrencyIcon = "//form[@id='currency_form_LangCurrencyComponent']";
	public static String topmenuLanguageIcon = "//form[@id='lang_form_LangCurrencyComponent']";
	public static String sighinFRegisterLink = "//li[@class='liOffcanvas']";
	public static String bottomheaderlnk = "//section[@class='header-section ']";
	public static String landingPageLogo = "//div[@class='yCmsComponent yComponentWrapper logo-blk']/div[@class='banner__component banner']/a/img";
	public static String areaCardSlot = "//div[@class='yCmsContentSlot componentContainer']";

	public static String mainMenuList = "//div[@class='col-md-3 menu hidden-xs hidden-sm']";
	public static String areaMainMenu = "//div[@class='navigation__overflow']/ul[2]";

	public static String topContentpagecomponent = "//div[@class='row hidden-xs hidden-sm top-blk']";
	public static String topContentpagecomponent1 = "//div[@class='row hidden-xs hidden-sm top-blk']/div[1]";
	public static String topContentpagecomponent2 = "//div[@class='row hidden-xs hidden-sm top-blk']/div[2]";
	public static String topContentpagecomponent3 = "//div[@class='row hidden-xs hidden-sm top-blk']/div[3]";
	public static String banner = "//div[@class='yCmsContentSlot home-banner carousel__component--carousel js-owl-carousel js-owl-rotating-image js-owl-carousel-reference owl-carousel owl-theme']";
	public static String advertiseLink = "//div[@class='yCmsContentSlot row ']";
	public static String advertising1 = "//div[@class='yCmsContentSlot row ']/div[1]";
	public static String advertising2 = "//div[@class='yCmsContentSlot row ']/div[2]";
	public static String advertising3 = "//div[@class='yCmsContentSlot row ']/div[3]";
	public static String advertising4 = "//div[@class='yCmsContentSlot row ']/div[4]";
	public static String advertising5 = "//div[@class='yCmsContentSlot row ']/div[5]";
	public static String advertising6 = "//div[@class='yCmsContentSlot row ']/div[6]";
	public static String newArrivalComponent = "//div[@class='yCmsComponent product-slider']";
	public static String bigBannerComponent1 = "//div[@class='yCmsContentSlot row banner-add']";
	public static String bgBanner1 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Open-Catalogue/Computers/tablet/c/1506']";
	public static String bgBanner2 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Open-Catalogue/Cameras/Digital-Cameras/Digital-Compacts/c/576']";
	public static String bgBanner3 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Open-Catalogue/Computers/laptop/c/1505']";
	public static String bestSeller = "//div[@class='yCmsComponent no-space yComponentWrapper seller-carousel product-slider']";
	public static String bigBannerComponent2 = "//div[@class='container']/div[7]";
	public static String bgBanner4 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Open-Catalogue/Computers/laptop/c/1505']";
	public static String bgBanner5 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Open-Catalogue/Mobiles/c/1500']";
	public static String bgBanner6 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Open-Catalogue/Televisions/c/1507']";
	public static String bgBanner7 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Brands/Sony/c/brand_5']";
	public static String bgBanner8 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Brands/Canon/c/brand_10']";
	public static String bgBanner9 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Brands/Kodak/c/brand_88']";
	public static String bgBanner10 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Brands/Toshiba/c/brand_2']";
	public static String bgBanner11 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Brands/Samsung/c/brand_26']";
	public static String bgBanner12 = "//div[@class='banner__component banner']/a[@href='/qspjstorefront/qspj/en/Brands/Kodak/c/brand_88']";
	public static String bigBannerComponent3 = "//div[@class='yCmsContentSlot brand-carousel carousel__component--carousel js-owl-carousel js-owl-lazy-reference js-owl-carousel-reference owl-carousel owl-theme']";
	public static String corporateTitle = "//div[@class='col-xs-12 col-sm-12 col-md-4 about']/h3";
	public static String corporateList = "//div[@class='col-xs-12 col-sm-12 col-md-4 about']/div/ul";
	public static String qckAccess = "//div[@class='col-xs-12 col-sm-12 col-md-4 col-md-offset-1 quick-access']/h3";
	public static String qckAccessList = "//div[@class='col-xs-12 col-sm-12 col-md-4 col-md-offset-1 quick-access']/div/ul";
	public static String qckAccessList1 = "//div[@class='col-xs-12 col-sm-12 col-md-4 col-md-offset-1 quick-access']/div/ul[2]";
	public static String followUSLink = "//div[@class='col-xs-12 col-sm-12 col-md-2 col-md-offset-1 follow-us']/h3";
	public static String facebooklink = "//div[@class='col-xs-12 col-sm-12 col-md-2 col-md-offset-1 follow-us']/ul/li[1]";
	public static String utubeLink = "//div[@class='col-xs-12 col-sm-12 col-md-2 col-md-offset-1 follow-us']/ul/li[2]";
	public static String twitterLink = "//div[@class='col-xs-12 col-sm-12 col-md-2 col-md-offset-1 follow-us']/ul/li[3]";
	public static String googlePluslink = "//div[@class='col-xs-12 col-sm-12 col-md-2 col-md-offset-1 follow-us']/ul/li[4]";
	public static String copyRight = "//div[@class='copyrights-blk']/div[@class='container']/p";
	public static String welcomeMessage1 = "//li[@class='logged_in js-logged_in']";
	public static String myAccount = "//a[@class='myAccountLinksHeader collapsed js-myAccount-toggle']";

	// My Profile

	public static String lnkUpdatePersonalDetails = "//a[@class='profile']";
	public static String ddTitle = "//select[@id='profile.title']";
	public static String txtFirstName = "//input[@id='profile.firstName']";
	public static String txtLastName = "//input[@id='profile.lastName']";
	public static String btSubmit = "//button[@class='btn btn-primary btn-block']";
	public static String alertSuccess = "//div[@class='alert alert-info alert-dismissable']";
	// Change Password
	public static String lnkChangeYourPassword = "//a[@class='update-password']";
	public static String txtCurrentPassword = "//input[@id='currentPassword']";
	public static String txtNewPassword = "//input[@id='newPassword']";
	public static String txtConfirmNewPassword = "//input[@id='checkNewPassword']";
	public static String btPwdSubmit = "//button[@class='btn btn-primary btn-block']";
	// Email Address Change
	public static String lnkChangeYourEmail = "//a[@class='update-email']";
	public static String txtNewEmail = "//input[@id='profile.email']";
	public static String txtConfEmail = "//input[@id='profile.checkEmail']";
	public static String txtPassword = "//input[@id='profile.pwd']";
	public static String btChangeEmailSubmit = "//button[@class='btn btn-primary btn-block']";
	// Wishlist
	public static String lnkDefaultWishlist = "//a[@href='/qspjstorefront/qspj/en/my-account/wishlist/defaultWishlist?wishlistName=Default Wishlist']";
	public static String lnkPrivateWishlist = "//a[@href='/qspjstorefront/qspj/en/my-account/wishlist/defaultWishlist?wishlistName=Senthil Private']";
	public static String lnkPublictWishlist = "//a[@href='/qspjstorefront/qspj/en/my-account/wishlist/defaultWishlist?wishlistName=Senthil Public']";

	public static String defmsg = "//div[@class='wishlist-noproduct-msg']";


	// public static String btnDeleteitem="//a[@class='glyphicon
	// glyphicon-remove']";
	public static String btnDeleteitem = "//div[@class='removebtn']/a";

	public static String listingItemsInWishlist = "//div[@class='listing-wishlist']";
	public static String itemRemoveClassName = "wishlist-remove";
	public static String btnDeletePublicWishlist = "//div[@class='listing-wishlist']/li[3]/span[2]/a/i";

	// public static String
	// txtExistPublicWishlist="//span[@class='wishlist-name']/a";
	public static String texPublicwishlist = "//div[@class='listing-wishlist']/li[3]/span[2]/a/i";
	public static String btnWishlistAddToCart = "//button[@id='saveCartButton']";
	// Find A Wishlist
	public static String texWishlistEmail = "//form[@id='addToWishListDropDown']/div[1]/div[1]/div/div/input";
	public static String btnFindNow = "//form[@id='addToWishListDropDown']/div[1]/div[2]/input";
	public static String defmsgFindWishlist = "//div[@wishlist-top-nav']/div";
	public static String lnkSeeAllWishlists = "//a[@class='js-see-all-lists']";
	public static String btnOthersWishlistAddToCart = "//button[@id='saveCartButton']";
	// Cancel Order
	public static String btnCancelOrder = "//div[@class='col-md-3 pull-right']";
	public static String btnCancelOrderPopup = "//class[@id='btn btn-primary btn-block confirm-btn']";

}
