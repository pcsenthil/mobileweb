package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class ProductDetailPage {

	// QSP Project

	public static String breadcrumHome = "//li [@class='active']";
	public static String lbProductName = "//div[@class='product-details']/div[1]/div[1]";
	public static String lbProductPrice = "//p[@class='price']";
	public static String txtReview = "//a[@class='js-writeReviewTab']";
	public static String conShowReview = "//a[@class='js-openTab']";
	public static String txtQuantity = "//input[@id='pdpAddtoCartInput']";
	public static String btnQuantityPlus = "//button[@class='btn btn-default js-qty-selector-plus']";
	public static String btnQuantityMinus = "//button[@class='btn btn-default js-qty-selector-minus']";
	public static String btAddToCart = "//button[@id='addToCartButton']";
	//public static String conProductDetails = "//ul[@class='clearfix tabs-list tabamount4']";
	/*public static String conDescrip = "//li[@id='accessibletabsnavigation0-0']/a";
	public static String conSpec = "//li[@id='accessibletabsnavigation0-1']/a";
	public static String conReview = "//li[@id='accessibletabsnavigation0-2']/a";
	public static String conReview1 = "//a[@id='tabreview']";
	public static String conDelivery = "//li[@id='accessibletabsnavigation0-3']/a";
	public static String carslLikeThis = "//div[@class='carousel__component']";*/
	
	public static String alertSuccess= "//div[@class='alert alert-info alert-dismissable']";
	//shyamala
	public static String productImageGallery = "//div[@class='image-gallery js-gallery']";
	  public static String productImage = "//div[@class='img-blk']";
	  public static String productName = "(//div[@class='name'])[2]";
	  public static String productRating = "(//div[@class='rating'])[2]";
	  public static String productReviews = "(//*[contains(text(),'Show Reviews')])[2]";
	  public static String productPrice = "//div[@class='add-cart-blk']/p";
	  public static String conProductDetails = "//div[@class='product-details']";
	  public static String productDescription = "(//div[@class='description'])[1]";
	    
	  public static String conDescrip = "//li[@id='accessibletabsnavigation0-0']/a";
	  //(//*[contains(text(),'Description')])[1]
	  public static String conSpec = "//li[@id='accessibletabsnavigation0-1']/a";
	  public static String conReview =  "//li[@id='accessibletabsnavigation0-2']/a";
	  public static String conReview1 = "//a[@id='tabreview']";
	  public static String carslLikeThis = "//div[@class='carousel__component']";
	  
	  public static String sharelabel = "(//*[contains(text(),'Share')])[1]";
	  public static String shareFacebook = "//div[@class='AddToCart-ShareOnSocialNetworkAction']/div/a";
	  public static String facebookPopup = "//body[@class='login_page  _4idf booklet chrome webkit win x1 Locale_en_GB']/div/div/div/div/h2";
	  public static String homepageWindow = "//img[@title='SAI DIGITAL']";
	  public static String shareGooglePlus = "//div[@class='AddToCart-ShareOnSocialNetworkAction']/div/a[3]";
	  public static String addToCompare = "//span[text()='Add to Compare']";
	  
	  public static String facebookLogin = "(//input[@class='inputtext _55r1 inputtext inputtext'])[1]";
	  public static String facebookpsw = "(//input[@class='inputtext _55r1 inputtext inputtext'])[2]";
	  public static String facebookSubmit = "//input[@name ='login']";
	  public static String facebookTitle = "(//*[contains(text(),'Post to Facebook')])[1]";
	  public static String ButtonPostFacebook = "(//*[contains(text(),'Post to Facebook')])[2]";


	}

