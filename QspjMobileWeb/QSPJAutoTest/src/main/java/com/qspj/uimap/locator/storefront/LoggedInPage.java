package com.qspj.uimap.locator.storefront;

public class LoggedInPage {

	public static String lbGeneralMessage = "//div[@class='global-alerts']/div[@class='alert alert-info alert-dismissable']";
	public static String lbUserWelcome = "//ul[@class='sticky-nav-top hidden-lg hidden-md js-sticky-user-group hidden-md hidden-lg']/li/div[1]/div";
	public static String alertSuccess = "//div[@class='alert alert-info alert-dismissable']";
	public static String btnClosealertSuccess = "//div[@class='alert alert-info alert-dismissable']/button";
	public static String lnkMyAccount = "//a[@href='#accNavComponentDesktopOne']";
	public static String lnkSignOut = "//div[@id='accNavComponentDesktopOne']/ul/li[5]/a";

	// sub menu
	public static String subItemMyAccount = "//div[@class='account-info']";
	public static String subItemFavoriteListCTN = "//a[@href='/cerebosstorefront/ctn/en/wish-list']";
	public static String subItemFavoriteListCSIN = "//a[@href='/cerebosstorefront/csin/en/wish-list']";
	public static String subItemFavoriteListCHK = "//a[@href='/cerebosstorefront/chk/en/wish-list']";
	public static String subItemFavoriteListCTL = "//a[@href='/cerebosstorefront/ctl/en/wish-list']";

	// sub item of My Account
	public static String subItemUpdatePersonalDetails = "//a[@title='Update Personal Details']";
	public static String subItemUpdateEmail = "//a[@title='Update Email']";
	public static String subItemPaymentDetails = "//a[@title='Payment Details']";
	public static String subItemUpdatePassword = "//a[@title='Update Password']";
	public static String subItemOrderHistory = "//a[@title='Order History']";
	public static String subItemAddressBook = "//a[@title='Address Book']";
	public static String subItemLoyaltyCorner = "//a[@title='Loyalty Corner']";

}
