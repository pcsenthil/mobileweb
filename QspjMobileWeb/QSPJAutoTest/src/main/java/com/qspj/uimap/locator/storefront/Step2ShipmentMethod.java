package com.qspj.uimap.locator.storefront;

public class Step2ShipmentMethod {

	// QSP Project

	public static String ShippingAdd = "//span[@class='address']";
	public static String itemList = "//div[@class='checkout-shipping-items']";
	public static String drpdwnShipmentMethod = "//select[@id='delivery_method']";
	public static String ShipmentMethodOpt1 = "//select[@id='delivery_method']/option[@value='standard-gross']";
	public static String ShipmentMethodOpt2 = "//select[@id='delivery_method']/option[@value='premium-gross']";
	public static String btnNext = "//button[@id='deliveryMethodSubmit']";

	public static String orderSummarySection = "//div[@class='checkout-order-summary']";
	public static String orderListItems = "//ul[@class='checkout-order-summary-list']";
	public static String OrderSummaryAddress = "//div[@class='checkout-order-summary-list-heading']/div[@class='address']";
}
