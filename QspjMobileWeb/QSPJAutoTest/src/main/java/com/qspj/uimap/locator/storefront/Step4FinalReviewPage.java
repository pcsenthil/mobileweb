package com.qspj.uimap.locator.storefront;

public class Step4FinalReviewPage {

	public static String orderSummary = "//div[@class='checkout-order-summary']";
	public static String chbAcceptTerms = "//span[contains(text(),'I have read and agree with')]";
	public static String txtOPT = "//input[@id='otp']";
	public static String btResendOPT = "//button[@class='btn resendOTP']";
	public static String btPlaceOrder = "//button[@id='placeOrder']";

	public static String orderSummarySection = "//div[@class='yCmsComponent checkout__confirmation__content--component s']";
	public static String orderListItems = "//div[@class='yCmsComponent checkout__confirmation__content--component s']/div/div[3]";
	public static String OrderSummaryAddress = "//div[@class='yCmsComponent checkout__confirmation__content--component s']/div/div[1]";
	public static String OrderSummaryPaymentAddress = "//div[@class='yCmsComponent checkout__confirmation__content--component s']/div/div[2]";
	public static String OrderSummaryTotal = "//div[@class='yCmsComponent checkout__confirmation__content--component s']/div/div[4]";
}
