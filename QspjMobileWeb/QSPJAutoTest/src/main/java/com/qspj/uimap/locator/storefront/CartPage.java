package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class CartPage {

	// QSP Project

	public static String btnCarticon = "//span[@class='glyphicon glyphicon-shopping-cart']";
	public static String emptyCartBanner = "//img[@alt='Order before 6pm for next day delivery']";
	public static String btnCheckout1 = "//div[@class='cart-header']/div/div[3]/div/div/button[@class='btn btn-primary btn-block btn--continue-checkout js-continue-checkout-button']";
	public static String btnCheckout2 = "//div[@class='cart__actions']/div/div[1]/button[@class='btn btn-primary btn-block btn--continue-checkout js-continue-checkout-button']";
	public static String btnDeleteitem = "//div[contains(@id, 'removeEntry')]/a/span";
	public static String txtQuantity = "//input[@id='quantity_0']";
	public static String btnDecreaseClassName = "glyphicon glyphicon-minus minus-count";
	public static String btnIncreaseClassName = "glyphicon glyphicon-plus add-count";
	
	public static String btnConShopping = "//button[@class='btn btn-default btn-block btn--continue-shopping js-continue-shopping-button']";
	public static String linkHelp = "//a[@class='help js-cart-help']";
	public static String HelpPopup = "//div[@class='help-popup-content']";
	public static String btnClosePopup = "//button[@id='cboxClose']/span";

	public static String listItemsInCart = "//ul[@class='item__list item__list__cart']";

	public static String alertInfo = "//div[@class='alert alert-info alert-dismissable']";
	public static String messEmptyCart = "//div[@class='yCmsComponent yComponentWrapper content__empty']/div/h2";
	public static String itemEntryClassName = "item__list--item";

	public static String txtCouponCode = "//input[@id='js-voucher-code-text']";
	public static String btnCouponApply = "//button[@id='js-voucher-apply-btn']";
	public static String lblCouponMessage = "//div[@class='js-voucher-validation-container help-block cart-voucher__help-block']";
	public static String listAppliedCoupons = "//ul[@id='js-applied-vouchers']";
	public static String listitemAppliedCouponsClassName = "voucher-list__item";
	public static String lblDiscountValue = "//div[@class='js-cart-totals']/div[@class='col-xs-6 cart-totals-right text-right discount']";
	public static String lblSubtotal = "//div[@class='js-cart-totals']/div[@class='col-xs-6 cart-totals-right text-right']";
	public static String lblOrderTotal = "//div[@class='js-cart-totals']/div[@class='col-xs-6 cart-totals-right text-right grand-total']";

	public static String listItemsInWishlist = "(//ul[@class='item__list item__list__cart'])[2]";
	public static String listItemsInWish = "item__list--item";
	public static String SveForLtrAlertInfo = "//div[@class='alert alert-info alert-dismissable']";
	public static String btMveToCrt = "//button[@id='saveCartButton']";
	public static String alertContent = "//div[@class='content']/h2";
	public static String btnSveFrLtrDeleteitem = "//button[@id='savedForLaterCartRemoveButton']";
	public static String btnDeleteSVitem = "//button[@id='savedForLaterCartRemoveButton']";
	//shyamala
	//div[@class='cart__actions']/div/div/button[@class='btn btn-default btn-block btn--continue-shopping js-continue-shopping-button']";
	
	

}
