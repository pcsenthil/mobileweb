package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class SearchResultPage {
	public static String lbSearchEmpty = "//main/div[3]/div[@class='search-empty']/div[@class='headline']";
	public static String lbSearchResults = "//div[@class='pagination-bar-results']";
	public static String lstProductList = "//ul[@class='product__listing product__grid js-list-reference']";

	public static String txtSearch = "//input[@id='js-site-search-input']";
	public static String btnSearchicon = "//button[@class='btn btn-link js_search_button']";
	public static String drpdwnSortby = "//select[@id='sortOptions1']";
	public static String drpdwnOpt1 = "//select[@id='sortOptions1']/option[@value='relevance']";
	public static String drpdwnOpt2 = "//select[@id='sortOptions1']/option[@value='topRated']";
	public static String drpdwnOpt3 = "//select[@id='sortOptions1']/option[@value='name-asc']";
	public static String drpdwnOpt4 = "//select[@id='sortOptions1']/option[@value='name-desc']";
	public static String drpdwnOpt5 = "//select[@id='sortOptions1']/option[@value='price-asc']";
	public static String drpdwnOpt6 = "//select[@id='sortOptions1']/option[@value='price-desc']";
	public static String btnAddToCart = "//button[@class='btn btn-primary btn-block js-enable-btn']";
	public static String btnQuickView = "//span[@class='glyphicon glyphicon-search']";
	public static String btnPopupAddToCart = "//button[@id='addToCartButton']";
	public static String btnClose = "//button[@id='cboxClose']";
	
	//shyam
	
	public static String searchTextDisplay = "//a[@title='PL60 Silver']";

}
