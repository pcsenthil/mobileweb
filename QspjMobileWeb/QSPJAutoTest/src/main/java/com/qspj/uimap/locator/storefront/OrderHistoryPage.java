package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class OrderHistoryPage {
	public static String titlePage = "//h1[@class='heading-title hidden-xs']";
	public static String cbFilterBy = "//select[id='filterOrder']";
	public static String tabPagination = "//ul[@class='pagination']";
	public static String tableOrder = "//table[@class='orderhistory-list-table responsive-table']";
	public static String orderList = "//table[@class='orderhistory-list-table responsive-table']/tbody";
	public static String btNextPage = "//div[@class='pagination-bar bottom']/div/div[2]/div/div/ul/li/a[@class='glyphicon glyphicon-chevron-right']";
	public static String firstOrder = "//table[@class='orderhistory-list-table responsive-table']/tbody/tr[2]/td[2]/a";

	// Order History
	public static String drpdwnSortby = "//select[@id='sortOptions1']";
	public static String tabPaginationTop = "//div[@class='col-xs-12 col-md-6 pull-right my-acc ']/div[@class='pagination-bar top']";
	public static String tabPaginationBottom = "//div[@class='pagination-bar bottom']";
	public static String pagination = "//div[@class='pagination-bar bottom']/div/div/div/div/ul[@class='pagination']";

	// Order Cancel

	public static String btnCancelOrder = "//form[@id='cancelorderForm']/button[@id='cancelOrderButton']";
	public static String btnConfirmToCancelOrderYes = "//div[@id='cboxContent']/div[1]/div/div[2]/form[@id='confirmcancelorderForm']/button";
	public static String btnConfirmToCancelOrderNo = "//*[@id='cboxLoadedContent']/div/div[3]/button";
	public static String alertCancelOrderMsg = "//div[@class='alert alert-warning alert-dismissable']";

}
