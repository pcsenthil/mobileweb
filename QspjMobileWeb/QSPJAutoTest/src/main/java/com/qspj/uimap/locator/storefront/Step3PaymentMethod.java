package com.qspj.uimap.locator.storefront;

public class Step3PaymentMethod {

	public static String rbCreditDebitCard = "//label[@for='creditCartOption']";
	public static String rbCashOnDelivery = "//label[@for='cashOnOption']";
	public static String rbBankTransfer = "//label[@for='bankOption']";
	public static String rbKonbiniConvenientStore = "//label[@for='konbiniOption']";
	public static String rbCybersource = "//label[@for='cyberSourceOption']";
	public static String rbLinePay = "//label[@for='linepayOption']";
	public static String rbPaypal = "//label[@for='paypalOption']";
	public static String rbNCCC = "//label[@for='ncccOption']";

	public static String ddCardType = "//select[@id='card_cardType']";
	public static String txtNameOnCard = "//input[@id='card_nameOnCard']";
	public static String txtCardNumber = "//input[@id='card_accountNumber']";
	public static String ddExpiryMonth = "//select[@id='ExpiryMonth']";
	public static String ddExpiryYear = "//select[@id='ExpiryYear']";
	public static String txtCVV = "//input[@id='card_cvNumber']";

	public static String rbSameAsShippingAddress = "//label[@for='useDeliveryAddress1']";
	public static String rbNewBillingAddress = "//label[@for='useDeliveryAddress2']";

	public static String btnNext = "//button[@id='paymentModeSubmit']";

	public static String orderSummarySection = "//div[@class='checkout-order-summary']";
	public static String orderListItems = "//ul[@class='checkout-order-summary-list']";
	public static String OrderSummaryAddress = "//div[@class='checkout-order-summary-list-heading']/div[@class='address']";

	// Billing Address

	public static String formBillingAddress = "//div[@id='billingAddressForm']";
	public static String rbtnSameShipAdd = "//label[@for='useDeliveryAddress1']";
	public static String rbtnNewShipAdd = "//label[@for='useDeliveryAddress2']";
	public static String cbCountry = "//select[@id='address.country']";
	public static String cbTitle = "//select[@id='address.title']";
	public static String txtFirstName = "//input[@id='address.firstName']";
	public static String txtLastName = "//input[@id='address.surname']";
	public static String txtPostCode = "//input[@id='address.postcode']";
	public static String cbPrefecName = "//select[@id='address.region']";
	public static String txtCity = "//input[@id='address.townCity']";
	public static String txtSubArea = "//input[@id='address.line2']";
	public static String txtFurtSubArea = "//input[@id='address.line1']";

}
