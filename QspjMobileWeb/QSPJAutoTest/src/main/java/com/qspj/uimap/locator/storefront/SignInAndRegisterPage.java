package com.qspj.uimap.locator.storefront;

@SuppressWarnings("javadoc")
public class SignInAndRegisterPage {
	// QSP Project
	// Element of Login Area
	public static String txtEmail = "//input[@id='j_username']";
	public static String txtPassword = "//input[@id='j_password']";
	public static String lnkForgotPassword = "//a[@class='js-password-forgotten']";
	//public static String btLogin = "//button[@class='btn btn-primary btn-block']";
	public static String btLogin = "//*[@id='loginForm']/button";
	
	
	public static String btLoginWithFB = "//div[@class='btn btn-block btn-default btn-loginfb']";

	public static String btLoginWithGoogle = "//div[@id='customBtn']";

	// Google
	public static String googleSignInWindowTitle = "Sign in - Google Accounts";
	public static String txtGoogleEmail = "//input[@id='identifierId']";
	public static String btnGoogleNext = "//span[@class='RveJvd snByac']";
	public static String txtGooglePassword = "//input[@class='whsOnd zHQkBf']";
	public static String btnGoogleNext1 = "//span[@class='RveJvd snByac']";

	// Facebook
	public static String facebookSignInWindowTitle = "Facebook";
	public static String txtFBEmail = "//input[@id='email']";
	public static String txtFBPassword = "//input[@id='pass']";
	public static String btnFBLogin = "//input[@value='Log In']";

	// Element of Create Account area

	public static String ddTitle = "//select[@id='register.title']";
	public static String txtLastName = "//input[@id='register.lastName']";
	public static String txtFirstName = "//input[@id='register.firstName']";
	public static String txtEmailRegister = "//input[@id='register.email']";
	public static String txtPasswordRegister = "//input[@id='password']";
	public static String txtConfirmPassword = "//input[@id='register.checkPwd']";
	public static String btRegister = "//button[@class='btn btn-default btn-block']";
	// forgotYourPassword
	public static String forgotPwd = "//button[@class='btn btn-default btn-block']";

	// Guest user checkout page

	public static String btExpCheckout = "//button[@class='btn btn-default btn-block expressCheckoutButton']";
	public static String txtGuestEmail = "//input[@id='guest.email']";
	public static String txtGuestConEmail = "//input[@id='guest.confirm.email']";
	public static String btGuestCheckout = "//button[@class='btn btn-default btn-block guestCheckoutBtn']";

	// Forgotpassword popup
	public static String txtForgotPasswordPopup = "//div[@class='forgotten-password']";
	public static String txtUserId = "//input[@id='forgottenPwd.email']";
	// public static String btResetPassword = "//button[@class='btn btn-primary
	// btn-block']";
	public static String btResetPassword = "//*[@id='forgottenPwdForm']/div[1]/button";
	public static String txtResetInstruction = "//div[@class='reset instructions']";

}
