package com.qspj.utils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import jxl.Sheet;
import jxl.Workbook;

import com.qspj.appcommon.AppInit;
import com.sai.configure.GlobalVariables;
import com.sai.configure.InputDataExcelConfig;
import com.sai.utils.ExcelHandle;
import com.sai.utils.StackTraceInfo;

public class StoreFrontExcelHandler extends ExcelHandle {

	@Override
	public ArrayList<String> excelReadableGetGlobalVars() throws Exception
	{
		try
		{
			// Open excel file
			ArrayList<String> globalVar = new ArrayList<String>();
			File inputWorkbook = new File(GlobalVariables.excelDataInput);
			Workbook workbook = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = workbook.getSheet(GlobalVariables.INPUTDATA_SHEET_NAME);
			// get global variables on Excel file
			globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Storefront URL").getRow()).getContents());
			
//			globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("CerebosHK site").getRow()).getContents());
//			globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("CerebosTL site").getRow()).getContents());
//			globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("CerebosTN site").getRow()).getContents());
			return globalVar;
		}
		catch (Exception e)
		{
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
			return null;
		}
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean excelReadableRunStep(String stepName, String[] param)
			throws Exception {
		boolean isExec = false;
		try {
			String className = "";
			ArrayList<String> listMethodOfClass = new ArrayList<String>();
			if (AppInit.availCommonScript.contains(stepName)) {
				listMethodOfClass = AppInit.availCommonScript;
				className = AppInit.commonTestscriptClass;
			} 
			else if (AppInit.availStorefrontScript.contains(stepName)) {
				listMethodOfClass = AppInit.availStorefrontScript;
				className = AppInit.storefrontTestscriptClass;
			} 
			
			// Init class instance
			Class cls = Class.forName(className);
			Object obj = cls.newInstance();

			// Invoke method
			if (listMethodOfClass.contains(stepName)) {
				Method method = null;
				if (param.length == 0) {
					try {
						method = cls.getDeclaredMethod(stepName);
						method.invoke(obj);
					} catch (Exception e) {
						log.error(StackTraceInfo.getCurrentClassName());
						log.error(StackTraceInfo.getCurrentFileName());
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + "STEP '" + stepName
								+ "' REQUIRE DATA");
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + e.getMessage());
						// set False Result
						utils.setFalseResult("STEP '" + stepName
								+ "' REQUIRE DATA");
					}
				} else {
					try {
						method = cls.getDeclaredMethod(stepName,
								param.getClass());
						method.invoke(obj, (Object) param);
					} catch (Exception e) {
						log.error(StackTraceInfo.getCurrentClassName());
						log.error(StackTraceInfo.getCurrentFileName());
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + "STEP '" + stepName
								+ "' DO NOT REQUIRE DATA");
						// set False Result
						utils.setFalseResult("STEP '" + stepName
								+ "' DO NOT REQUIRE DATA");
					}
				}
				isExec = true;
			} else {
				log.error(StackTraceInfo.getCurrentClassName());
				log.error(StackTraceInfo.getCurrentFileName());
				log.error(StackTraceInfo.getCurrentMethodName() + log.tab
						+ "STEP '" + stepName + "' DO NOT EXIST IN TESTSCRIPT");
				// set False Result
				utils.setFalseResult("STEP '" + stepName
						+ "' DO NOT EXIST IN TESTSCRIPT");
			}
			return isExec;
		} catch (Exception e) {
			log.error(log.tab + "STEP " + stepName
					+ "' EXECUTE BUT THROWED EXCEPTION");
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab
					+ e.getMessage());
			utils.setFalseResult("STEP " + stepName
					+ "' EXECUTE BUT THROWED EXCEPTION");
			return isExec;
		}
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean excelReadableRunStepNew(String stepName, String[] param) throws Exception {
		boolean isExec = false;
		String className = "";
		try {
			
			log.info("className 0 "+className);
			log.info("className 0 "+AppInit.allApplicationList.size());
			
			// search step name on all application list
			for(int i=0; i < AppInit.allApplicationList.size(); i ++) {
				
				log.info("className 1 "+className);
				
				if (AppInit.allApplicationList.get(i).contains(stepName)) {
					int itemCount = AppInit.allApplicationList.get(i).size();
					className = (String) AppInit.allApplicationList.get(i).get(itemCount-1);
					
					log.info("className 2"+className);
					
				} 
			}

			// Init class instance
			Class cls = Class.forName(className);
			Object obj = cls.newInstance();

			// Invoke method
			if (!"".equals(className)) {
				Method method = null;
				if (param.length == 0) {
					try {
						method = cls.getDeclaredMethod(stepName);
						method.invoke(obj);
					} catch (Exception e) {
						log.error(StackTraceInfo.getCurrentClassName());
						log.error(StackTraceInfo.getCurrentFileName());
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + "STEP '" + stepName
								+ "' REQUIRE DATA");
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + e.getMessage());
						// set False Result
						utils.setFalseResult("STEP '" + stepName
								+ "' REQUIRE DATA");
					}
				} else {
					try {
						method = cls.getDeclaredMethod(stepName,
								param.getClass());
						method.invoke(obj, (Object) param);
					} catch (Exception e) {
						log.error(StackTraceInfo.getCurrentClassName());
						log.error(StackTraceInfo.getCurrentFileName());
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + "STEP '" + stepName
								+ "' DO NOT REQUIRE DATA");
						// set False Result
						utils.setFalseResult("STEP '" + stepName
								+ "' DO NOT REQUIRE DATA");
					}
				}
				isExec = true;
			} else {
				log.error(StackTraceInfo.getCurrentClassName());
				log.error(StackTraceInfo.getCurrentFileName());
				log.error(StackTraceInfo.getCurrentMethodName() + log.tab
						+ "STEP '" + stepName + "' DO NOT EXIST IN TESTSCRIPT");
				// set False Result
				utils.setFalseResult("STEP '" + stepName
						+ "' DO NOT EXIST IN TESTSCRIPT");
			}
			return isExec;
		} catch (Exception e) {
			log.error(log.tab + "STEP " + stepName
					+ "' EXECUTE BUT THROWED EXCEPTION");
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab
					+ e.getMessage());
			utils.setFalseResult("STEP " + stepName
					+ "' EXECUTE BUT THROWED EXCEPTION");
			return isExec;
		}
	}

	@Override
	public void getAllScript() throws Exception {
		try {
			// get all script of Common
			Class cls = Class.forName(AppInit.commonTestscriptClass);
			Object obj = cls.newInstance();
			Method[] tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availCommonScript.add(tempmethod[i].getName());
			}

			// get all script of Storefront
			cls = Class.forName(AppInit.storefrontTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availStorefrontScript.add(tempmethod[i].getName());
			}
		
			log.info("GET ALL SCRIPTS OF APPLICATION SUCCESSFUL");
		} catch (Exception e) {
			log.error("COULD NOT GET ALL SCRIPTS OF APPLICATION");
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab
					+ e.getMessage());
			throw new Exception();
		}
	}
	
	public void getAllScriptNew() throws Exception {
		try {
			// get all classes of testscript package- application function
			ApplicationReflectionsSearch appReflectionSearch = new ApplicationReflectionsSearch(AppInit.appTestScriptPackage);
			Set<Class<?>> classesAppList = appReflectionSearch.getClasses();
			log.info("className Size "+ classesAppList.size());
			for (Iterator<Class<?>> it = classesAppList.iterator(); it.hasNext(); ) {
				Class<?> f = it.next();
				// get all methods of class
				ArrayList<String> methodList = appReflectionSearch.getMethodsOfClass(f);
				// add class name as latest item on method list
				log.info("className Name "+ f.getName());
				methodList.add(f.getName());
				// add this list to all application list
				AppInit.allApplicationList.add(methodList);
			}
			log.info("GET ALL SCRIPTS OF APPLICATION SUCCESSFUL");
		} catch (Exception e) {
			log.error("COULD NOT GET ALL SCRIPTS OF APPLICATION");
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab
					+ e.getMessage());
			throw new Exception();
		}
	}
}
