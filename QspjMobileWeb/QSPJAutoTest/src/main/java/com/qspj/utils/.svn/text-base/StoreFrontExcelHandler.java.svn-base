package com.oneliberty.utils;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;

import jxl.Sheet;
import jxl.Workbook;

import com.oneliberty.appcommon.AppInit;
import com.sai.configure.GlobalVariables;
import com.sai.configure.InputDataExcelConfig;
import com.sai.utils.ExcelHandle;
import com.sai.utils.StackTraceInfo;

public class StoreFrontExcelHandler extends ExcelHandle {
	
	public ArrayList<String> excelReadableGetGlobalVars() throws Exception
    {
        try
        {
            // Open excel file
            ArrayList<String> globalVar = new ArrayList<String>();
            File inputWorkbook = new File(GlobalVariables.excelDataInput);
            Workbook workbook = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = workbook.getSheet(GlobalVariables.INPUTDATA_SHEET_NAME);
            // get global variables on Excel file
            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Liberty URL").getRow()).getContents());
            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("NRIC URL").getRow()).getContents());
            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Agent UserName").getRow()).getContents());
            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Agent Password").getRow()).getContents());
            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Internal Staff UserName").getRow()).getContents());
            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Internal Staff Password").getRow()).getContents());
            return globalVar;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            return null;
        }
    }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public boolean excelReadableRunStep(String stepName, String[] param)
			throws Exception {
		boolean isExec = false;
		try {
			String className = "";
			ArrayList<String> listMethodOfClass = new ArrayList<String>();
			if (AppInit.availCommonScript.contains(stepName)) {
				listMethodOfClass = AppInit.availCommonScript;
				className = AppInit.commonTestscriptClass;
			} 
			else if (AppInit.availMedicalScript.contains(stepName)) {
				listMethodOfClass = AppInit.availMedicalScript;
				className = AppInit.medicalTestscriptClass;
			} 
			else if (AppInit.availMotorScript.contains(stepName)) {
				listMethodOfClass = AppInit.availMotorScript;
				className = AppInit.motorTestscriptClass;
			} 
			else if (AppInit.availPAScript.contains(stepName)) {
				listMethodOfClass = AppInit.availPAScript;
				className = AppInit.paTestscriptClass;
			} 
			else if (AppInit.availPackageScript.contains(stepName)) {
				listMethodOfClass = AppInit.availPackageScript;
				className = AppInit.packageTestscriptClass;
			}
			else if (AppInit.availReportScript.contains(stepName)) {
				listMethodOfClass = AppInit.availReportScript;
				className = AppInit.reportTestscriptClass;
			}
			else if (AppInit.availServicesScript.contains(stepName)) {
				listMethodOfClass = AppInit.availServicesScript;
				className = AppInit.servicesTestscriptClass;
			}
			else if (AppInit.availTravelScript.contains(stepName)) {
				listMethodOfClass = AppInit.availTravelScript;
				className = AppInit.travelTestscriptClass;
			}


			// Init class instance
			Class cls = Class.forName(className);
			Object obj = cls.newInstance();

			// Invoke method
			if (listMethodOfClass.contains(stepName)) {
				Method method = null;
				if (param.length == 0) {
					try {
						method = cls.getDeclaredMethod(stepName);
						method.invoke(obj);
					} catch (Exception e) {
						log.error(StackTraceInfo.getCurrentClassName());
						log.error(StackTraceInfo.getCurrentFileName());
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + "STEP '" + stepName
								+ "' REQUIRE DATA");
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + e.getMessage());
						// set False Result
						utils.setFalseResult("STEP '" + stepName
								+ "' REQUIRE DATA");
					}
				} else {
					try {
						method = cls.getDeclaredMethod(stepName,
								param.getClass());
						method.invoke(obj, (Object) param);
					} catch (Exception e) {
						log.error(StackTraceInfo.getCurrentClassName());
						log.error(StackTraceInfo.getCurrentFileName());
						log.error(StackTraceInfo.getCurrentMethodName()
								+ log.tab + "STEP '" + stepName
								+ "' DO NOT REQUIRE DATA");
						// set False Result
						utils.setFalseResult("STEP '" + stepName
								+ "' DO NOT REQUIRE DATA");
					}
				}
				isExec = true;
			} else {
				log.error(StackTraceInfo.getCurrentClassName());
				log.error(StackTraceInfo.getCurrentFileName());
				log.error(StackTraceInfo.getCurrentMethodName() + log.tab
						+ "STEP '" + stepName + "' DO NOT EXIST IN TESTSCRIPT");
				// set False Result
				utils.setFalseResult("STEP '" + stepName
						+ "' DO NOT EXIST IN TESTSCRIPT");
			}
			return isExec;
		} catch (Exception e) {
			log.error(log.tab + "STEP " + stepName
					+ "' EXECUTE BUT THROWED EXCEPTION");
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab
					+ e.getMessage());
			utils.setFalseResult("STEP " + stepName
					+ "' EXECUTE BUT THROWED EXCEPTION");
			return isExec;
		}
	}

	public void getAllScript() throws Exception {
		try {
			// get all script of Common
			Class cls = Class.forName(AppInit.commonTestscriptClass);
			Object obj = cls.newInstance();
			Method[] tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availCommonScript.add(tempmethod[i].getName());
			}

			// get all script of Medical
			cls = Class.forName(AppInit.medicalTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availMedicalScript.add(tempmethod[i].getName());
			}

			// get all script of Motor
			cls = Class.forName(AppInit.motorTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availMotorScript.add(tempmethod[i].getName());
			}

			// get all script of Package
			cls = Class.forName(AppInit.packageTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availPackageScript.add(tempmethod[i].getName());
			}

			// get all script of PA
			cls = Class.forName(AppInit.paTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availPAScript.add(tempmethod[i].getName());
			}

			// get all script of Report
			cls = Class.forName(AppInit.reportTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availReportScript.add(tempmethod[i].getName());
			}

			// get all script of Services
			cls = Class.forName(AppInit.servicesTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availServicesScript.add(tempmethod[i].getName());
			}

			// get all script of Travel
			cls = Class.forName(AppInit.travelTestscriptClass);
			obj = cls.newInstance();
			tempmethod = cls.getDeclaredMethods();
			for (int i = 0; i < tempmethod.length; i++) {
				AppInit.availTravelScript.add(tempmethod[i].getName());
			}

			log.info("GET ALL SCRIPTS OF APPLICATION SUCCESSFUL");
		} catch (Exception e) {
			log.error("COULD NOT GET ALL SCRIPTS OF APPLICATION");
			log.error(StackTraceInfo.getCurrentClassName());
			log.error(StackTraceInfo.getCurrentFileName());
			log.error(StackTraceInfo.getCurrentMethodName() + log.tab
					+ e.getMessage());
			throw new Exception();
		}
	}

}
