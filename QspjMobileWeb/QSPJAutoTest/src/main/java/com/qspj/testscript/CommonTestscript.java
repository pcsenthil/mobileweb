package com.qspj.testscript;

import com.qspj.appcommon.CommonAppFunc;
import com.sai.configure.Environment;


@SuppressWarnings(
{ "javadoc", "javadoc" })
public class CommonTestscript extends Environment
{
	private final CommonAppFunc commonApp = new CommonAppFunc();

	public void switchToHMCWindow() throws Exception
	{
		commonApp.switchToHMCWindow();
	}

	public void switchToStorefrontWindow() throws Exception
	{
		commonApp.switchToStorefrontWindow();
	}
}
