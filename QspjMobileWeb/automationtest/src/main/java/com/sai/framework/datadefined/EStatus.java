package com.sai.framework.datadefined;

public enum EStatus
{
    PASSED, FAILED, SKIPPED, WARNING, BLOCKED
}
