package com.sai.framework.datadefined;

public enum EFindElementBy
{
    CSS_SELECTOR, XPATH, CLASSNAME, ID
}
