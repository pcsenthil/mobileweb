package com.sai.framework.excelentity;

public class ExcelScenarioSheet
{
    private int _rowBrowserName;
    private int _colBrowserName;
    private int _rowScenarioHeader;
    private int _colScenario;
    private int _colExecuteFlag;
    private int _colTestCaseIDs;

    public ExcelScenarioSheet() {

    }

    public int getRowBrowserName() {
        return this._rowBrowserName;
    }

    public void setRowBrowserName(int rowBrowserName) {
        this._rowBrowserName = rowBrowserName;
    }

    public int getColBrowserName() {
        return this._colBrowserName;
    }

    public void setColBrowserName(int colBrowserName) {
        this._colBrowserName = colBrowserName;
    }

    public int getRowScenarioHeader() {
        return this._rowScenarioHeader;
    }

    public void setRowScenarioHeader(int rowScenarioHeader) {
        this._rowScenarioHeader = rowScenarioHeader;
    }

    public int getColScenario() {
        return this._colScenario;
    }

    public void setColScenario(int colScenario) {
        this._colScenario = colScenario;
    }

    public int getColExecuteFlag() {
        return this._colExecuteFlag;
    }

    public void setColExecuteFlag(int colExecuteFlag) {
        this._colExecuteFlag = colExecuteFlag;
    }

    public int getColTestCaseIDs() {
        return this._colTestCaseIDs;
    }

    public void setColTestCaseIDs(int colTestCaseIDs) {
        this._colTestCaseIDs = colTestCaseIDs;
    }

}
