package com.sai.framework.excelentity;

/**
 * @Name: ExcelResultSheet
 * @author DatNguyen
 * @Description: This class is declare ExcelResultSheet object and attributes
 * @CreatedDate: 04/03/2014
 */
public class ExcelResultSheet
{
    private int _colSceName;
    private int _colSceStatus;
    private int _colTestCaseNumber;
    private int _colRunNumber;
    private int _colTestCaseStatus;
    private int _colTestCaseName;
    private int _colAutomationStepName;
    private int _colTestData;
    private int _colStepNumber;
    private int _colStepDescription;
    private int _colExpectedResults;
    private int _colObtainedResults;
    private int _colAutomationStepStatus;
    private int _colScreenShot;
    private int _colStartTime;
    private int _colEndTime;

    public ExcelResultSheet() {
    }

    public void setColScenarioName(int iColSceName) {
        this._colSceName =  iColSceName;
    }

    public void setColScenarioStatus(int iColSceStatus) {
        this._colSceStatus = iColSceStatus;
    }

    public void setColTestCaseNumber(int iColTCNumber) {
        this._colTestCaseNumber = iColTCNumber;
    }

    public void setColRunNumber(int iColRunNumber) {
        this._colRunNumber = iColRunNumber;
    }

    public void setColTestCaseStatus(int iColTestCaseStatus) {
        this._colTestCaseStatus = iColTestCaseStatus;
    }

    public void setColTestCaseName(int iColTestCaseName) {
        this._colTestCaseName = iColTestCaseName;
    }

    public void setColTestData(int iColTestData) {
        this._colTestData = iColTestData;
    }

    public void setColAutomationStepName(int iColAutoStepName) {
        this._colAutomationStepName = iColAutoStepName;
    }

    public void setColAutomationStepStatus(int iColAutoStepStatus) {
        this._colAutomationStepStatus = iColAutoStepStatus;
    }

    public void setColStepNumber(int iColStepNumber) {
        this._colStepNumber = iColStepNumber;
    }

    public void setColStepDescription(int iColStepDescription) {
        this._colStepDescription = iColStepDescription;
    }

    public void setColExpectedResults(int iColExpectedResults) {
        this._colExpectedResults = iColExpectedResults;
    }

    public void setColObtainedResults(int iColObtainedResults) {
        this._colObtainedResults = iColObtainedResults;
    }

    public void setColScreenShot(int iColScreenShot) {
        this._colScreenShot = iColScreenShot;
    }

    public void setColStartTime(int iColStartTime) {
        this._colStartTime = iColStartTime;
    }

    public void setColEndTime(int iColEndTime) {
        this._colEndTime = iColEndTime;
    }
}
