package com.sai.framework.excelentity;

public class ExcelTestCaseSheet
{
    int _colTestCaseID;
    int _colTestCaseName;
    int _colExecuteFlag;
    int _colExecuteTimes;
    int _colTestData;
    int _colAutomationStepName;
    int _colStepNumber;
    int _colStepDescription;
    int _colExpectedResult;
    int _colTestCaseType;
}
