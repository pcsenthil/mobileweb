package com.sai.framework.reporting;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.sai.configure.GlobalVariables;
import com.sai.framework.excelentity.BrowserEnvTest;
import com.sai.framework.excelentity.Scenario;
import com.sai.framework.excelentity.TestCase;
import com.sai.framework.excelentity.TestStep;

@XmlRootElement
public class ScenarioReport
{
    private final List<BrowserEnvTest> browserEnvTests = new ArrayList<BrowserEnvTest>();
    private final List<Scenario> scenarios = new ArrayList<Scenario>();
    private final List<TestCase> testCases = new ArrayList<TestCase>();
    private final List<TestStep> testSteps = new ArrayList<TestStep>();

    private String _projectName;

    public ScenarioReport() {
        setProjectName(GlobalVariables.PROJECT_NAME);
    }

    public String getProjectName() {
        return _projectName;
    }

    public void setProjectName(String projectName) {
        this._projectName = projectName;
    }

    @XmlElement(name="browserEnvTest")
    public List<BrowserEnvTest> getBrowserEnvTests() {
        return browserEnvTests;
    }

    @XmlElement(name="scenario")
    public List<Scenario> getScenarios() {
        return scenarios;
    }

    @XmlElement(name = "testcase")
    public List<TestCase> getTestCases() {
        return testCases;
    }

    @XmlElement(name="testStep")
    public List<TestStep> getTestSteps() {
        return testSteps;
    }
}

