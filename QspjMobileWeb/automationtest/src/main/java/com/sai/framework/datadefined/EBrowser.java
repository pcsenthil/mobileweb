package com.sai.framework.datadefined;

public enum EBrowser
{
    FIREFOX, IE, CHROME, SAFARI, OPERA
}
