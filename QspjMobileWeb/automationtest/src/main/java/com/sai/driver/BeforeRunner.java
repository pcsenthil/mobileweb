package com.sai.driver;

import java.io.File;

import com.sai.configure.Environment;
import com.sai.configure.GlobalVariables;
import com.sai.utils.ExcelHandle;
import com.sai.utils.StackTraceInfo;

public class BeforeRunner extends Environment
{

    private final ExcelHandle eh;

    public BeforeRunner (ExcelHandle eh){
        this.eh = eh;
    }

    public void beforeRunner() throws Exception
    {
        try {
            // Get framework path
            //String projectPath = new File("").getAbsolutePath();

            String projectPath = "D:\\01_WORKING\\09_LIBERTY\\AutomationCode";
        	

            projectPath = projectPath.replace("\\", "/");
            String frameworkPath = projectPath.substring(0, projectPath.lastIndexOf("/"));
            String projectName = projectPath.substring(projectPath.lastIndexOf("/") + 1);

            // Config FW
            GlobalVariables.FWConfigPath = frameworkPath + "/" + GlobalVariables.CONFIG_FOLDER + "/" + GlobalVariables.CONFIG_FILENAME;
            ConfigurationLoading.getProjectConfig();
            ConfigurationLoading.getExcelFWConfig();

            // Get test run environment
            AutomationFWTestCase.testrun.setProjectName(utils.getXmlNodeValue("project", "PROJECT_NAME"));

            // create a Screenshot folder
            GlobalVariables.screenshootFolder = GlobalVariables.resultFolder + "IMG/";
            new File(GlobalVariables.screenshootFolder).mkdir();

            // copy JavaScript folder to Results folder
            String jsFolder = GlobalVariables.resultFolder + "js/";
            new File(jsFolder).mkdir();
            utils.copyJavaScriptFoldertoResult(projectPath + "/src/com/sai/javascript", jsFolder);

            GlobalVariables.excelDataInput = frameworkPath + "/" + GlobalVariables.DATAINPUT_FOLDER + "/" + GlobalVariables.EXCEL_FILENAME;
            GlobalVariables.firefoxProfile = frameworkPath + "/" + GlobalVariables.JARS_FOLDER + "/FirefoxProfile";
            GlobalVariables.ieDriver = frameworkPath + "/" + GlobalVariables.JARS_FOLDER + "/IEDriverServer.exe";
            GlobalVariables.chromeDriver = frameworkPath + "/" + GlobalVariables.JARS_FOLDER + "/chromedriver.exe";


            // Get all scripts of Application
            eh.getAllScript();

            //Get Platform Details
            AutomationFWTestCase.platformName = excelhandle.excelReadableGetMobilePlatform();
            AutomationFWTestCase.deviceName = excelhandle.excelReadableGetDeviceName();
            AutomationFWTestCase.platformVersion = excelhandle.excelReadableGetPlatformVersion();
            AutomationFWTestCase.appiumAddress = excelhandle.excelReadableGetAppiumAddress();
            AutomationFWTestCase.browserName = excelhandle.excelReadableGetBrowser();
            // print out Cross browser
            log.info("APPIUM TEST FOR FOLLOWING:");
            log.info("PLATFORM : " +AutomationFWTestCase.platformName);
            log.info("DEVICE NAME : " +AutomationFWTestCase.deviceName);
            log.info("DEVICE PLATFORM VERSION : " +AutomationFWTestCase.platformVersion);
            log.info("MOBILE BROWSER : " +AutomationFWTestCase.browserName);
            log.info("APPIUM SERVER URL : " +AutomationFWTestCase.appiumAddress);
            log.info(log.logLine);         
            
        }
        catch (Exception e) {
            log.error("COULD NOT EXECUTE BEFORERUNNER");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
        }


    }

}
