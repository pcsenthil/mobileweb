package com.sai.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.Pattern;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableHyperlink;

import com.sai.configure.ChartConfig;
import com.sai.configure.Environment;
import com.sai.configure.GlobalVariables;
import com.sai.configure.InputDataExcelConfig;
import com.sai.configure.ResultExcelConfig;
import com.sai.configure.ScenarioExcelConfig;
import com.sai.configure.TestCaseExcelConfig;

/**
 * @Name: ExcelHandle
 * @author DatNguyen
 * @Description: This class store all function working with Excel file.
 * @CreatedDate: 05/03/2014
 */
public class ExcelHandle extends Environment
{
    public ExcelHandle() {

    }

    /**
     * @FunctionName: excelReadableGetBrowser
     * @Description: This function will get all browser need for executing test
     * @author DatNguyen
     * @return ArrayList<String> browsers
     * @throws Exception : null
     * @CreatedDate: 05/03/2014
     */
    public final String excelReadableGetBrowser() throws Exception
    {
        try
        {
            // Open excel file
            String browser = "";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

                if (!sheet.getCell(ScenarioExcelConfig.COL_BROWSER_NAME, ScenarioExcelConfig.ROW_BROWSER_NAME).getContents().equals(""))
                {
                    browser=sheet.getCell(ScenarioExcelConfig.COL_BROWSER_NAME, ScenarioExcelConfig.ROW_BROWSER_NAME).getContents();
                }

            if (browser.equals(""))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE BROWSER NAME FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return browser;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }
    
    public final String excelReadableGetMobilePlatform() throws Exception
    {
        try
        {
            // Open excel file
            String platformName ="";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

             if(!sheet.getCell(ScenarioExcelConfig.COL_PLATFORM_NAME, ScenarioExcelConfig.ROW_PLATFORM_NAME).getContents().equals(""))
            {
            	platformName = sheet.getCell(ScenarioExcelConfig.COL_PLATFORM_NAME, ScenarioExcelConfig.ROW_PLATFORM_NAME).getContents();
            }
            
            if (platformName.equals(""))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE PLATFORM NAME FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return platformName;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }
    
    public final String excelReadableGetDeviceName() throws Exception
    {
        try
        {
            // Open excel file
            String deviceName ="";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

             if(!sheet.getCell(ScenarioExcelConfig.COL_DEVICE_NAME, ScenarioExcelConfig.ROW_DEVICE_NAME).getContents().equals(""))
            {
            	 deviceName = sheet.getCell(ScenarioExcelConfig.COL_DEVICE_NAME, ScenarioExcelConfig.ROW_DEVICE_NAME).getContents();
            }
            
            if (deviceName.equals(""))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE DEVICE NAME FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return deviceName;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }
    
    public final String excelReadableGetPlatformVersion() throws Exception
    {
        try
        {
            // Open excel file
            String platformVersion ="";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

             if(!sheet.getCell(ScenarioExcelConfig.COL_PLATFORM_VERSION, ScenarioExcelConfig.ROW_PLATFORM_VERSION).getContents().equals(""))
            {
            	 platformVersion = sheet.getCell(ScenarioExcelConfig.COL_PLATFORM_VERSION, ScenarioExcelConfig.ROW_PLATFORM_VERSION).getContents();
            }
            
            if (platformVersion.equals(""))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE DEVICE PLATFORM VERSION FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return platformVersion;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }
    
    public final String excelReadableGetAppiumAddress() throws Exception
    {
        try
        {
            // Open excel file
            String appiumAddress ="";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

             if(!sheet.getCell(ScenarioExcelConfig.COL_APPIUM_ADDRESS, ScenarioExcelConfig.ROW_APPIUM_ADDRESS).getContents().equals(""))
            {
            	 appiumAddress = sheet.getCell(ScenarioExcelConfig.COL_APPIUM_ADDRESS, ScenarioExcelConfig.ROW_APPIUM_ADDRESS).getContents();
            }
            
            if (appiumAddress.equals(""))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE DEVICE PLATFORM VERSION FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return appiumAddress;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }
    
    public final String excelReadableGetAppPackageName() throws Exception
    {
        try
        {
            // Open excel file
            String packageName ="";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

             if(!sheet.getCell(ScenarioExcelConfig.COL_PACKAGE_NAME, ScenarioExcelConfig.ROW_PACKAGE_NAME).getContents().equals(""))
            {
            	 packageName = sheet.getCell(ScenarioExcelConfig.COL_PACKAGE_NAME, ScenarioExcelConfig.ROW_PACKAGE_NAME).getContents();
            }
            
            if (packageName.equals("") && this.excelReadableGetBrowser().equalsIgnoreCase("AndroidApp"))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE APPLICATION PACKAGE NAME FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return packageName;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }

    public final String excelReadableGetAppAcivityName() throws Exception
    {
        try
        {
            // Open excel file
            String ActivityName ="";
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

             if(!sheet.getCell(ScenarioExcelConfig.COL_ACTIVITY_NAME, ScenarioExcelConfig.ROW_ACTIVITY_NAME).getContents().equals(""))
            {
            	 ActivityName = sheet.getCell(ScenarioExcelConfig.COL_ACTIVITY_NAME, ScenarioExcelConfig.ROW_ACTIVITY_NAME).getContents();
            }
            
            if (ActivityName.equals("") && this.excelReadableGetBrowser().equalsIgnoreCase("AndroidApp"))
            {
                log.info(StackTraceInfo.getCurrentClassName());
                log.info(StackTraceInfo.getCurrentFileName());
                log.info(StackTraceInfo.getCurrentMethodName());
                log.info("PLEASE CONFIGURE APPLICATION ACTIVITY NAME FOR EXECUTING APPIUM TEST");
                throw new Exception();
            }

            return ActivityName;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName());
            log.error("Function 'excelReadableGetBrowser()' throw exception: ");
            log.error(log.tab + e.getMessage());
            throw new Exception();
        }
    }
    /**
     * @FunctionName: excelReadableGetScenario
     * @Description: This function will do. - Open Excel file - Get list Scenario - Convert list
     * scenario to Array String
     * @author DatNguyen
     * @return String[][] list valid scenario
     * @throws Exception : null
     * @CreatedDate: 05/03/2014
     */
    public final String[][] excelReadableGetScenario() throws Exception
    {
        try
        {
            // Open excel file
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);

            // Get valid scenario from Scenario sheet
            ArrayList<String> listSceName = new ArrayList<String>();
            ArrayList<String> listSceTestCase = new ArrayList<String>();

            for (int i = ScenarioExcelConfig.ROW_SCENARIO_HEADER + 1; i < sheet.getRows(); i++)
            {
                String sceName = sheet.getCell(ScenarioExcelConfig.COL_SCENARIO_NAME, i).getContents().trim();
                String sceFlag = sheet.getCell(ScenarioExcelConfig.COL_EXECUTE_FLAG, i).getContents().trim();
                String sceTestCase = sheet.getCell(ScenarioExcelConfig.COL_TESTCASEIDS, i).getContents().trim();
                if (!sceName.equals("") && sceFlag.equals("Yes"))
                {
                    if (!sceTestCase.equals(""))
                    {
                        listSceName.add(sceName);
                        listSceTestCase.add(sceTestCase);
                    }
                    else
                    {
                        log.info(StackTraceInfo.getCurrentClassName());
                        log.info(StackTraceInfo.getCurrentFileName());
                        log.info(StackTraceInfo.getCurrentMethodName());
                        log.info("SCENARIO '" + sceName + "': DO NOT HAVE TESTCASE - THIS SCENARIO WILL BE IGNORE");
                    }
                }
            }

            // Convert list scenario to Array String
            String[][] listScenario = new String[listSceName.size()][2];

            for (int i = 0; i < listSceName.size(); i++)
            {
                listScenario[i][0] = listSceName.get(i);
                listScenario[i][1] = listSceTestCase.get(i);
            }

            return listScenario;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            return null;
        }
    }


    /**
     * @FunctionName: excelReadableGetScenarioDesc
     * @Description: This function will do. - Open Excel file - Get scenario description which are belonged to specific scenario
     * @author DatNguyen
     * @return String: Scenario Description
     * @throws Exception : null
     * @CreatedDate: 02/07/2014
     */
    public final String excelReadableGetScenarioDesc(String scename) throws Exception
    {
        String scenarioDes = "";
        try
        {
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);
            Cell cell = sheet.findCell(scename);
            int iRowScenario = cell.getRow();
            scenarioDes = sheet.getCell(ScenarioExcelConfig.COL_DESCRIPTION, iRowScenario).getContents();

            return scenarioDes;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            return null;
        }
    }

    /**
     * @FunctionName: excelReadableGetTestCase
     * @Description: This function will do. - Open Excel file - Get list test cases which are belonged to specific scenario
     * @author DatNguyen
     * @return Object[][] list valid scenario
     * @throws Exception : null
     * @CreatedDate: 07/03/2014
     */
    public final Object[][] excelReadableGetTestCase(String scename) throws Exception
    {
        ArrayList<String> listAllTestCase = new ArrayList<String>();
        ArrayList<String> listTestCase = new ArrayList<String>();
        Object[][] objTestCase = null;
        try
        {
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.SCENARIO_SHEET_NAME);
            Cell cell = sheet.findCell(scename);
            int iRowScenario = cell.getRow();

            // Get list testcase and convert to arraylist
            String strTestCase = sheet.getCell(ScenarioExcelConfig.COL_TESTCASEIDS, iRowScenario).getContents().replace(" ", "");
            String[] arrTestCase = strTestCase.split(",");
            for (int i = 0; i < arrTestCase.length; i++)
            {
                listAllTestCase.add(arrTestCase[i]);
            }

            // Get list Test case return
            sheet = workbook.getSheet(GlobalVariables.TESTCASE_SHEET_NAME);
            for (int j = 0; j < listAllTestCase.size(); j++)
            {
                Cell cellTestcase = sheet.findCell(listAllTestCase.get(j));
                int testcaserow = cellTestcase.getRow();
                String isRun = sheet.getCell(TestCaseExcelConfig.COL_EXECUTE_FLAG, testcaserow).getContents();
                if (isRun.equals("Yes"))
                {
                    listTestCase.add(listAllTestCase.get(j));
                }
            }

            // Set size for list Test Case
            objTestCase = new Object[listTestCase.size()][4];

            // Convert list testcase to array string
            for (int k = 0; k < listTestCase.size(); k++)
            {
                Cell cellTestcase = sheet.findCell(listTestCase.get(k));
                int testcaserow = cellTestcase.getRow();
                String executionTimes = sheet.getCell(TestCaseExcelConfig.COL_EXECUTE_TIMES, testcaserow).getContents().replace(" ", "").trim();
                if("#".equals(executionTimes.substring(0, 1)))
                    executionTimes = executionTimes.substring(1);

                String[] runTime = executionTimes.split(",#");
                String testcasename = sheet.getCell(TestCaseExcelConfig.COL_TESTCASE_NAME, testcaserow).getContents();

                int times = Integer.parseInt(runTime[0]);
                objTestCase[k][0] = listTestCase.get(k);
                objTestCase[k][1] = times;
                objTestCase[k][3] = testcasename;
                ArrayList<String> dataset = new ArrayList<String>();

                // Get data list
                if (runTime.length == 1)
                {
                    objTestCase[k][2] = 0;
                }
                else
                {
                    for (int i = 1; i < runTime.length; i++)
                    {
                        dataset.add(runTime[i]);
                    }
                    objTestCase[k][2] = dataset;
                }
            }
            return objTestCase;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            return null;
        }
    }

    /**
     * @FunctionName: excelReadableRunStep
     * @Description: This function will do. - Init Test Script instance - Check is method exist in test script or not - Invoke method
     * @author DatNguyen
     * @Param clsName- string class name; stepName- string step name; param- String[] test data
     * @return boolean executed status
     * @throws Exception : null
     * @CreatedDate: 07/03/2014
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public boolean excelReadableRunStep(String clsName, String stepName, String[] param) throws Exception
    {
        boolean isExec = false;
        try
        {
            // Init class instance
            Class cls = Class.forName(clsName);
            Object obj = cls.newInstance();
            Method[] tempmethod = cls.getDeclaredMethods();

            ArrayList<String> listMethod = new ArrayList<String>();
            for (int i = 0; i < tempmethod.length; i++)
            {
                listMethod.add(tempmethod[i].getName());
            }

            // Invoke method
            if (listMethod.contains(stepName))
            {
                Method method = null;
                if (param.length == 0)
                {
                    try
                    {
                        method = cls.getDeclaredMethod(stepName);
                        method.invoke(obj);
                    }
                    catch (Exception e)
                    {
                        log.error(StackTraceInfo.getCurrentClassName());
                        log.error(StackTraceInfo.getCurrentFileName());
                        log.error(StackTraceInfo.getCurrentMethodName() + log.tab + "STEP '" + stepName + "' REQUIRE DATA");
                    }
                }
                else
                {
                    try
                    {
                        method = cls.getDeclaredMethod(stepName, param.getClass());
                        method.invoke(obj, (Object) param);
                    }
                    catch (Exception e)
                    {
                        log.error(StackTraceInfo.getCurrentClassName());
                        log.error(StackTraceInfo.getCurrentFileName());
                        log.error(StackTraceInfo.getCurrentMethodName() + log.tab + "STEP '" + stepName + "' DO NOT REQUIRE DATA");
                    }
                }
                isExec = true;
            }
            else
            {
                log.error(StackTraceInfo.getCurrentClassName());
                log.error(StackTraceInfo.getCurrentFileName());
                log.error(StackTraceInfo.getCurrentMethodName() + log.tab + "STEP '" + stepName + "' DO NOT EXIST IN TESTSCRIPT");
            }
            return isExec;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            log.error("COULD NOT EXECUTE STEP: " + stepName);
            return isExec;
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public boolean excelReadableRunStep(String stepName, String[] param) throws Exception
    {
        //        boolean isExec = false;
        //        try
        //        {
        //            String className = "";
        //            ArrayList<String> listMethodOfClass = new ArrayList<String>();
        //            if (AppVariables.availStorefrontScript.contains(stepName)) {
        //                listMethodOfClass = AppVariables.availStorefrontScript;
        //                className = AppVariables.storefrontScriptClass;
        //            }
        //            else if (AppVariables.availHMCScript.contains(stepName)) {
        //                listMethodOfClass = AppVariables.availHMCScript;
        //                className = AppVariables.hmcScriptClass;
        //            }
        //            else if (AppVariables.availCMSCockpitScript.contains(stepName)) {
        //                listMethodOfClass = AppVariables.availCMSCockpitScript;
        //                className = AppVariables.cmsCockpitScriptClass;
        //            }
        //            else if (AppVariables.availCSCockpitScript.contains(stepName)) {
        //                listMethodOfClass = AppVariables.availCSCockpitScript;
        //                className = AppVariables.csCockpitScriptClass;
        //            }
        //            else if (AppVariables.availProductCockpitScript.contains(stepName)) {
        //                listMethodOfClass = AppVariables.availProductCockpitScript;
        //                className = AppVariables.productCockpitScriptClass;
        //            }
        //
        //
        //            // Init class instance
        //            Class cls = Class.forName(className);
        //            Object obj = cls.newInstance();
        //
        //            // Invoke method
        //            if (listMethodOfClass.contains(stepName))
        //            {
        //                Method method = null;
        //                if (param.length == 0)
        //                {
        //                    try
        //                    {
        //                        method = cls.getDeclaredMethod(stepName);
        //                        method.invoke(obj);
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        log.error(StackTraceInfo.getCurrentClassName());
        //                        log.error(StackTraceInfo.getCurrentFileName());
        //                        log.error(StackTraceInfo.getCurrentMethodName() + log.tab + "STEP '" + stepName + "' REQUIRE DATA");
        //                        log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
        //                        // set False Result
        //                        utils.setFalseResult("STEP '" + stepName + "' REQUIRE DATA");
        //                    }
        //                }
        //                else
        //                {
        //                    try
        //                    {
        //                        method = cls.getDeclaredMethod(stepName, param.getClass());
        //                        method.invoke(obj, (Object) param);
        //                    }
        //                    catch (Exception e)
        //                    {
        //                        log.error(StackTraceInfo.getCurrentClassName());
        //                        log.error(StackTraceInfo.getCurrentFileName());
        //                        log.error(StackTraceInfo.getCurrentMethodName() + log.tab + "STEP '" + stepName + "' DO NOT REQUIRE DATA");
        //                        // set False Result
        //                        utils.setFalseResult("STEP '" + stepName + "' DO NOT REQUIRE DATA");
        //                    }
        //                }
        //                isExec = true;
        //            }
        //            else
        //            {
        //                log.error(StackTraceInfo.getCurrentClassName());
        //                log.error(StackTraceInfo.getCurrentFileName());
        //                log.error(StackTraceInfo.getCurrentMethodName() + log.tab + "STEP '" + stepName + "' DO NOT EXIST IN TESTSCRIPT");
        //                // set False Result
        //                utils.setFalseResult("STEP '" + stepName + "' DO NOT EXIST IN TESTSCRIPT");
        //            }
        //            return isExec;
        //        }
        //        catch (Exception e)
        //        {
        //            log.error(log.tab + "STEP " + stepName + "' EXECUTE BUT THROWED EXCEPTION");
        //            log.error(StackTraceInfo.getCurrentClassName());
        //            log.error(StackTraceInfo.getCurrentFileName());
        //            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
        //            utils.setFalseResult("STEP " + stepName + "' EXECUTE BUT THROWED EXCEPTION");
        //            return isExec;
        //        }
        return true;
    }


    public void getAllScript() throws Exception
    {
        //        try {
        //            // get all script of Storefront
        //            Class cls = Class.forName(AppVariables.storefrontScriptClass);
        //            Object obj = cls.newInstance();
        //            Method[] tempmethod = cls.getDeclaredMethods();
        //            for (int i = 0; i < tempmethod.length; i++)
        //            {
        //                AppVariables.availStorefrontScript.add(tempmethod[i].getName());
        //            }
        //
        //            // get all script of HMC
        //            cls = Class.forName(AppVariables.hmcScriptClass);
        //            obj = cls.newInstance();
        //            tempmethod = cls.getDeclaredMethods();
        //            for (int i = 0; i < tempmethod.length; i++)
        //            {
        //                AppVariables.availHMCScript.add(tempmethod[i].getName());
        //            }
        //
        //            // get all script of CMSCockpit
        //            cls = Class.forName(AppVariables.cmsCockpitScriptClass);
        //            obj = cls.newInstance();
        //            tempmethod = cls.getDeclaredMethods();
        //            for (int i = 0; i < tempmethod.length; i++)
        //            {
        //                AppVariables.availCMSCockpitScript.add(tempmethod[i].getName());
        //            }
        //
        //            // get all script of CSCockpit
        //            cls = Class.forName(AppVariables.csCockpitScriptClass);
        //            obj = cls.newInstance();
        //            tempmethod = cls.getDeclaredMethods();
        //            for (int i = 0; i < tempmethod.length; i++)
        //            {
        //                AppVariables.availCSCockpitScript.add(tempmethod[i].getName());
        //            }
        //
        //            // get all script of ProductCockpit
        //            cls = Class.forName(AppVariables.productCockpitScriptClass);
        //            obj = cls.newInstance();
        //            tempmethod = cls.getDeclaredMethods();
        //            for (int i = 0; i < tempmethod.length; i++)
        //            {
        //                AppVariables.availProductCockpitScript.add(tempmethod[i].getName());
        //            }
        //            log.info("GET ALL SCRIPTS OF APPLICATION SUCCESSFULLY");
        //        }
        //        catch (Exception e) {
        //            log.error("COULD NOT GET ALL SCRIPTS OF APPLICATION");
        //            log.error(StackTraceInfo.getCurrentClassName());
        //            log.error(StackTraceInfo.getCurrentFileName());
        //            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
        //            throw new Exception();
        //        }
    }

    /**
     * @FunctionName: excelReadableGetGlobalVars
     * @Description:
     * @author DatNguyen
     * @return array global Vars
     * @throws Exception : null
     * @CreatedDate: 10/03/2014
     */
    public ArrayList<String> excelReadableGetGlobalVars() throws Exception
    {
//        try
//        {
//            // Open excel file
//            ArrayList<String> globalVar = new ArrayList<String>();
//            File inputWorkbook = new File(GlobalVariables.excelDataInput);
//            Workbook workbook = Workbook.getWorkbook(inputWorkbook);
//            Sheet sheet = workbook.getSheet(GlobalVariables.INPUTDATA_SHEET_NAME);
//
//            // get URLs
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Storefront URL").getRow()).getContents());
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("HMC URL").getRow()).getContents());
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("CMSCockpit URL").getRow()).getContents());
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("CSCockpit URL").getRow()).getContents());
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("ProductCockpit URL").getRow()).getContents());
//
//            // get Admin username and password
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Admin UserName").getRow()).getContents());
//            globalVar.add(sheet.getCell(InputDataExcelConfig.COL_VARIABLE_VALUE, sheet.findCell("Admin Password").getRow()).getContents());
//            return globalVar;
//        }
//        catch (Exception e)
//        {
//            log.error(StackTraceInfo.getCurrentClassName());
//            log.error(StackTraceInfo.getCurrentFileName());
//            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
//            return null;
//        }
    	return null;
    }

    /**
     * @FunctionName: excelReadableGetData
     * @Description:
     * @author DatNguyen
     * @return String[] data based on TestCase, Step and DataSet Number(Value1 or Value2...)
     * @throws Exception : null
     * @CreatedDate: 11/03/2014
     */
    public final String[] excelReadableGetData(final String testcase, final String step, final int dataset)
            throws Exception
            {
        String[] retdata = new String[] {};
        try
        {
            // Open excel file
            Workbook workbook = Workbook.getWorkbook(new File(GlobalVariables.excelDataInput));
            Sheet sheet = workbook.getSheet(GlobalVariables.INPUTDATA_SHEET_NAME);

            // Find Range of TestCase data
            ArrayList<String> data = new ArrayList<String>();

            Cell cellTestCase = sheet.findCell(testcase);
            int iStartRowTestCase = cellTestCase.getRow();
            int iEndRowTestCase = 0;
            int i = 0;
            try
            {
                // find end row of test case
                for (i = iStartRowTestCase; i <= sheet.getRows(); i++)
                {
                    if (!sheet.getCell(InputDataExcelConfig.COL_FUNCTION_NAME, i + 1).getContents().equals("")
                            && !sheet.getCell(InputDataExcelConfig.COL_TESTDATA_VALUE1, i + 1).getContents().equals(""))
                    {
                        iEndRowTestCase = i;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                iEndRowTestCase = i;
            }

            // ***********************
            // BIG MODIFY
            // NEED TO BE TEST CAREFULLY
            Cell cellStep;
            if (GlobalVariables.iVar1 == 0 || GlobalVariables.iVar1 <= iStartRowTestCase
                    || GlobalVariables.iVar1 >= iEndRowTestCase)
            {
                cellStep = sheet.findCell(step, 0, iStartRowTestCase, 0, iEndRowTestCase, false);
            }
            else
            {
                cellStep = sheet.findCell(step, 0, GlobalVariables.iVar1, 0, iEndRowTestCase, false);
                // Edited 16/10/2014
                if(cellStep == null)
                    cellStep = sheet.findCell(step, 0, iStartRowTestCase, 0, iEndRowTestCase, false);
                //////////////
            }

            // ***********************

            // Find Range of Step Data
            // Cell cellStep = sheet.findCell(step, 0, GlobalVariables.iVar1, 0, iEndRowTestCase,
            // false);
            int iStartRowStep = cellStep.getRow();
            int iEndRowStep = 0;
            for (i = iStartRowStep; i < iEndRowTestCase; i++)
            {
                if (!sheet.getCell(0, i + 1).getContents().equals(""))
                {
                    iEndRowStep = i;
                    break;
                }
                if (i == iEndRowTestCase -1)
                {
                    iEndRowStep = iEndRowTestCase;
                }
            }
            GlobalVariables.iVar1 = iEndRowStep;

            // *************************

            // Get data for Step Name
            for (i = iStartRowStep + 1; i <= iEndRowStep; i++)
            {
                int value = dataset;
                data.add(sheet.getCell(value + 1, i).getContents());
            }

            // Move data from array list to String
            retdata = new String[data.size()];
            for (i = 0; i < data.size(); i++)
            {
                retdata[i] = data.get(i);
            }
            // write to log
            log.info("TEST DATA OF STEP '" + step + "':");
            for (int z = 0; z < data.size(); z++)
            {
                log.info(data.get(z).toString());
            }

            return retdata;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            return retdata;
        }
            }

    /**
     * @FunctionName: excelReadableGetStepName
     * @Description:
     * @author DatNguyen
     * @return Object[][] of step based on test case input
     * @throws Exception : null
     * @CreatedDate: 11/03/2014
     */
    public final Object[][] excelReadableGetStepName(final String testcase)
    {
        Object[][] listStep = null;
        try
        {
            File inputWorkbook = new File(GlobalVariables.excelDataInput);
            Workbook workbook = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = workbook.getSheet(GlobalVariables.TESTCASE_SHEET_NAME);
            Cell cell = sheet.findCell(testcase);
            int iStartRowTestcase = cell.getRow();
            int iEndRowTestCase = sheet.getRows();
            int i = 0;

            // Find step range
            try
            {
                for (i = iStartRowTestcase;; i++)
                {
                    if (!sheet.getCell(TestCaseExcelConfig.COL_TESTCASEID, i + 1).getContents().equals(""))
                    {
                        iEndRowTestCase = i;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                iEndRowTestCase = i;
            }

            // Get list step of testcase
            listStep = new Object[iEndRowTestCase - iStartRowTestcase + 1][4];
            int j = 0;
            for (i = iStartRowTestcase; i <= iEndRowTestCase; i++)
            {
                //sheet.getCell(9, i).getContents();
                listStep[j][0] = sheet.getCell(TestCaseExcelConfig.COL_AUTOMATION_STEPNAME, i).getContents();
                listStep[j][1] = sheet.getCell(TestCaseExcelConfig.COL_STEP_NUMBER, i).getContents();
                listStep[j][2] = sheet.getCell(TestCaseExcelConfig.COL_STEP_DESCRIPTION, i).getContents();
                listStep[j][3] = sheet.getCell(TestCaseExcelConfig.COL_EXPECTED_RESULT, i).getContents();
                j++;
            }
            return listStep;
        }
        catch (Exception e)
        {
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            return listStep;
        }
    }

    /**
     * @FunctionName: excelWritableResult
     * @Description: This function will write result into excel file
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableResult(final int col, final int row, final String content) throws Exception
    {
        Label label;
        try
        {
            WritableFont font = new WritableFont(WritableFont.ARIAL, 8);
            WritableCellFormat format = new WritableCellFormat(font);
            label = new Label(col, row, content, format);
            GlobalVariables.sheetWritable.addCell(label);
        }
        catch (Exception e)
        {
            log.error("COULD NOT WRITE TO EXCEL FILE");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelWritableSeperate
     * @Description: This function will write gray line to separate the result of each test case
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableSeperate(ArrayList<Integer> indexList) throws Exception
    {
        try
        {
            for (int i = 0; i < indexList.size(); i++) {
                for (int j = 0; j <= ResultExcelConfig.COL_END_TIME; j++)
                {
                    WritableCellFormat cellFormat = new WritableCellFormat();
                    cellFormat.setBackground(Colour.GRAY_25, Pattern.SOLID);
                    Label label = new Label(j, indexList.get(i), "", cellFormat);
                    GlobalVariables.sheetWritable.addCell(label);
                }
            }
        }
        catch (Exception e)
        {
            log.error("COULD NOT INSERT SPACE BETWEEN TWO TESTCASE");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelWritableFillStatus
     * @Description: This function will fill status of test run into excel file
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableFillStatus() throws Exception
    {
        try
        {
            Label label;
            for (int i = 0; i <= GlobalVariables.sheetWritable.getColumns(); i++)
            {
                for (int j = 1; j <= GlobalVariables.sheetWritable.getRows(); j++)
                {
                    WritableCell cell = GlobalVariables.sheetWritable.getWritableCell(i, j);
                    if (cell.getContents().equals("PASS"))
                    {
                        if (cell.getType() == CellType.LABEL)
                        {
                            WritableFont font = new WritableFont(
                                WritableFont.ARIAL, 8, WritableFont.BOLD);
                            font.setColour(Colour.GREEN);
                            WritableCellFormat format = new WritableCellFormat(font);
                            label = new Label(i, j, "PASS", format);
                            GlobalVariables.sheetWritable.addCell(label);
                        }
                    }
                    else if (cell.getContents().equals("FAIL"))
                    {
                        if (cell.getType() == CellType.LABEL)
                        {
                            WritableFont font = new WritableFont(
                                WritableFont.ARIAL, 8, WritableFont.BOLD);
                            font.setColour(Colour.RED);
                            WritableCellFormat format = new WritableCellFormat(font);
                            label = new Label(i, j, "FAIL", format);
                            GlobalVariables.sheetWritable.addCell(label);
                        }
                    }
                    else if (cell.getContents().equals("SKIP"))
                    {
                        if (cell.getType() == CellType.LABEL)
                        {
                            WritableFont font = new WritableFont(
                                WritableFont.ARIAL, 8, WritableFont.BOLD);
                            font.setColour(Colour.VIOLET);
                            WritableCellFormat format = new WritableCellFormat(font);
                            label = new Label(i, j, "SKIP", format);
                            GlobalVariables.sheetWritable.addCell(label);
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("COULD NOT FILL COLOR FOR STATUS");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelWritableDrawChart
     * @Description: This function will write the chart result on excel file
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableDrawChart() throws Exception
    {
        try
        {
            double pass = 0;
            double fail = 0;
            double skip = 0;
            Number num;

            // draw chart for TestCase Status
            for (int i = 0; i <= GlobalVariables.sheetWritable.getRows(); i++)
            {
                WritableCell cell = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_TESTCASE_STATUS, i);
                if (cell.getContents().equals("PASS"))
                {
                    pass = pass + 1;
                }
                if (cell.getContents().equals("FAIL"))
                {
                    fail = fail + 1;
                }
                if (cell.getContents().equals("SKIP"))
                {
                    skip = skip + 1;
                }
            }
            num = new Number(ChartConfig.COL_TESTCASE_PASSED, ChartConfig.ROW_TESTCASE_PASSED, pass);
            GlobalVariables.sheetWritable.addCell(num);
            num = new Number(ChartConfig.COL_TESTCASE_FAILED, ChartConfig.ROW_TESTCASE_FAILED, fail);
            GlobalVariables.sheetWritable.addCell(num);
            num = new Number(ChartConfig.COL_TESTCASE_SKIPED, ChartConfig.ROW_TESTCASE_SKIPED, skip);
            GlobalVariables.sheetWritable.addCell(num);

            // draw chart for Step Status
            pass = 0;
            fail = 0;
            skip = 0;
            for (int i = 0; i <= GlobalVariables.sheetWritable.getRows(); i++)
            {
                WritableCell cell = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_AUTOMATION_STEP_STATUS, i);
                if (cell.getContents().equals("PASS"))
                {
                    pass = pass + 1;
                }
                if (cell.getContents().equals("FAIL"))
                {
                    fail = fail + 1;
                }
                if (cell.getContents().equals("SKIP"))
                {
                    skip = skip + 1;
                }
            }
            num = new Number(ChartConfig.COL_STEP_PASSED, ChartConfig.ROW_STEP_PASSED, pass);
            GlobalVariables.sheetWritable.addCell(num);
            num = new Number(ChartConfig.COL_STEP_FAILED, ChartConfig.ROW_STEP_FAILED, fail);
            GlobalVariables.sheetWritable.addCell(num);
            num = new Number(ChartConfig.COL_STEP_SKIPED, ChartConfig.ROW_STEP_SKIPED, skip);
            GlobalVariables.sheetWritable.addCell(num);

            log.info("UPDATED RESULT ON CHART");

        }
        catch (Exception e)
        {
            log.error("COULD NOT DRAW RESUT CHART");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelReadableGetGlobalVars
     * @Description:
     * @author DatNguyen
     * @return array global Vars
     * @throws Exception : null
     * @CreatedDate: 11/03/2014
     */
    public final String excelCopyResult(final String platformName) throws Exception
    {
        try
        {
            String preoutputFile = GlobalVariables.EXCEL_FILENAME.substring(
                GlobalVariables.EXCEL_FILENAME.indexOf("_") + 1,
                GlobalVariables.EXCEL_FILENAME.length() - 4) + "_" + platformName + "_" + utils.datetimeStamp() + ".xls";

            String outputFile = GlobalVariables.resultFolder + preoutputFile.replaceAll(":", "-").replaceAll(" ", "_");

            File originalFile = new File(GlobalVariables.excelDataInput);
            File destinationFile = new File(outputFile);
            InputStream inStream = new FileInputStream(originalFile);
            OutputStream outStream = new FileOutputStream(destinationFile);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inStream.read(buffer)) > 0)
            {
                outStream.write(buffer, 0, length);
            }

            inStream.close();
            outStream.close();
            return outputFile;
        }
        catch (Exception e)
        {
            log.error("COULD NOT CREATE EXECUTION EXCEL FILE");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelReadableGetGlobalVars
     * @Description:
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelCopySendMail() throws Exception
    {
        try
        {
            String outputFile = GlobalVariables.excelResultFile.substring(
                0,
                GlobalVariables.excelResultFile.indexOf("_"));
            outputFile = outputFile + "_EMAILRESULT.xls";
            File originalFile = new File(GlobalVariables.excelResultFile);
            File destinationFile = new File(outputFile);
            InputStream inStream = new FileInputStream(originalFile);
            OutputStream outStream = new FileOutputStream(destinationFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inStream.read(buffer)) > 0)
            {
                outStream.write(buffer, 0, length);
            }
            inStream.close();
            outStream.close();
        }
        catch (Exception e)
        {
            log.error("COULD NOT COPY RESULT FILE FOR SEND MAIL");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelReadableGetGlobalVars
     * @Description:
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableAddScreenShoot(final int col, final int row, final String link, final String desc)
            throws Exception
            {
        try
        {
            File image = new File(link);
            WritableHyperlink hlk = new WritableHyperlink(col, row, image, desc);
            GlobalVariables.sheetWritable.addHyperlink(hlk);
        }
        catch (Exception e)
        {
            log.error("COULD NOT ADD SCREEN SHOT FOR FAIL TESTCASE");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
            }

    /**
     * @FunctionName: excelReadableGetGlobalVars
     * @Description:
     * @author DatNguyen
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableOpen() throws Exception
    {
        try {
            InputStream ins = new FileInputStream(GlobalVariables.excelResultFile);
            Workbook workbook = Workbook.getWorkbook(ins);
            File outputFile = new File(GlobalVariables.excelResultFile);
            GlobalVariables.workbookWritable = Workbook.createWorkbook(outputFile, workbook);
            GlobalVariables.sheetWritable = GlobalVariables.workbookWritable.getSheet(GlobalVariables.RESULTS_SHEET_NAME);
        }
        catch(Exception e)
        {
            log.error("COULD NOT OPEN RESULT EXCEL FILE TO WRITE");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }

    /**
     * @FunctionName: excelReadableGetGlobalVars
     * @Description:
     * @author DatNguyen
     * @return array global Vars
     * @throws Exception : null
     * @CreatedDate: 11/03/2014
     */
    public final void excelWritableClose() throws Exception
    {
        try
        {
            GlobalVariables.workbookWritable.write();
            GlobalVariables.workbookWritable.close();

            log.info(MessageFormat.format("WRITE TO RESULT EXCEL FILE \"{0}\" IS DONE", GlobalVariables.excelResultFile));
        }
        catch (Exception e) {
            log.error("COULD NOT CLOSE RESULT EXCEL FILE");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }

    }


    public final void excelWritableSetBorder() throws Exception
    {
        try
        {
            Label label;
            int maxRow = GlobalVariables.sheetWritable.getRows();
            for (int i = 0; i <= ResultExcelConfig.COL_END_TIME; i++)
            {
                for (int j = 1; j <= maxRow; j++)
                {
                    WritableCell cell = GlobalVariables.sheetWritable.getWritableCell(i, j);
                    WritableCellFormat format = new WritableCellFormat();
                    format.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN);
                    format.setAlignment(Alignment.LEFT);

                    if("Link".equals(cell.getContents().trim())) {
                        WritableFont font = new WritableFont(
                            WritableFont.ARIAL, 8, WritableFont.BOLD);
                        font.setColour(Colour.DARK_BLUE);
                        font.setItalic(true);
                        format.setFont(font);
                        label = new Label(i, j, cell.getContents(), format);
                    }
                    else {
                        label = new Label(i, j, cell.getContents(), format);
                    }
                    GlobalVariables.sheetWritable.addCell(label);
                }
            }
            log.info("SET BORDER ON RESULT SHEET");
        }
        catch (Exception e)
        {
            log.error("COULD NOT SET BORDER ON RESULT SHEET");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }


    public final void excelWritableScenarioGrouping() throws Exception
    {
        try
        {
            int startRowScenario = 2;
            int maxRow = GlobalVariables.sheetWritable.getRows();
            for (int i = 2; i < maxRow; i++) {
                WritableCell cell = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_SCENARIO_NAME, i);
                if(i < maxRow - 1) {
                    if (!"".equals(cell.getContents().trim())) {
                        GlobalVariables.sheetWritable.setRowGroup(startRowScenario, i-1, true);
                        startRowScenario = i + 1;
                    }
                }
                else {
                    GlobalVariables.sheetWritable.setRowGroup(startRowScenario, i-1, true);
                }
            }
            log.info("GROUPED SCENARIOS ON RESULT SHEET");
        }

        catch (Exception e)
        {
            log.error("COULD NOT GROUP SCENARIOS ON RESULT SHEET");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }


    public final void excelWritableTestCaseGrouping1() throws Exception
    {
        try
        {
            int startRowTestCase = 3;
            int maxRow = GlobalVariables.sheetWritable.getRows();
            for (int i = 2; i < maxRow; i++) {
                WritableCell cell = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_TESTCASE_NUMBER, i);
                if(i < maxRow - 1) {
                    if (!"".equals(cell.getContents().trim())) {
                        GlobalVariables.sheetWritable.setRowGroup(startRowTestCase, i-2, true);
                        startRowTestCase = i + 1;
                    }
                }
                else {
                    GlobalVariables.sheetWritable.setRowGroup(startRowTestCase, i-2, true);
                }
            }
            log.info("GROUPED TESTCASES ON RESULT SHEET");
        }

        catch (Exception e)
        {
            log.error("COULD NOT GROUP TESTCASES ON RESULT SHEET");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }


    public final void excelWritableTestCaseGrouping(ArrayList<String> testcaseList) throws Exception
    {
        try
        {
            int startRow = 0;
            int endRow = 0;
            int j = 2;
            int orderNo = 0;
            int maxRow = GlobalVariables.sheetWritable.getRows();
            while(j< maxRow && orderNo < testcaseList.size()) {

                WritableCell cellTestCase = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_TESTCASE_NUMBER, j);

                if(testcaseList.get(orderNo).toString().equals(cellTestCase.getContents().trim())) {
                    startRow = j+1;
                    for (int k= j + 1; k< maxRow; k++) {
                        cellTestCase = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_TESTCASE_NUMBER, k);
                        WritableCell cellScenario = GlobalVariables.sheetWritable.getWritableCell(ResultExcelConfig.COL_SCENARIO_NAME, k-1);
                        if(!"".equals(cellTestCase.getContents()) && "".equals(cellScenario.getContents())) {
                            endRow = k-2;
                            j= k + 1;
                            break;
                        }
                        if(!"".equals(cellTestCase.getContents()) && !"".equals(cellScenario.getContents())) {
                            endRow = k-3;
                            j= k + 1;
                            break;
                        }
                    }
                    GlobalVariables.sheetWritable.setRowGroup(startRow, endRow, true);
                    orderNo++;
                }
                else {
                    j++;
                }

            }
            log.info("GROUPED TESTCASES ON RESULT SHEET");
        }

        catch (Exception e)
        {
            log.error("COULD NOT GROUP TESTCASES ON RESULT SHEET");
            log.error(StackTraceInfo.getCurrentClassName());
            log.error(StackTraceInfo.getCurrentFileName());
            log.error(StackTraceInfo.getCurrentMethodName() + log.tab + e.getMessage());
            throw new Exception();
        }
    }
}
