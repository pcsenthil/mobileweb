package com.sai.utils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.sai.configure.GlobalVariables;

/**
 * @Name: Log
 * @author DatNguyen
 * @Description: This class implement log4j
 * @CreatedDate: 04/03/2014
 */
public class Log
{
    private final Logger log;
    public final String logLine = "======================================================";
    public final String tab = " >>> ";

    @SuppressWarnings("rawtypes")
    public Log(Class className)
    {
        log = Logger.getLogger(className);
        //configure();
    }

    public void configure()
    {
//        String projectPath = new File("").getAbsolutePath();
//        projectPath = projectPath.replace("\\", "/");
//        String frameworkPath = projectPath.substring(0, projectPath.lastIndexOf("/"));
//
//        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss dd-MMM-yyyy");
//        // get current date time with Date()
//        Date date = new Date();
//
//        // Configuration other file
//        String preoutputFile = GlobalVariables.EXCEL_FILENAME.substring(
//            GlobalVariables.EXCEL_FILENAME.indexOf("_") + 1,
//            GlobalVariables.EXCEL_FILENAME.length() - 4) + "_" + dateFormat.format(date);
//
//        GlobalVariables.resultFolder = frameworkPath + "/" + GlobalVariables.RESULT_FOLDER + "/"
//                + preoutputFile.replaceAll(":", "-").replaceAll(" ", "_") + "/";
//        new File(GlobalVariables.resultFolder).mkdir();

        String randomFile = GlobalVariables.resultFolder
                + "log.html";
        System.setProperty("randomFile", randomFile);

        PropertyConfigurator.configure("log4j.properties");
    }

    /**
     * Log Info
     * 
     * @param logMessage
     */
    public void info(String logMessage)
    {
        log.info(logMessage);
    }

    /**
     * Log Debug
     * 
     * @param logMessage
     */
    public void debug(String logMessage)
    {
        log.debug(logMessage);
    }

    /**
     * Log Warning
     * 
     * @param logMessage
     */
    public void warning(String logMessage)
    {
        log.warn(logMessage);
    }

    /**
     * Log error
     * 
     * @param logMessage
     */
    public void error(String logMessage)
    {
        log.error(logMessage);
    }

    /**
     * Log fatal
     * 
     * @param logMessage
     */
    public void fatal(String logMessage)
    {
        log.fatal(logMessage);
    }
}
