package com.sai.configure;

public class ScenarioExcelConfig
{
    public static String XMLCategory = "scenarioSheetConfig";

    public static int ROW_BROWSER_NAME;
    public static int COL_BROWSER_NAME;
    public static int ROW_PLATFORM_NAME;
    public static int COL_PLATFORM_NAME;
    public static int ROW_SCENARIO_HEADER;
    public static int COL_SCENARIO_NAME;
    public static int COL_EXECUTE_FLAG;
    public static int COL_TESTCASEIDS;
    public static int COL_DESCRIPTION;
    public static int ROW_DEVICE_NAME;
    public static int COL_DEVICE_NAME;
    public static int ROW_PLATFORM_VERSION;
    public static int COL_PLATFORM_VERSION;
    public static int ROW_APPIUM_ADDRESS;
    public static int COL_APPIUM_ADDRESS;
    public static int ROW_PACKAGE_NAME;
    public static int COL_PACKAGE_NAME;
    public static int ROW_ACTIVITY_NAME;
    public static int COL_ACTIVITY_NAME;

}
