package com.sai.configure;

public class TestCaseExcelConfig
{
    public static String XMLCategory = "testcaseSheetConfig";

    public static int ROW_TESTCASE_HEADER;
    public static int COL_TESTCASEID;
    public static int COL_TESTCASE_NAME;
    public static int COL_EXECUTE_FLAG;
    public static int COL_EXECUTE_TIMES;
    public static int COL_TESTDATA;
    public static int COL_AUTOMATION_STEPNAME;
    public static int COL_STEP_NUMBER;
    public static int COL_STEP_DESCRIPTION;
    public static int COL_EXPECTED_RESULT;
    public static int COL_TESTCASE_TYPE;
}
