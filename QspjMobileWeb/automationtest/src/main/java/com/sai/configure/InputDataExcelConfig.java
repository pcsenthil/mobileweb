package com.sai.configure;

public class InputDataExcelConfig
{
    public static String XMLCategory = "inputDataSheetConfig";

    public static int ROW_INPUTDATA_HEADER;
    public static int COL_VARIABLE_NAME;
    public static int COL_VARIABLE_VALUE;
    public static int COL_FUNCTION_NAME;
    public static int COL_TESTDATA_NAME;
    public static int COL_TESTDATA_VALUE1;
}
