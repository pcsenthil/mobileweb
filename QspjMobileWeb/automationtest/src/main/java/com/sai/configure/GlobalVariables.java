package com.sai.configure;

import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * @Name: GlobalVariables
 * @author DatNguyen
 * @Description: This class store all system config and variables .
 * @CreatedDate: 04/03/2014
 */
public final class GlobalVariables
{

    private GlobalVariables()
    {

    }
    public enum LocatorType {
		ID,
		CLASS,
		CSS,
		XPATH
	}
    
    // variables of project configuration
    public static String PROJECT_NAME           = "";
    public static String BUILD_VERSION          = "";
    public static String BUILD_DATE             = "";

    // variables of folder configuration
    public static String DATAINPUT_FOLDER       = "DataInput";
    public static String RESULT_FOLDER          = "Results";
    public static String JARS_FOLDER            = "Jars";
    public static String CONFIG_FOLDER          = "Config";
    public static String CONFIG_FILENAME        = "FWConfig.xml";

    // constant of excel sheet
    public static String EXCEL_FILENAME         = "AutomationExcelFW.xls";
    public static String SCENARIO_SHEET_NAME    = "Scenario";
    public static String TESTCASE_SHEET_NAME    = "Test Cases";
    public static String INPUTDATA_SHEET_NAME   = "InputData";
    public static String RESULTS_SHEET_NAME     = "Result";

    // constant of excel content
    public static int SCENARIO_SHEET_NAME_FirstRowOfSce = 3;

    // constant of browsers
    public static String FIREFOX_LATEST = "C:/Program Files (x86)/Mozilla FirefoxLatest/firefox.exe";
    public static String FIREFOX_36 = "C:/Program Files (x86)/Mozilla Firefox3.6/firefox.exe";
    public static String FIREFOX_10 = "C:/Program Files (x86)/Mozilla Firefox 10/firefox.exe";
    public static String firefoxProfile = "";
    public static String ieDriver = "";
    public static String ieEdgeDriver = "";
    public static String chromeDriver = "";
    public static String geckoDriver = "";
    public static String safariDriver = "";
    public static String operaDriver = "";

    public static String RUNTIME_BROWSER = "";

    //public static final String HUBADDRESS = "http://localhost:4444/wd/hub";
    public static String HUBADDRESS = "";

    public static final int NUMBROWSER = 1;

    public static String logFile = "";

    // public static String excelDataInput = "";
    public static String excelDataInput = "C:\\cerebos automation\\automation\\DataInput\\AutomationExcelFW.xls";
    public static String excelConfig = "C:\\cerebos automation\\automation\\Config\\ExcelConfig.xls";
    public static String FWConfigPath = "";

    public static String resultFolder = "";

    public static String excelResultFile = "";

    public static String screenshootFolder = "";

    public static Object[] result = new Object[2];

    public static String failreason = "";

    public static String actualreason = "";

    public static int iVar1 = 0;

    public static int iVar2 = 0;

    public static String strVar1 = "";

    public static String strVar2 = "";

    public static String strVar3 = "";

//    public static int crossBrowser = 0;

    public static WritableWorkbook workbookWritable = null;

    public static WritableSheet sheetWritable = null;

    public static String imgScreenShoot = "";

    public static String imgSikuli = "/src/com/sai/uimap/img/";

    public static int MIN_MILISECOND = 1000;

    public static String defaultURLSelenium = "http://google.com";

    public static String curBrowser = "";

    public static String testRun_RunDate = "";

    public static long defaultTimePageLoad = 5;
    
    public static String IESecurity  		= "IESecurity.png";
    


}
