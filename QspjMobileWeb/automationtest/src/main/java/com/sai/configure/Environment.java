package com.sai.configure;

import org.junit.Assert;

import com.sai.framework.library.CommonFunctions;
import com.sai.framework.library.JQueryFunctions;
import com.sai.framework.library.SeleniumFunctions;
import com.sai.framework.library.SikuliFunctions;
import com.sai.framework.library.VerifyFunctions;
import com.sai.utils.ExcelHandle;
import com.sai.utils.Log;
import com.sai.utils.Utils;

/**
 * @Name: GlobalVariables
 * @author DatNguyen
 * @Description: This class will init Standard Library instance and Util instance.
 * @CreatedDate: 04/03/2014
 */
public class Environment extends Assert
{
    public static ExcelHandle excelhandle;
    public static SikuliFunctions sikuliFunc;
        
    public static Utils utils = new Utils();
    public static Log log = new Log(Environment.class);
    
    // init Framework Library
    public static CommonFunctions driver = new CommonFunctions();
    public static JQueryFunctions jqueryFunc = new JQueryFunctions(driver);
    public static SeleniumFunctions selFunc = new SeleniumFunctions(driver);
    public static VerifyFunctions verifyFunc =  new VerifyFunctions();
    //public static SikuliFunctions sikuliFunc = new SikuliFunctions();
}
